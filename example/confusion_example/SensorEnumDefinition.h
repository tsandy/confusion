/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef CONFUSION_EXAMPLE_SENSORENUMDEFINITION_H
#define CONFUSION_EXAMPLE_SENSORENUMDEFINITION_H

enum PROCESS_TYPE { IMU = 0, NUM_PROCESS_SENSORS };

enum UPDATE_TYPE { TAG = 0, POSEMEAS, NUM_UPDATE_SENSORS };

#endif  // CONFUSION_EXAMPLE_SENSORENUMDEFINITION_H
