/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace confusion {

template <typename StateType>
TagTracker<StateType>::TagTracker(const std::string& configFileName,
                                  const std::string& dataFilePath)
    : conFusor_(std::make_shared<StateType>()),
      statesBatch_(NUM_PROCESS_SENSORS, NUM_UPDATE_SENSORS),
      imuPropagator_(5000) {
  std::cout << "[TagTracker] Starting initialization." << std::endl;

  // Load config parameters from the text file
  confusionPath_ = dataFilePath;
  boost::property_tree::read_info(configFileName, pt);

  aprilTagInterface_ = std::unique_ptr<AprilTagModule>(new AprilTagModule(
      &conFusor_, confusionPath_, configFileName, TAG, &newTagMeasReady_));
#ifdef SYNC_DEBUG
  aprilTagInterface_->setVerbose(true);
#endif

  // Link the states to the reference frame maps for adding new frames on the
  // fly
  auto firstState =
      std::dynamic_pointer_cast<StateType>(conFusor_.stateVector_.front());
  firstState->setTagReferenceFrameOffsets(
      aprilTagInterface_->getTagReferenceFrameOffsetsMapPointer());

  // Set up the logger
  std::string logFileName = confusionPath_ + "/tagtracker_log.txt";
  logger_ = std::unique_ptr<confusion::Logger>(
      new confusion::Logger(logFileName, *conFusor_.stateVector_.front()));
  logData_ = pt.get<bool>("logData");

  conFusor_.setSolverOptions(pt.get<int>("numThreads"),
                             pt.get<int>("maxNumIterations"));
  batchSize_ = pt.get<int>("batchSize");
  runBatch_ = pt.get<bool>("runBatch");
  forwardPropagateState_ = pt.get<bool>("forwardPropagateState");

  // Set the initial constraint on the state
  tagTrackerParameters_.twi_init_stddev = pt.get<double>("twi_init_stddev");
  tagTrackerParameters_.qwi_init_stddev = pt.get<double>("qwi_init_stddev");
  tagTrackerParameters_.vwi_init_stddev = pt.get<double>("vwi_init_stddev");
  tagTrackerParameters_.ba_init_stddev = pt.get<double>("ba_init_stddev");
  tagTrackerParameters_.bg_init_stddev = pt.get<double>("bg_init_stddev");

  std::vector<Eigen::MatrixXd> stateParamInitialWeightings(
      conFusor_.stateVector_.front()->parameters_.size());
  Eigen::MatrixXd param_weighting(3, 3);
  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.twi_init_stddev;
  stateParamInitialWeightings[0] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.qwi_init_stddev;
  stateParamInitialWeightings[1] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.vwi_init_stddev;
  stateParamInitialWeightings[2] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.ba_init_stddev;
  stateParamInitialWeightings[3] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.bg_init_stddev;
  stateParamInitialWeightings[4] = param_weighting;

  conFusor_.stateVector_.front()->setInitialStateWeighting(
      stateParamInitialWeightings);

  tagTrackerParameters_.imuCalibration_.gravityMagnitude_ =
      pt.get<double>("gravityMagnitude");
  tagTrackerParameters_.wi_stddev_ = pt.get<double>("wi_stddev");
  tagTrackerParameters_.ai_stddev_ = pt.get<double>("ai_stddev");
  tagTrackerParameters_.bg_stddev_ = pt.get<double>("bg_stddev");
  tagTrackerParameters_.ba_stddev_ = pt.get<double>("ba_stddev");
  tagTrackerParameters_.initialize();

  gravity_rot_.setZero();
#ifdef OPT_GRAVITY
  //	Eigen::MatrixXd gravityRotInitialWeighting(2,2);
  //	gravityRotInitialWeighting.setIdentity();
  //	gravityRotInitialWeighting /=
  //tagTrackerParameters_.tagMeasCalibration_.ftr_init_stddev;
  conFusor_.addStaticParameter(confusion::Parameter(
      gravity_rot_.data(), 2, "g_r"));  //, gravityRotInitialWeighting);
#endif

  // Set up publishers and subscribers
  auto callbackPtr =
      std::make_shared<confusion::SolverOverrunCallback>(newTagMeasReady_);
  conFusor_.setIterationCallback(callbackPtr);

  // Start the estimator on a separate thread
  estimatorThread_ = std::thread(&TagTracker<StateType>::runEstimator, this);
  estimatorThread_.detach();

  std::cout << "[TagTracker] Initialization complete." << std::endl;
}

template <typename StateType>
void TagTracker<StateType>::addImuMeasurement(const double& t,
                                              const Eigen::Vector3d& linAccel,
                                              const Eigen::Vector3d& angVel) {
  if (!run_) return;

  t_imu_latest_ = t;

  conFusor_.addProcessMeasurement(std::make_shared<confusion::ImuMeas>(
      t, linAccel, angVel, &tagTrackerParameters_.imuCalibration_,
      &gravity_rot_));

  // Optionally publish the forward-propagated state at IMU rate
  if (forwardPropagateState_) {
    confusion::ImuMeas meas(t, linAccel, angVel,
                            &tagTrackerParameters_.imuCalibration_,
                            &gravity_rot_);
    imuPropagator_.addImuMeasurement(meas);
  }
}

template <typename StateType>
void TagTracker<StateType>::runEstimator() {
  confusion::Rate loop_rate(loop_freq);

  while (run_) {
    auto t_start = std::chrono::high_resolution_clock::now();

    // This processes any new measurements that have been added to the ConFusor
    // in the callbacks and either assigns them to existing states or spawns new
    // states if the measurements are newer than the most recent state.
    if (!conFusor_.assignMeasurements()) {
      loop_rate.sleep();
      continue;  // No new states or measurements added, so wait for more
                 // measurements
    }
    newTagMeasReady_ = false;

    // Manage the batch size. If there are too many states currently active,
    // remove the oldest state(s).
    if (conFusor_.numStates() > batchSize_) {
      if (conFusor_.numStates() - batchSize_ > 1)
        std::cout << "WARNING: Had to marginalize "
                  << conFusor_.numStates() - batchSize_
                  << " states in one iteration. The estimator is not running "
                     "fast enough."
                  << std::endl;

      stateToDropIndex_ -= conFusor_.numStates() - batchSize_;

      // Optionally store the marginalized states to run a batch prboblem later
      if (runBatch_)
        conFusor_.marginalizeFrontStates(conFusor_.numStates() - batchSize_,
                                         &statesBatch_);
      else
        conFusor_.marginalizeFrontStates(conFusor_.numStates() - batchSize_);
    }

    // Solve the MHE problem for the active batch of states
    conFusor_.optimize();
    conFusor_.verbosePrint();  // briefPrint();

    tracking_ = true;
    aprilTagInterface_->copyOutEstimateAfterOptimization();

    // You can optionally draw a diagram of the current MHE problem structure
    if (drawDiagramRequest_) {
       confusion::Diagram diagram(conFusor_);
      drawDiagramRequest_ = false;
    }

    auto t_stop = std::chrono::high_resolution_clock::now();

    std::cout << ">>>>>>>>> TagTracker run took "
              << std::chrono::duration<double>(t_stop - t_start).count()
              << " sec\n"
              << std::endl;

    publish(conFusor_.stateVector_.back(), &(conFusor_.stateVector_));

    if (forwardPropagateState_) {
      auto state =
          std::dynamic_pointer_cast<StateType>(conFusor_.stateVector_.back());
      imuPropagator_.update(state->getParameterStruct());
    }

    if (logData_) {
      Eigen::VectorXd priorResidual;
      std::vector<Eigen::VectorXd> processResiduals;
      std::vector<Eigen::VectorXd> updateResiduals;
      conFusor_.getResiduals(priorResidual, processResiduals, updateResiduals);

      logger_->writeBatch(conFusor_.stateVector_);
      logger_->writeStaticParameters(conFusor_.staticParameters_,
                                     conFusor_.stateVector_.back()->t_);
      logger_->writeResiduals(priorResidual, processResiduals, updateResiduals);
    }

    loop_rate.sleep();
  }

  runEstimatorLoopDone_ = true;
}

template <typename StateType>
void TagTracker<StateType>::visualizeInImage(const double& t, cv::Mat& image) {
  if (!tracking_) return;

  // Get the estimated state at the desired time
  confusion::ImuStateParameters imuState = imuPropagator_.propagate(t);

  aprilTagInterface_->projectTagsIntoImage(imuState.T_w_i_, "cam", image);
}

template <typename StateType>
void TagTracker<StateType>::stopTracking() {
  std::cout << "Stopping tracking" << std::endl;

  // Stop the estimator and wait for it to exit
  run_ = false;
  tracking_ = false;
  while (!runEstimatorLoopDone_)
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

  // Stop tracking so that it can be started with the optimized parameters
  conFusor_.stopTracking(&statesBatch_);

  runEstimatorLoopDone_ = false;
}

template <typename StateType>
void TagTracker<StateType>::startTracking() {
  if (run_) {
    std::cout << "TagTracker is already running." << std::endl;
    return;
  }

  stateToDropIndex_ = -1;

  std::cout << "Starting tracking" << std::endl;
  run_ = true;
  estimatorThread_ = std::thread(&TagTracker<StateType>::runEstimator, this);
  estimatorThread_.detach();
}

template <typename StateType>
void TagTracker<StateType>::drawDiagramCallback() {
  std::cout << "User requested for a MHE diagram to be drawn." << std::endl;
  drawDiagramRequest_ = true;
}

template <typename StateType>
void TagTracker<StateType>::triggerBatchCalCallback() {
  if (!runBatch_) {
    std::cout << "WARNING: User requested a batch calibration run even "
                 "runBatch is not set in "
                 "the config file. Cannot perform the batch calibration."
              << std::endl;
    return;
  }

  if (run_) stopTracking();

  std::cout << "Starting a batch calibration run for " << statesBatch_.size()
            << " states" << std::endl;

  // Activate the extrinsic calibrations
  auto& T_c_i_ptr = aprilTagInterface_->sensorFrameOffsets_["cam"];
  conFusor_.staticParameters_.getParameter(T_c_i_ptr->trans.data())
      ->unsetConstant();
  conFusor_.staticParameters_.getParameter(T_c_i_ptr->rot.coeffs().data())
      ->unsetConstant();

  publish(statesBatch_.back(), &statesBatch_);

  confusion::BatchFusor batchFusor(statesBatch_, conFusor_.staticParameters_);
  batchFusor.buildProblem();

  Eigen::VectorXd priorResidual;
  priorResidual.resize(0);
  std::vector<Eigen::VectorXd> processResiduals;
  std::vector<Eigen::VectorXd> updateResiduals;
  batchFusor.getResiduals(processResiduals, updateResiduals);

  batchFusor.optimize();

  batchFusor.printParameterCovariance(T_c_i_ptr->trans.data(), "t_c_i");
  batchFusor.printParameterCovariance(T_c_i_ptr->rot.coeffs().data(), "q_c_i");

  publish(statesBatch_.back(), &statesBatch_);

  T_c_i_ptr->print("T_c_i");

  processResiduals.clear();
  updateResiduals.clear();
  batchFusor.getResiduals(processResiduals, updateResiduals);

  if (logData_) {
    logger_->writeBatch(batchFusor.stateVector_);
    logger_->writeStaticParameters(conFusor_.staticParameters_,
                                   statesBatch_.back()->t_);
    logger_->writeResiduals(priorResidual, processResiduals, updateResiduals);
  }

  // Reset the status of the extrinsic calibration parameters
  if (!pt.get<bool>("optimizeTci")) {
    conFusor_.staticParameters_.getParameter(T_c_i_ptr->trans.data())
        ->setConstant();
    conFusor_.staticParameters_.getParameter(T_c_i_ptr->rot.coeffs().data())
        ->setConstant();
  }

  // Empty the batch states to start logging for another batch problem
  statesBatch_.reset();

  // Now restart the estimator
  startTracking();
}

}  // namespace confusion
