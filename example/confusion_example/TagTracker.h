/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_EXAMPLES_TAGTRACKER_H_
#define INCLUDE_CONFUSION_EXAMPLES_TAGTRACKER_H_

#define SYNC_DEBUG
//#define COST_DEBUG

#include <confusion/BatchFusor.h>
#include <confusion/ConFusor.h>
#include <confusion/Logger.h>
#include <confusion/utilities/time_utils.h>
#include <confusion/utilities/utilities.h>

#include <Eigen/Core>
#include <boost/circular_buffer.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <mutex>
#include <string>
#include <thread>

#include "confusion/models/PoseMeas.h"
#include "confusion/modules/apriltag/AprilTagModule.h"
#include "confusion/utilities/ImuPropagator.h"
#include "confusion_example/TagTrackerParameters.h"
#include "confusion/Diagram.h"

namespace confusion {

template <typename StateType>
class TagTracker {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  TagTracker(const std::string& configFileName,
             const std::string& dataFilePath);

  void addImuMeasurement(const double& t, const Eigen::Vector3d& linAccel,
                         const Eigen::Vector3d& angVel);

  void triggerBatchCalCallback();
  void drawDiagramCallback();

  virtual void runEstimator();

  void stopTracking();

  void startTracking();

  virtual void publish(std::shared_ptr<confusion::State> statePtr,
                       const confusion::StateVector* stateVector) {}

  void visualizeInImage(const double& t, cv::Mat& image);

  std::thread estimatorThread_;

  ConFusor conFusor_;

  std::unique_ptr<AprilTagModule> aprilTagInterface_;
  TagTrackerParameters tagTrackerParameters_;

  Eigen::Vector2d gravity_rot_;

  int batchSize_;
  bool newTagMeasReady_ = false;

  bool logData_;

 protected:
  bool forwardPropagateState_;
  bool tracking_ = false;
  double t_imu_latest_ = 0.0;

 private:
  std::string confusionPath_;
  std::unique_ptr<confusion::Logger> logger_;
  boost::property_tree::ptree pt;

  // For saving states and running a batch problem at the end
  confusion::StateVector statesBatch_;
  bool runBatch_ = false;
  bool run_ = true;
  bool runEstimatorLoopDone_ = false;
  int stateToDropIndex_ = -1;  //-1 means that we shouldn't drop a state
  bool drawDiagramRequest_ = false;

  confusion::ImuPropagator imuPropagator_;

  std::string configFile = "/example/tagtracker_config.cfg";
  double loop_freq = 100;  // [Hz]
};

}  // namespace confusion

#include "confusion_example/impl/TagTracker.h"

#endif /* INCLUDE_CONFUSION_EXAMPLES_TAGTRACKER_H_ */
