/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CON_FUSION_PRIORCONSTRAINT_H_
#define INCLUDE_CON_FUSION_PRIORCONSTRAINT_H_

#include <Eigen/Core>

#include "confusion/Parameter.h"
#include "confusion/PriorCost.h"

namespace confusion {

class PriorConstraint {
 public:
  void addStateParameter(confusion::Parameter& param);

  // It is only supported to call this before tracking has started
  // fixMarginalizedParams needs to be called after the parameters on the first
  // state have been activated
  void addStateParameterAndActivate(confusion::Parameter& param);

  void clearStateParameters();

  void addStaticParameter(confusion::Parameter& param);

  // Note that this will invalidate the marginalizedParams and the prior problem
  // so these need to be re-constructed.
  // todo Hold a flag indicating if these things are valid to notify the user if
  // they are doing things in the wrong order?
  void removeStaticParameter(confusion::Parameter& param);

  // Fix the value of the parameters in the prior cost for the next problem
  // Build the vector of prior parameter blocks for problem computation
  void fixMarginalizedParams();

  bool check() const;

  void addPriorCostToProblem(ceres::Problem* problem,
                             double* dtFirstState = nullptr);

  void print();

  void initialize() { initialized_ = true; }

  bool initialized() const { return initialized_; }

  void reset() { initialized_ = false; }

  // This also removes the static parameter portion to remove all prior
  // information
  void completeReset();

  // Members governing the prior cost. These are set during state
  // marginalization and manipulated when tracking is stopped and static
  // parameters are added/removed from the prior constraint.
  Eigen::VectorXd priorErrorOffset_;    ///< (J_p^T)^dagger bstar
  Eigen::MatrixXd priorCostWeighting_;  ///< J_p
  Eigen::MatrixXd Hstar_;               ///< Hstar = J_p^T J_p
  Eigen::VectorXd bstar_;

  std::vector<confusion::Parameter*> statePriorParameters_;
  std::vector<confusion::Parameter*> staticPriorParameters_;

  int statePortionLocalSize_ = 0;
  int staticPortionLocalSize_ = 0;
  int priorParameterLocalSize_ = 0;

  std::vector<confusion::FixedParameter> marginalizedParams_;

  std::shared_ptr<confusion::PriorCost> priorCostPtr;
  std::vector<double*>
      priorParams_;  // This is sent at problem.AddResidualBlock to tell Ceres
                     // which parameters are involved

 private:
  bool initialized_ = false;
};

}  // namespace confusion

#endif /* INCLUDE_CON_FUSION_PRIORCONSTRAINT_H_ */
