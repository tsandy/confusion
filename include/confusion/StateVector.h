/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_STATEVECTOR_H_
#define INCLUDE_CONFUSION_STATEVECTOR_H_

#include <deque>
#include <memory>

#include "confusion/ProcessChain.h"
#include "confusion/State.h"
#include "confusion/UpdateMeasurement.h"

namespace confusion {

class StateVector {
  friend class MeasurementManager;

 public:
  // Pass a pointer to the first derived state. That is the way that this class
  // knows what kind of state is being used.
  StateVector(std::shared_ptr<State> firstStatePtr);

  // This is only used in BatchFusor to copy in an existing state trajectory.
  StateVector(StateVector& stateVector);

  // todo Do I really need this? It is used to store states from a ConFusor for
  // use later. With this constructor, a lot of the functionality of the class
  // is lost
  StateVector(const size_t numProcessSensors, const size_t numUpdateSensors);

  void pushState(const std::shared_ptr<State>& newState);

  /**
   * Insert a new state in the middle of the current active batch
   * @param newState Pointer to the new state
   * @param index Position at which the new state should be added (e.g. to add a
   * state between the states at indices 2 and 3, set this to 3)
   */
  void insertState(const std::shared_ptr<State>& newState, size_t index);

  void popState();

  void removeState(size_t index);

  bool check() const;

  void print() const;

  void deactivateParameters();

  size_t size() const;

  // This is used when tracking is stopped and restarted.
  void reset();

  // This wipes out the whole state history. Used when the vector of stored
  // states for a batch problem is reset to start another batch run. Note that
  // the measurement state index vectors are invalid after this is called.
  void clear();

  std::shared_ptr<State>& operator[](size_t pos) { return states_[pos]; }
  const std::shared_ptr<const State> operator[](size_t pos) const {
    return std::const_pointer_cast<const State>(states_[pos]);
  }
  std::shared_ptr<State>& front() { return states_.front(); }
  std::shared_ptr<State>& back() { return states_.back(); }
  std::deque<std::shared_ptr<State>>::iterator begin() {
    return states_.begin();
  }
  std::deque<std::shared_ptr<State>>::iterator end() { return states_.end(); }
  std::deque<std::shared_ptr<State>>::const_iterator begin() const {
    return states_.begin();
  }
  std::deque<std::shared_ptr<State>>::const_iterator end() const {
    return states_.end();
  }
  bool empty() const { return states_.empty(); }
  bool firstStateInitialized() const { return firstStateInitialized_; }

  size_t NumProcessSensors() const { return numProcessSensors_; }
  size_t NumUpdateSensors() const { return numUpdateSensors_; }

 protected:
  std::deque<std::shared_ptr<State>> states_;
  bool firstStateInitialized_ = false;

  const size_t numProcessSensors_;
  const size_t numUpdateSensors_;

  // These keep track of the last state to which measurements of different types
  // were assigned
  std::vector<size_t> updateMeasStateIndex;
  std::vector<size_t> processMeasStateIndex;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_STATEVECTOR_H_ */
