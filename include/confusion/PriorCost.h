/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_PRIOR_COST_H
#define INCLUDE_CONFUSION_PRIOR_COST_H

#include <Eigen/Core>
#include <memory>

#include "confusion/Parameter.h"
#include "confusion/utilities/ceres_utils.h"
#include "confusion/utilities/distances.h"

// Constrains the start of the batch, given the marginalized value of the
// leading state and the corresponding confidence in the estimate. Calculates the
// error in tangent space

namespace confusion {

class PriorCost : public ceres::CostFunction {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  PriorCost(const std::vector<FixedParameter>& marginalizedParams,
            const Eigen::MatrixXd& W_, Eigen::VectorXd& priorErrorOffset_);

  bool Evaluate(double const* const* x, double* residuals,
                double** jacobians) const final;

  const std::vector<FixedParameter>&
      marginalizedParams_;
  const Eigen::MatrixXd& W;

  // The prior error from the previous MHE problem
  const Eigen::VectorXd& priorErrorOffset;

  size_t errorSize;
};

}  // namespace confusion

#endif  // INCLUDE_CONFUSION_PRIOR_COST_H
