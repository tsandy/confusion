/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_PARAMETERBASE_H_
#define INCLUDE_CONFUSION_PARAMETERBASE_H_

#include <ceres/ceres.h>

#include <Eigen/Core>
#include <string>
#include <vector>

#include "confusion/LocalParameterization.h"
#include "confusion/utilities/StaticParameterRandomWalkProcess.h"

namespace confusion {
/**
 * Class containing all of the internal status information of estimated
 * parameters (either as part of a state or static parameter).
 */
class Parameter {
  friend class FixedParameter;
  friend class PriorConstraint;
  friend class ConFusor;

 public:
  /**
   * Create a new Parameter. Parameters are set with constant_ = false by
   * default.
   * @param data Pointer to the start of the array of parameter data. The
   * Parameter does NOT take ownership of the data.
   * @param size Global size of the parameter
   * @param name Name of the parameter (just used for diagnostics and in the
   * Diagram class)
   * @param constant If true, the parameter value will not be optimized
   * @param parameterization Pointer to an associated local parameterization.
   * Set to nullptr when no local parameterization is used.
   */
  Parameter(
      double* data, size_t size, const std::string& name, bool constant = false,
      std::shared_ptr<LocalParameterizationBase> parameterization = nullptr);

  /**
   * Create a new Parameter. With this constructor, constant_ = false by
   * default.
   * @param data Pointer to the start of the array of parameter data. The
   * Parameter does NOT take ownership of the data.
   * @param size Global size of the parameter
   * @param name Name of the parameter (just used for diagnostics and in the
   * Diagram class)
   * @param parameterization Pointer to an associated local parameterization.
   * Set to nullptr when no local parameterization is used.
   */
  Parameter(double* data, size_t size, const std::string& name,
            std::shared_ptr<LocalParameterizationBase> parameterization);

  /**
   * When there is a prior likelihood on an estimated parameter, this function
   * sets the confidence in the initial parameter values.
   * @param w The stiffness matrix (inverse standard deviation) of the prior
   * confidence in the parameter values. Should be a square matrix with size
   * equal to the local size of the parameter.
   */
  void setInitialConstraintWeighting(const Eigen::MatrixXd& w);

  /**
   * Add the parameter to the current optimization problem being built
   * @param problem Pointer to the ceres optimization problem
   */
  void addToProblem(ceres::Problem* problem);

  //
  /**
   * Clone the parameter, copying the current data values into the new
   * parameter, but maintaining the current parameter's internal state. This is
   * used when a new state is created from the previous state during online
   * operation.
   * @param data The address of the new parameter data
   * @return The cloned Parameter
   */
  Parameter clone(double* data) const;

  /**
   * Attach a random walk process to this static parameter. It will add a
   * process constraint between the prior constraint and the static parameter.
   * That means that two parameters will then be optimized for this static
   * parameter; one between the prior and the process, and one between the
   * process and the other residuals in the MHE.
   * @param processNoise  Process white noise. Units [unit of parameter *
   * sqrt(sec)].
   * @return Shared pointer to the random walk process cost function.
   */
  void attachRandomWalkProcess(const double& processNoise);

  /**
   * Detach the random walk process from this static parameter. Note that the
   * rwp cost function will be destroyed, but may stay active in the MHE problem
   * last built. It will not be included in the next problem built.
   */
  void detachRandomWalkProcess();

  /**
   * Convenience function to print the current data values
   */
  void toCout() const;

  /**
   * Set the parameter constant. This should only be used before sensor fusion
   * has started! If you want to set a parameter constant during operation
   * use ConFusor::setStaticParamsConstant. Setting a state parameter constant
   * during operation is not yet supported.
   */
  void setConstant();

  /**
   * Set the parameter to be optimized, starting from the creation of the next
   * optimization problem.
   */
  void unsetConstant() { constant_ = false; }

  size_t size() const { return size_; }
  size_t localSize() const { return localSize_; }
  bool isConstant() const { return constant_; }
  bool isActive() const { return active_; }
  bool immediatelyAddToPrior() const { return immediatelyAddToPrior_; }
  bool priorConstraintActive() const { return priorConstraintActive_; }

  int priorConstraintLocalPosition() const {
    return priorConstraintLocalPosition_;
  }
  std::string name() const { return name_; }

  bool isRandomWalkProcessAttached() const { return randomWalkProcess_.get(); }
  bool isRandomWalkProcessActive() const { return randomWalkProcessActive_; }
  double* rwpPriorSideParameterAddress() {
    return rwpPriorSideParameterValue_.data();
  }

  double* data_;  ///< Address of the associated parameter data. The Parameter
                  ///< does not take ownership of the data.
  std::shared_ptr<LocalParameterizationBase> parameterization_ =
      nullptr;  ///< Pointer to the parameter's local parameterization. Set to
                ///< nullptr when no local parameterization is active.
  bool active_ =
      false;  ///< This flag indicates if the parameter is linked to any of the
              ///< cost functions in the current optimization problem.
              // todo It needs to be set in many different places, so it is left
              // public for now even though it shouldn't be set from outside of
              // ConFusion

 protected:
  /**
   * Update the RWP, copying the new value of the optimized parameter to the
   * prior side parameter and setting the new dt. This is called after one or
   * more states are marginalized in the MHE. Called by
   * ConFusion::PriorConstraint.
   * @param dt The delta time from the time of the last marginalized state to
   * that of the new marginalized state.
   */
  void updateRandomWalkProcess(const double& dt);

  /**
   * Add the random walk process to the MHE problem. Called by
   * ConFusion::PriorConstraint.
   * @param problem Pointer to the current MHE problem
   */
  void addRandomWalkProcessToProblem(ceres::Problem* problem);

  size_t size_;  ///< Global size of the parameter (e.g. 4 for a quaternion)
  size_t localSize_;  ///< Local size of the parameter (e.g. 3 for a quaternion)
  std::string name_;  ///< Name of the parameter. Just used for diagnostics.
  bool constant_ = false;  ///< When true, the parameter will not be optimized

  // When true, a prior distribution on this parameter is specified, and the
  // parameter will be immediately added to the prior constraint when it first
  // goes active
  bool immediatelyAddToPrior_ = false;
  ///< The inverse standard deviation for the prior confidence in the parameter
  ///< values
  Eigen::MatrixXd initialConstraintWeighting_;
  bool priorConstraintActive_ = false;

  // Indicates where the parameter sits among the static parameters (NOT
  // including the state parameters!) in the prior constraint.
  int priorConstraintLocalPosition_ = 0;

  std::shared_ptr<StaticParameterRandomWalkProcess> randomWalkProcess_ =
      nullptr;
  bool randomWalkProcessActive_ = false;

  // When a RWP is attached to this parameter, this copy of it is constrained by
  // the prior and the rwp.
  Eigen::VectorXd rwpPriorSideParameterValue_;
};

// This class copies and fixes the values of a parameter for use in
// marginalization
class FixedParameter {
  friend class PriorCost;

 public:
  FixedParameter(Parameter paramIn);

  size_t size() const { return size_; }
  size_t localSize() const { return localSize_; }
  bool isConstant() const { return constant_; }
  std::string name() const { return name_; }
  Eigen::VectorXd data() const { return copyOfData_; }

 protected:
  Eigen::VectorXd copyOfData_;
  size_t size_;  // The size of the parameter before applying the local
                 // parameterization (e.g. 4 for a quaternion)
  size_t localSize_;
  std::shared_ptr<LocalParameterizationBase> parameterization_;
  bool constant_;
  std::string name_;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_PARAMETERBASE_H_ */
