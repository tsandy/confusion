//
// Created by tim on 03.09.19.
//

#ifndef CONFUSION_ARMODOMETRYCONFIG_H
#define CONFUSION_ARMODOMETRYCONFIG_H

#include <confusion/ConFusor.h>
#include <confusion/utilities/Pose.h>
#include <confusion/utilities/rotation_utils.h>

#include <boost/property_tree/ptree.hpp>

#include "confusion/modules/arm_odometry/ArmOdometryBuffer.h"

namespace confusion {

/**
 * The JointOdometry module handles the use of arm kinemtics and joint encoder
 * measurements linking a robot body from to a sensor frame connected to the arm
 * end-effector.
 */
template <int DOF>
struct ArmOdometryConfig {
  void initialize(const boost::property_tree::ptree& pt,
                  confusion::ConFusor& conFusor) {
    // Set up static parameters
    // b_q. Optionally specify an inital value
    if (pt.get<bool>("setBqInitialValue")) {
      size_t numVals = 0;
      for (const boost::property_tree::ptree::value_type& val :
           pt.get_child("bq.")) {
        jointAngleBias_(numVals) = std::stod(val.second.data());
        ++numVals;
      }
      if (numVals != DOF - 2) {
        std::cout << "ERROR: User requested to set bq from the config file but "
                     "DOF-2 values were not given! Instead "
                  << numVals << " were found. Aborting" << std::endl;
        abort();
      }
    } else
      jointAngleBias_.setZero();

    optJointAngleBias_ = pt.get<bool>("opt_jointAngleBias");
    if (optJointAngleBias_) {
      confusion::Parameter jointAngleBiasParam(jointAngleBias_.data(), DOF - 2,
                                               "b_q");
      Eigen::MatrixXd jointAngleBiasInitialWeighting(DOF - 2, DOF - 2);
      jointAngleBiasInitialWeighting.setIdentity();
      jointAngleBiasInitialWeighting /=
          pt.get<double>("jointAngleBias_init_stddev");
      jointAngleBiasParam.setInitialConstraintWeighting(
          jointAngleBiasInitialWeighting);
      conFusor.addStaticParameter(jointAngleBiasParam);
    } else {
      conFusor.addStaticParameter(
          confusion::Parameter(jointAngleBias_.data(), DOF - 2, "b_q", true));
    }

    // T_sensor_ee
    T_sensor_ee_.trans(0) = pt.get<double>("T_sensor_ee.px");
    T_sensor_ee_.trans(1) = pt.get<double>("T_sensor_ee.py");
    T_sensor_ee_.trans(2) = pt.get<double>("T_sensor_ee.pz");
    T_sensor_ee_.rot.w() = pt.get<double>("T_sensor_ee.qw");
    T_sensor_ee_.rot.x() = pt.get<double>("T_sensor_ee.qx");
    T_sensor_ee_.rot.y() = pt.get<double>("T_sensor_ee.qy");
    T_sensor_ee_.rot.z() = pt.get<double>("T_sensor_ee.qz");
    T_sensor_ee_.rot.normalize();
    optSensorEeTransformation_ = pt.get<bool>("opt_T_sensor_ee");
    if (optSensorEeTransformation_) {
      Eigen::MatrixXd t_sensor_ee_initialWeighting(3, 3);
      t_sensor_ee_initialWeighting.setIdentity();
      t_sensor_ee_initialWeighting /= pt.get<double>("t_sensor_ee_init_stddev");
      confusion::Parameter tseParam(T_sensor_ee_.trans.data(), 3,
                                    "t_sensor_ee");
      tseParam.setInitialConstraintWeighting(t_sensor_ee_initialWeighting);
      conFusor.addStaticParameter(tseParam);
      Eigen::MatrixXd q_sensor_ee_initialWeighting(3, 3);
      q_sensor_ee_initialWeighting.setIdentity();
      q_sensor_ee_initialWeighting /= pt.get<double>("q_sensor_ee_init_stddev");
      confusion::Parameter qseParam(T_sensor_ee_.rot.coeffs().data(), 4,
                                    "q_sensor_ee",
                                    std::make_shared<confusion::QuatParam>());
      qseParam.setInitialConstraintWeighting(q_sensor_ee_initialWeighting);
      conFusor.addStaticParameter(qseParam);
    } else {
      conFusor.addStaticParameter(confusion::Parameter(
          T_sensor_ee_.trans.data(), 3, "t_sensor_ee", true));
      conFusor.addStaticParameter(confusion::Parameter(
          T_sensor_ee_.rot.coeffs().data(), 3, "q_sensor_ee", true,
          std::make_shared<confusion::QuatParam>()));
    }

    // T_armbase_body
    T_armbase_body_.trans(0) = pt.get<double>("T_armbase_body.px");
    T_armbase_body_.trans(1) = pt.get<double>("T_armbase_body.py");
    T_armbase_body_.trans(2) = pt.get<double>("T_armbase_body.pz");
    T_armbase_body_.rot.w() = pt.get<double>("T_armbase_body.qw");
    T_armbase_body_.rot.x() = pt.get<double>("T_armbase_body.qx");
    T_armbase_body_.rot.y() = pt.get<double>("T_armbase_body.qy");
    T_armbase_body_.rot.z() = pt.get<double>("T_armbase_body.qz");
    T_armbase_body_.rot.normalize();
    optArmBaseBodyTransformation_ = pt.get<bool>("opt_T_armbase_body");
    if (optArmBaseBodyTransformation_) {
      Eigen::MatrixXd t_armbase_body_initialWeighting(3, 3);
      t_armbase_body_initialWeighting.setIdentity();
      t_armbase_body_initialWeighting /=
          pt.get<double>("t_armbase_body_init_stddev");
      confusion::Parameter tseParam(T_armbase_body_.trans.data(), 3,
                                    "t_armbase_body");
      tseParam.setInitialConstraintWeighting(t_armbase_body_initialWeighting);
      conFusor.addStaticParameter(tseParam);
      Eigen::MatrixXd q_armbase_body_initialWeighting(3, 3);
      q_armbase_body_initialWeighting.setIdentity();
      q_armbase_body_initialWeighting /=
          pt.get<double>("q_armbase_body_init_stddev");
      confusion::Parameter qseParam(T_armbase_body_.rot.coeffs().data(), 4,
                                    "q_armbase_body",
                                    std::make_shared<confusion::QuatParam>());
      qseParam.setInitialConstraintWeighting(q_armbase_body_initialWeighting);
      conFusor.addStaticParameter(qseParam);
    } else {
      conFusor.addStaticParameter(confusion::Parameter(
          T_armbase_body_.trans.data(), 3, "t_armbase_body", true));
      conFusor.addStaticParameter(confusion::Parameter(
          T_armbase_body_.rot.coeffs().data(), 3, "q_armbase_body", true,
          std::make_shared<confusion::QuatParam>()));
    }

    dt_ = pt.get<double>("dt_init", 0.0);
    optDt_ = pt.get<bool>("opt_dt");
    confusion::Parameter dtParam(&dt_, 1, "dt", !optDt_);
    Eigen::MatrixXd dt_initialWeighting(1, 1);
    dt_initialWeighting.setIdentity();
    dt_initialWeighting /= pt.get<double>("dt_init_stddev");
    dtParam.setInitialConstraintWeighting(dt_initialWeighting);
    conFusor.addStaticParameter(dtParam);

    cov_q_.setIdentity();
    auto joint_encoder_stddev = pt.get<double>("joint_encoder_stddev");
    cov_q_ *= joint_encoder_stddev * joint_encoder_stddev;
  }

  // Set all parameters to be optimized in a batch problem
  void setParametersActiveForBatchProblem(confusion::ConFusor& conFusor) {
    confusion::Parameter* bqParam =
        conFusor.staticParameters_.getParameter(jointAngleBias_.data());
    if (bqParam)
      bqParam->unsetConstant();
    else
      std::cout << "WARNING: JointAngleBias parameter not found for solving "
                   "the batch problem?"
                << std::endl;

    confusion::Parameter* tseParam =
        conFusor.staticParameters_.getParameter(T_sensor_ee_.trans.data());
    if (tseParam)
      tseParam->unsetConstant();
    else
      std::cout << "WARNING: t_sensor_ee parameter not found for solving the "
                   "batch problem?"
                << std::endl;

    confusion::Parameter* qseParam = conFusor.staticParameters_.getParameter(
        T_sensor_ee_.rot.coeffs().data());
    if (qseParam)
      qseParam->unsetConstant();
    else
      std::cout << "WARNING: q_sensor_ee parameter not found for solving the "
                   "batch problem?"
                << std::endl;

    confusion::Parameter* tabParam =
        conFusor.staticParameters_.getParameter(T_armbase_body_.trans.data());
    if (tabParam)
      tabParam->unsetConstant();
    else
      std::cout << "WARNING: t_armbase_body parameter not found for solving "
                   "the batch problem?"
                << std::endl;

    confusion::Parameter* qabParam = conFusor.staticParameters_.getParameter(
        T_armbase_body_.rot.coeffs().data());
    if (qabParam)
      qabParam->unsetConstant();
    else
      std::cout << "WARNING: q_armbase_body parameter not found for solving "
                   "the batch problem?"
                << std::endl;

    confusion::Parameter* dtParam =
        conFusor.staticParameters_.getParameter(&dt_);
    if (dtParam)
      dtParam->unsetConstant();
    else
      std::cout
          << "WARNING: Dt parameter not found for solving the batch problem?"
          << std::endl;
  }

  // Reset to the desired state of the parameters after batch optimization
  void unsetParametersActiveAfterBatchProblem(confusion::ConFusor& conFusor) {
    std::vector<double*> params;
    if (!optJointAngleBias_) params.push_back(jointAngleBias_.data());
    if (!optSensorEeTransformation_) {
      params.push_back(T_sensor_ee_.trans.data());
      params.push_back(T_sensor_ee_.rot.coeffs().data());
    }
    if (!optArmBaseBodyTransformation_) {
      params.push_back(T_armbase_body_.trans.data());
      params.push_back(T_armbase_body_.rot.coeffs().data());
    }
    if (!optDt_) params.push_back(&dt_);

    conFusor.setStaticParamsConstant(params);
  }

  void printParameters() {
    std::cout << "Arm Odometry Calibration:";
    std::cout << "bq: " << jointAngleBias_.transpose() << "; dt=" << dt_
              << std::endl;
    T_armbase_body_.print("T_armbase_body");
    T_sensor_ee_.print("T_sensor_ee");
  }

  // Optimized arm parameters
  Eigen::Matrix<double, DOF - 2, 1>
      jointAngleBias_;  // Minus two because the first ans last joint biases get
                        // baked into the sensor offsets
  confusion::Pose<double> T_armbase_body_;
  confusion::Pose<double> T_sensor_ee_;
  double dt_;

  // Desired optimization parameter state
  bool optJointAngleBias_ = false;
  bool optSensorEeTransformation_ = false;
  bool optArmBaseBodyTransformation_ = false;
  bool optDt_ = false;

  Eigen::Matrix<double, DOF, DOF> cov_q_;
  confusion::ArmOdometryBuffer<DOF> armOdometryBuffer;
};

}  // namespace confusion

#endif  // CONFUSION_ARMODOMETRYCONFIG_H
