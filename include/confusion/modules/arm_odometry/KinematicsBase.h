//
// Created by tim on 01.10.19.
//

#ifndef CONFUSION_KINEMATICSBASE_H
#define CONFUSION_KINEMATICSBASE_H

#include <confusion/utilities/Pose.h>

#include <Eigen/Core>

namespace confusion {

/**
 * Provides a minimal interface to a robot's kinematics. Derived implementations
 * can use Robcogen, RBLD, or something custom.
 * @tparam DOF Number of joints in the arm
 */
template <int DOF>
class KinematicsBase {
 public:
  virtual confusion::Pose<double> computeForwardKinematics(
      const Eigen::Matrix<double, DOF, 1> q) = 0;
  virtual Eigen::Matrix<double, 6, DOF> computeJacobian(
      const Eigen::Matrix<double, DOF, 1> q) = 0;
};

}  // namespace confusion

#endif  // CONFUSION_KINEMATICSBASE_H
