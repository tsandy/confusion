//
// Created by tim on 03.09.19.
//

#ifndef CONFUSION_ARMODOMETRY_MEAS_H
#define CONFUSION_ARMODOMETRY_MEAS_H

#include <confusion/ConFusor.h>
#include <confusion/UpdateMeasurement.h>
#include <confusion/utilities/Pose.h>
#include <confusion/utilities/ceres_utils.h>
#include <confusion/utilities/distances.h>
#include <confusion/utilities/rotation_utils.h>

#include "confusion/modules/arm_odometry/ArmOdometryBuffer.h"
#include "confusion/modules/arm_odometry/ArmOdometryConfig.h"
#include "confusion/modules/arm_odometry/ArmOdometryCost.h"
#include "confusion/modules/arm_odometry/KinematicsBase.h"

namespace confusion {

/**
 * The JointOdometry module handles the use of arm kinemtics and joint encoder
 * measurements linking a robot body from to a sensor frame connected to the arm
 * end-effector.
 */
template <int DOF>
class ArmOdometryMeas : public confusion::UpdateMeasurement {
  friend ArmOdometryCost<DOF>;

 public:
  ArmOdometryMeas(double t, ArmOdometryConfig<DOF>& jointOdometryConfig,
                  confusion::KinematicsBase<DOF>& kinematics,
                  int measurementType, int eePoseParameterIndex = 0,
                  int basePoseParameterIndex = 2)
      : confusion::UpdateMeasurement(measurementType, t, "Odom", false),
        jointOdometryConfig_(jointOdometryConfig),
        kinematics_(kinematics),
        eePoseParameterIndex_(eePoseParameterIndex),
        basePoseParameterIndex_(basePoseParameterIndex) {}

  bool createCostFunction(std::unique_ptr<ceres::CostFunction>& costFunctionPtr,
                          std::unique_ptr<ceres::LossFunction>& lossFunctionPtr,
                          std::vector<size_t>& stateParameterIndexVector,
                          std::vector<double*>& staticParameterDataVector) {
    stateParameterIndexVector.push_back(eePoseParameterIndex_);  // T_w_ee
    stateParameterIndexVector.push_back(eePoseParameterIndex_ + 1);
    stateParameterIndexVector.push_back(basePoseParameterIndex_);  // T_w_b
    stateParameterIndexVector.push_back(basePoseParameterIndex_ + 1);

    staticParameterDataVector.push_back(
        jointOdometryConfig_.T_armbase_body_.trans.data());
    staticParameterDataVector.push_back(
        jointOdometryConfig_.T_armbase_body_.rot.coeffs().data());
    staticParameterDataVector.push_back(
        jointOdometryConfig_.T_sensor_ee_.trans.data());
    staticParameterDataVector.push_back(
        jointOdometryConfig_.T_sensor_ee_.rot.coeffs().data());
    staticParameterDataVector.push_back(
        jointOdometryConfig_.jointAngleBias_.data());
    staticParameterDataVector.push_back(&jointOdometryConfig_.dt_);
    std::cout << "t_a_b: " << jointOdometryConfig_.T_armbase_body_.trans.data()
              << ", q_a_b: "
              << jointOdometryConfig_.T_armbase_body_.rot.coeffs().data()
              << ", t_s_e: " << jointOdometryConfig_.T_sensor_ee_.trans.data()
              << ", q_s_e: "
              << jointOdometryConfig_.T_sensor_ee_.rot.coeffs().data()
              << ", b_a: " << jointOdometryConfig_.jointAngleBias_.data()
              << ", dt: " << &jointOdometryConfig_.dt_ << std::endl;

    std::unique_ptr<ceres::CostFunction> costFunctionPtr_(
        new ceres::AutoDiffCostFunction<ArmOdometryCost<DOF>, 6, 3, 4, 3, 4, 3,
                                        4, 3, 4, DOF - 2, 1>(
            new ArmOdometryCost<DOF>(this)));
    costFunctionPtr = std::move(costFunctionPtr_);

    //    lossFunctionPtr.reset();

    return true;
  }

  Eigen::Matrix<double, DOF, 1> q() {
    Eigen::Matrix<double, DOF, 1> q;
    jointOdometryConfig_.armOdometryBuffer.get(t(), bufferSeedIndex_, q);
    for (size_t i = 0; i < 5; ++i) {
      q(i + 1) += jointOdometryConfig_.jointAngleBias_(i);
    }
    return q;
  }

  // Note that this cannot be used while optimization is running!
  confusion::Pose<double> getTbe() {
    return kinematics_.computeForwardKinematics(q());
  }

  int residualDimension() { return 6; }

  ArmOdometryConfig<DOF>& jointOdometryConfig_;
  int bufferSeedIndex_ = -1;

  confusion::KinematicsBase<DOF>& kinematics_;

 private:
  template <int N>
  void setTbe(const confusion::Pose<double>& T_b_e_meas_double,
              const Eigen::Matrix<double, 7, DOF>& dTbe_dq,
              const Eigen::Matrix<ceres::Jet<double, N>, DOF, 1>& q,
              confusion::Pose<ceres::Jet<double, N>>& T_b_e_meas);

  void setTbe(const confusion::Pose<double>& T_b_e_meas_double,
              const Eigen::Matrix<double, 7, DOF>& dTbe_dq,
              const Eigen::Matrix<double, DOF, 1>& q,
              confusion::Pose<double>& T_b_e_meas);

  int eePoseParameterIndex_;  // The state parameter index for the translational
                              // ee pose. The next parameter should then be the
                              // orientation.
  int basePoseParameterIndex_;  // The state parameter index for the
                                // translational base pose. The next parameter
                                // should then be the orientation.
};

template <int DOF>
template <int N>
void ArmOdometryMeas<DOF>::setTbe(
    const confusion::Pose<double>& T_b_e_meas_double,
    const Eigen::Matrix<double, 7, DOF>& dTbe_dq,
    const Eigen::Matrix<ceres::Jet<double, N>, DOF, 1>& q,
    confusion::Pose<ceres::Jet<double, N>>& T_b_e_meas) {
  // We need to manually fill in the partial derivatives of Tbe
  // Get dq_dx from the q Jets
  ceres::Jet<double, 4> jet;
  size_t x_size = q(0).v.rows();
  Eigen::MatrixXd dq_dx(DOF, x_size);
  for (int i = 0; i < DOF; ++i) {
    dq_dx.row(i) = q(i).v.transpose();
  }

  Eigen::MatrixXd dTbe_dx(7, x_size);
  dTbe_dx = dTbe_dq * dq_dx;

  for (int i = 0; i < 3; ++i) {
    T_b_e_meas.trans(i).a = T_b_e_meas_double.trans(i);
    T_b_e_meas.trans(i).v = dTbe_dx.row(i).transpose();
  }

  // Be careful of the order here! Its [w,x,y,z]
  T_b_e_meas.rot.w().a = T_b_e_meas_double.rot.w();
  T_b_e_meas.rot.w().v = dTbe_dx.row(3).transpose();
  T_b_e_meas.rot.x().a = T_b_e_meas_double.rot.x();
  T_b_e_meas.rot.x().v = dTbe_dx.row(4).transpose();
  T_b_e_meas.rot.y().a = T_b_e_meas_double.rot.y();
  T_b_e_meas.rot.y().v = dTbe_dx.row(5).transpose();
  T_b_e_meas.rot.z().a = T_b_e_meas_double.rot.z();
  T_b_e_meas.rot.z().v = dTbe_dx.row(6).transpose();
}

template <int DOF>
void ArmOdometryMeas<DOF>::setTbe(
    const confusion::Pose<double>& T_b_e_meas_double,
    const Eigen::Matrix<double, 7, DOF>& dTbe_dq,
    const Eigen::Matrix<double, DOF, 1>& q,
    confusion::Pose<double>& T_b_e_meas) {
  T_b_e_meas = T_b_e_meas_double;
}

}  // namespace confusion

#endif  // CONFUSION_ARMODOMETRY_MEAS_H
