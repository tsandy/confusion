/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_ARMODOMETRYBUFFER_H_
#define CONFUSION_ARMODOMETRYBUFFER_H_

#include <ceres/ceres.h>
#include <confusion/utilities/ceres_utils.h>

#include <Eigen/Core>
#include <deque>
#include <mutex>

namespace confusion {

template <size_t N>
struct ArmOdometryBin {
  ArmOdometryBin(const double& t_, const Eigen::Matrix<double, N, 1>& x_)
      : t(t_), x(x_) {}
  void setDerivative(const Eigen::Matrix<double, N, 1>& x_dot_) {
    x_dot = x_dot_;
    derivativeComputed = true;
  }
  bool getStoredDerivative(Eigen::Matrix<double, N, 1>& x_dot_out) {
    if (!derivativeComputed) {
      return false;
    }
    x_dot_out = x_dot;
    return true;
  }
  double t;
  Eigen::Matrix<double, N, 1> x;

 private:
  bool derivativeComputed =
      false;  // Indicates if the derivative has been set for this measurement
  Eigen::Matrix<double, N, 1> x_dot;
};

/**
 * Multi-thread protected so one thread can add measurements, while the
 * estimator thread asynchronously calls pullInNewMeasurements, get, etc.
 * @tparam N
 */
template <size_t N>
class ArmOdometryBuffer {
 public:
  void addMeasurement(const double& t,
                      const Eigen::Matrix<double, N, 1>& meas) {
    std::lock_guard<std::mutex> lg(mtx_);
    pendingMeasurements_.push_back(ArmOdometryBin<N>(t, meas));
  }

  void pullInNewMeasurements() {
    std::lock_guard<std::mutex> lg(mtx_);

    // Make sure measurements are ordered in time
    while (!pendingMeasurements_.empty()) {
      if (!buffer_.empty() &&
          pendingMeasurements_.front().t <= buffer_.back().t) {
        std::cout
            << "[ArmOdometryBuffer::pullInNewMeasurements] Measurement "
               "received not ordered in time. Dropping the measurement. t_meas="
            << pendingMeasurements_.front().t << ", t_back=" << buffer_.back().t
            << std::endl;
        pendingMeasurements_.pop_front();
        continue;
      }
      buffer_.push_back(pendingMeasurements_.front());
      pendingMeasurements_.pop_front();
    }
  }

  /**
   * Get the interpolated measurement at the desired time. The user can seed the
   * search from the index at which the last query was found to speed up
   * repeated queries.
   * @param t Query time
   * @param seedIndex Index from which the search should start. -1 means start
   * from the back.
   * @param meas_out Interpolated measurement
   * @return Return true if an interpolated measurement was found. False if the
   * query time does not fall inside of the measurement time range.
   */
  bool get(const double& t, int& seedIndex,
           Eigen::Matrix<double, N, 1>& meas_out);

  template <int M>
  bool get(const ceres::Jet<double, M>& t, int& seedIndex,
           Eigen::Matrix<ceres::Jet<double, M>, N, 1>& meas_out);

  bool get(const double& t, Eigen::Matrix<double, N, 1>& meas_out) {
    int seed =
        -1;  // Minus one so that the check on terminal time is always performed
    return get(t, seed, meas_out);
  }

  /**
   * Check that the queried time falls within the acceptable range of odometry
   * measurements currently in the buffer. To be used to decide when to add
   * states or UpdateMeasurements.
   * @param t Queried time
   * @return true if the queried time is valid
   */
  bool checkValidTimestamp(const double& t) {
    if (buffer_.size() < 3 || t < buffer_[2].t ||
        t >= buffer_[buffer_.size() - 2].t)
      return false;
    return true;
  }

  /**
   * Remove measurements before the specified time
   * @param t
   */
  void removeMeasurements(const double& t) {
    while (!buffer_.empty() && buffer_.front().t < t) {
      buffer_.pop_front();
    }
    std::cout << "[ArmOdometryBuffer::removeMeasurements] Removing "
                 "measurements up to t="
              << t << ". New front measurement at t=" << buffer_.front().t
              << std::endl;
  }

  void clear() { buffer_.clear(); }

  ArmOdometryBin<N>& operator[](size_t pos) { return buffer_[pos]; }
  const ArmOdometryBin<N>& front() const { return buffer_.front(); }
  const ArmOdometryBin<N>& back() const { return buffer_.back(); }
  size_t size() const { return buffer_.size(); }

  bool getDerivative(const size_t& measIndex,
                     Eigen::Matrix<double, N, 1>& x_dot) {
    // Re-use the derivative previously computed if its available
    if (buffer_[measIndex].getStoredDerivative(x_dot)) return true;

    // Compute the derivative using central differences, with the samples
    // separated by a desired delta t
    // todo Better method! Filter signal then take the derivative directly from
    // the surrounding samples? Image mask-style averaging?
    double t_meas = buffer_[measIndex].t;
    // std::cout << "getDerivative for measIndex=" << measIndex << " of " <<
    // buffer_.size() << ". t_meas=" << t_meas << std::endl;

    if (measIndex >= buffer_.size()) {
      std::cout
          << "WARNING: Requested to get derivative of arm odometry "
             "measurement at a time later than the measurements in the buffer."
          << std::endl;
    }

    // Find the following measurement for derivative computation
    size_t followingIndex = std::min(measIndex, buffer_.size() - 1);
    while (buffer_[followingIndex].t <
           t_meas + derivativeTargetInterval_ / 2.0) {
      ++followingIndex;
      if (followingIndex == buffer_.size()) {
        // Use the last measurement
        --followingIndex;
        break;
      }
    }
    double t_following = buffer_[followingIndex].t;
    // std::cout << "followingIndex=" << followingIndex << ", t_following: " <<
    // t_following << std::endl;
    // Find the preceding measurement
    size_t precedingIndex = measIndex;
    while (t_following - buffer_[precedingIndex].t <
           derivativeTargetInterval_) {
      if (precedingIndex <= 0) {
        //        std::cout << "Can't get derivative of measurement because old
        //        enough measurement not found." << std::endl; return false;
        //        //todo Need to handle this case!!!
        precedingIndex = 0;
        break;
      }

      --precedingIndex;  // Be careful that it doesn't go negative
    }
    // std::cout << "precedingIndex=" << precedingIndex << ", t_preceding: " <<
    // buffer_[precedingIndex].t << std::endl;
    // Compute the derivative and store it for use later
    //    std::cout << "Computing derivative at " << t_meas << " with
    //    measurement of " << buffer_[precedingIndex].x.transpose() << " at " <<
    //    buffer_[precedingIndex].t << " and of " <<
    //    buffer_[followingIndex].x.transpose() << " at " <<
    //    buffer_[followingIndex].t << std::endl;
    x_dot = (buffer_[followingIndex].x - buffer_[precedingIndex].x) /
            (buffer_[followingIndex].t - buffer_[precedingIndex].t);
    buffer_[measIndex].setDerivative(x_dot);

    return true;
  }

 private:
  std::deque<ArmOdometryBin<N>> buffer_;

  // One thread adds new pending measurements. The estimator then pulls them
  // into the buffer when an optimization,etc is not running.
  std::mutex mtx_;
  std::deque<ArmOdometryBin<N>> pendingMeasurements_;

  Eigen::Matrix<double, N, 1> interpolate(
      const double& t,
      typename std::deque<ArmOdometryBin<N>>::const_iterator beforeMeasIter) {
    //		printf("t=%f, t0=%f, t1=%f",t,meas0.first,meas1.t);
    auto afterMeasIter = beforeMeasIter + 1;
    return beforeMeasIter->x + (afterMeasIter->x - beforeMeasIter->x) *
                                   (t - beforeMeasIter->t) /
                                   (afterMeasIter->t - beforeMeasIter->t);
  }

  double derivativeTargetInterval_ =
      0.05;  // Compute the derivative of the measurement with samples spread
             // over this time interval
};

template <size_t N>
bool ArmOdometryBuffer<N>::get(const double& t, int& seedIndex,
                               Eigen::Matrix<double, N, 1>& meas_out) {
  if (buffer_.empty()) {
    // printf("ArmOdometryBuffer is empty!\n");
    return false;
  }
  //    if (t < buffer_.front().t) {
  //      printf("Requested the joint angles at a time earlier than those
  //      stored"
  //             " in the buffer_. t_query=%15.4f, t_front=%15.4f\n", t,
  //             buffer_.front().t);
  //      return false;
  //    }

  if (seedIndex < 0 || seedIndex > buffer_.size() - 1) {
    seedIndex = (int)buffer_.size() - 1;

    // Also make sure that recent enough measurements have been received, since
    // this is the first time get has been called from the state at this time
    if (t + derivativeTargetInterval_ >
        buffer_.back()
            .t) {  // todo Give a little padding by requiring newer measurements
                   // past by the full derivative interval instead of half
      //      std::cout << "Recent enough odometry measurement not received.
      //      t_query=" << t + derivativeTargetInterval_/2.0 << "; t_back=" <<
      //      buffer_.back().t << std::endl;
      return false;
    }
  }

  // Find the element directly before the query time
  auto measIter = buffer_.cbegin() + seedIndex;
  bool movedBack = false;
  while (measIter->t > t) {
    if (measIter == buffer_.cbegin()) {
      // std::cout << "[ArmOdometryBuffer] Requested measurements at time t=" <<
      // t << " precedes the first measurement at t_front=" << measIter->t <<
      // std::endl;
      return false;
    }
    measIter--;
    seedIndex--;
    movedBack = true;
  }
  if (!movedBack) {
    bool movedForward = false;
    while (measIter->t < t) {
      measIter++;
      seedIndex++;
      movedForward = true;

      if (measIter == buffer_.cend()) {
        // std::cout << "[ArmOdometryBuffer] Requested measurements at time t="
        // << t << " is after the last measurement at at t_back=" <<
        // (measIter-1)->t << std::endl;
        return false;
      }
    }
    if (movedForward && measIter->t != t) {
      // Need to step back by one measurement after moving forward
      measIter--;
      seedIndex--;
    }
  }

  meas_out = interpolate(t, measIter);

  return true;
}

template <size_t N>
template <int M>
bool ArmOdometryBuffer<N>::get(
    const ceres::Jet<double, M>& t, int& seedIndex,
    Eigen::Matrix<ceres::Jet<double, M>, N, 1>& meas_out) {
  double t_double = confusion::getDouble(t);

  Eigen::Matrix<double, N, 1> meas_doubles;
  if (!get(t_double, seedIndex, meas_doubles)) return false;

  // Get q_dot
  Eigen::Matrix<double, N, 1> q_dot;
  if (!getDerivative(seedIndex, q_dot)) {
    std::cout << "[ArmOdometryBuffer::get] ERROR: getDerivative returned "
                 "false? t_query="
              << t << "; t_odom_front=" << buffer_.front().t
              << "; t_odom_back=" << buffer_.back().t << std::endl;
    abort();  // todo Do something better
  }

  for (size_t i = 0; i < N; ++i) {
    Eigen::Matrix<double, M, 1> jacobians = q_dot(i) * t.v;
    meas_out(i) = ceres::Jet<double, M>(
        meas_doubles(i), q_dot(i) * t.v);  // Filling in the Jacobians wrt dt
  }

  return true;
}

}  // namespace confusion

#endif /* CONFUSION_ARMODOMETRYBUFFER_H_ */
