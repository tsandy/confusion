//
// Created by tim on 03.09.19.
//

#ifndef CONFUSION_ARMODOMETRYCOST_H
#define CONFUSION_ARMODOMETRYCOST_H

#include <confusion/ConFusor.h>
#include <confusion/UpdateMeasurement.h>
#include <confusion/utilities/Pose.h>
#include <confusion/utilities/ceres_utils.h>
#include <confusion/utilities/distances.h>
#include <confusion/utilities/rotation_utils.h>

#include "confusion/modules/arm_odometry/ArmOdometryBuffer.h"
#include "confusion/modules/arm_odometry/ArmOdometryConfig.h"
#include "confusion/modules/arm_odometry/KinematicsBase.h"

namespace confusion {

/**
 * The JointOdometry module handles the use of arm kinemtics and joint encoder
 * measurements linking a robot body from to a sensor frame connected to the arm
 * end-effector.
 */
template <int DOF>
class ArmOdometryMeas;

template <int DOF>
class ArmOdometryCost {
 public:
  ArmOdometryCost(ArmOdometryMeas<DOF>* odometryMeas)
      : odometryMeas_(odometryMeas) {}

  // These functions are broken out this way to help with unit testing

  void computeForwardKinematics(const Eigen::Matrix<double, DOF, 1>& q,
                                confusion::Pose<double>& T_b_e_meas,
                                Eigen::Matrix<double, 7, DOF>& dTbe_dq) const;

  template <typename T>
  void computeResidual(const Eigen::Matrix<T, DOF, 1>& q,
                       const confusion::Pose<T>& T_b_e_est,
                       Eigen::Matrix<double, 6, DOF>& de_dq,
                       Eigen::Map<Eigen::Matrix<T, 6, 1>>& e) const;

  template <typename T>
  void computeWeightedResidual(const Eigen::Matrix<T, DOF, 1>& q,
                               const confusion::Pose<T>& T_b_e_est,
                               Eigen::Map<Eigen::Matrix<T, 6, 1>>& e) const;

  template <typename T>
  bool operator()(T const* t_w_ie_, T const* q_w_ie_, T const* t_w_ib_,
                  T const* q_w_ib_, T const* t_b_ib_, T const* q_b_ib_,
                  T const* t_ie_e_, T const* q_ie_e_, T const* b_q_,
                  T const* dt_, T* residual_) const;

  ArmOdometryMeas<DOF>* odometryMeas_;
};

template <int DOF>
void ArmOdometryCost<DOF>::computeForwardKinematics(
    const Eigen::Matrix<double, DOF, 1>& q, confusion::Pose<double>& T_b_e_meas,
    Eigen::Matrix<double, 7, DOF>& dTbe_dq) const {
  T_b_e_meas = odometryMeas_->kinematics_.computeForwardKinematics(q);

  // The Jacobian gives us ddRot_dq.
  Eigen::Matrix<double, 6, DOF> J =
      odometryMeas_->kinematics_.computeJacobian(q);

  Eigen::Matrix<double, 4, 3> dqbe_ddRot_temp =
      confusion::quaternionBoxPlusJacob(T_b_e_meas.rot);
  Eigen::Matrix<double, 4, 3> dqbe_ddRot;
  dqbe_ddRot.bottomRows(3) =
      dqbe_ddRot_temp.topRows(3);  // todo The row ordering is different to
                                   // QuatDistanceJacob!! Fix this??
  dqbe_ddRot.row(0) = dqbe_ddRot_temp.row(3);

  // Rows ordered [tx, ty, tz, qw, qx, qy, qz]
  dTbe_dq.topRows(3) = J.bottomRows(3);
  dTbe_dq.bottomRows(4) = dqbe_ddRot * J.topRows(3);
}

template <int DOF>
template <typename T>
void ArmOdometryCost<DOF>::computeResidual(
    const Eigen::Matrix<T, DOF, 1>& q, const confusion::Pose<T>& T_b_e_est,
    Eigen::Matrix<double, 6, DOF>& de_dq,
    Eigen::Map<Eigen::Matrix<T, 6, 1>>& e) const {
  // Compute FK
  Eigen::Matrix<double, DOF, 1> q_double;
  confusion::getDoubles(q.data(), DOF, q_double.data());
  confusion::Pose<double> T_b_e_meas_double;
  Eigen::Matrix<double, 7, DOF> dTbe_dq;
  computeForwardKinematics(q_double, T_b_e_meas_double, dTbe_dq);

  // Fill in the Jacobians for Tbe
  confusion::Pose<T> T_b_e_meas;
  odometryMeas_->setTbe(T_b_e_meas_double, dTbe_dq, q, T_b_e_meas);

  // Compute the residual
  confusion::QuatDistance(T_b_e_meas.rot, T_b_e_est.rot, e.data());
  confusion::VectorDistance(T_b_e_meas.trans.data(), T_b_e_est.trans.data(),
                            e.data() + 3);

  // Get de_dq, which is needed to get the residual weighting later
  Eigen::Matrix<double, 3, 4> de_dqbe;
  Eigen::Quaterniond q_b_e_est_double;
  confusion::getDoubles(T_b_e_est.rot.coeffs().data(), 4,
                        q_b_e_est_double.coeffs().data());
  confusion::calc_de_dq_left(T_b_e_meas_double.rot, q_b_e_est_double, de_dqbe);
  de_dq.topRows(3) = de_dqbe * dTbe_dq.bottomRows(4);
  de_dq.bottomRows(3) = dTbe_dq.topRows(3);

  // todo Can we use the Jacobian directly for derivatives? Is de_dqbe *
  // dqbe_ddRot always identity?
  //  Eigen::Matrix<double, 4, 3> dqbe_ddRot_temp =
  //  confusion::quaternionBoxPlusJacob(T_b_e_meas_double.rot);
  //  Eigen::Matrix<double, 4, 3> dqbe_ddRot;
  //  dqbe_ddRot.bottomRows(3) = dqbe_ddRot_temp.topRows(3); //todo The row
  //  ordering is different to QuatDistanceJacob!! Fix this?? dqbe_ddRot.row(0)
  //  = dqbe_ddRot_temp.row(3); std::cout << "J_r multiplied by:\n" << de_dqbe *
  //  dqbe_ddRot << std::endl;

#ifdef COST_DEBUG
  T_b_e_meas_double.print("T_b_e_meas");
  confusion::getDoublesPose(T_b_e_est).print("T_b_e_est");
  Eigen::Matrix<double, 6, 1> e_unweighed;
  confusion::getDoubles(e.data(), 6, e_unweighed.data());
  std::cout << "e_unweighed: " << e_unweighed.transpose() << std::endl;
#endif
}

template <int DOF>
template <typename T>
void ArmOdometryCost<DOF>::computeWeightedResidual(
    const Eigen::Matrix<T, DOF, 1>& q, const confusion::Pose<T>& T_b_e_est,
    Eigen::Map<Eigen::Matrix<T, 6, 1>>& e) const {
  Eigen::Matrix<double, 6, DOF> de_dq;
  computeResidual(q, T_b_e_est, de_dq, e);

  //  std::cout << "de_dq:\n" << de_dq << std::endl;

  // Propagate the joint encoder covariance through the cost function
  // We need the stiffness matrix: cov^(-1/2)
  // Note that we are assuming that S is nearly constant wrt bq
  Eigen::Matrix<double, 6, 6> cov_Tbe =
      de_dq * odometryMeas_->jointOdometryConfig_.cov_q_ * de_dq.transpose();
  //  std::cout << "cov_Tbe:\n" << cov_Tbe << std::endl;

  Eigen::Matrix<double, 6, 6> S;

  // [Option 1] Eigendecomposition todo Drop singular directions
  //  Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,6,6>> solver(cov_Tbe);
  //  std::cout << "Eigenvalues: " << solver.eigenvalues().transpose() <<
  //  std::endl; S = solver.operatorInverseSqrt(); std::cout << "Solver info: "
  //  << solver.info() << std::endl;

  // [Option 2] LLT todo Should use U instead of L? Not working at
  // singularities?
  Eigen::Matrix<double, 6, 6> information = cov_Tbe.inverse();
  information = 0.5 * information + 0.5 * information.transpose().eval();
  Eigen::LLT<Eigen::Matrix<double, 6, 6>> llt(information);
  S = llt.matrixL().transpose();

  // [Option 3] Pre-conditioned SVD matrix inversion
//  Eigen::MatrixXd p = (cov_Tbe.diagonal().array()
//  > 1.0e-9).select(cov_Tbe.diagonal().cwiseSqrt(),1.0e-3); Eigen::MatrixXd
//  p_inv = p.cwiseInverse();
//
//  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> saes(0.5  *
//  p_inv.asDiagonal() * (cov_Tbe + cov_Tbe.transpose())  * p_inv.asDiagonal());
//  if (saes.info() != Eigen::Success) {
//    std::cout << "\n-----Eigendecomposition of cov_Tbe was not successful in
//    ArmOdometryCost!" << std::endl; std::cout << "cov_Tbe:\n" << cov_Tbe <<
//    std::endl;
//  }
//
//  //Pre-conditioned SVD matrix inversion
//  static const double epsilon = std::numeric_limits<double>::epsilon();
//  double tolerance = epsilon * 6 * saes.eigenvalues().array().maxCoeff();
//  Eigen::MatrixXd lambda = (saes.eigenvalues().array() >
//  tolerance).select(saes.eigenvalues().array(), 0); Eigen::MatrixXd
//  lambda_sqrt = lambda.cwiseSqrt();
////std::cout << "tolerance: " << tolerance << "; eigenvalues: " <<
///saes.eigenvalues().transpose() << std::endl; /std::cout << "eigenvectors:\n"
///<< saes.eigenvectors() << std::endl; /std::cout << "p:\n" << p << std::endl;
////std::cout << "S_sqrt:\n" << S_sqrt << std::endl;
//  // Stiffness matrix from Cov = S^T S
//  S = (p.asDiagonal() * saes.eigenvectors() *
//  (lambda_sqrt.asDiagonal())).transpose();

//  std::cout << "information:\n" << information << std::endl;
#ifdef COST_DEBUG
  std::cout << "S:\n" << S << std::endl;
#endif

#ifndef UNIT_TEST
  e = S.cast<T>() * e;  // Don't apply the weighting in the unit test since it
                        // adds inaccuracy to the derivatives
#endif
}

template <int DOF>
template <typename T>
bool ArmOdometryCost<DOF>::operator()(T const* t_w_ie_, T const* q_w_ie_,
                                      T const* t_w_ib_, T const* q_w_ib_,
                                      T const* t_b_ib_, T const* q_b_ib_,
                                      T const* t_ie_e_, T const* q_ie_e_,
                                      T const* b_q_, T const* dt_,
                                      T* residual_) const {
  //#ifdef COST_DEBUG
  //  std::cout << "Starting OdometryCost computation" << std::endl;
  //#endif
  Eigen::Matrix<T, 3, 1> t_w_ie(t_w_ie_);
  Eigen::Quaternion<T> q_w_ie(q_w_ie_);
  confusion::Pose<T> T_w_ie(t_w_ie, q_w_ie);

  Eigen::Matrix<T, 3, 1> t_w_ib(t_w_ib_);
  Eigen::Quaternion<T> q_w_ib(q_w_ib_);
  confusion::Pose<T> T_w_ib(t_w_ib, q_w_ib);

  Eigen::Matrix<T, 3, 1> t_b_ib(t_b_ib_);
  Eigen::Quaternion<T> q_b_ib(q_b_ib_);
  confusion::Pose<T> T_b_ib(t_b_ib, q_b_ib);

  Eigen::Matrix<T, 3, 1> t_ie_e(t_ie_e_);
  Eigen::Quaternion<T> q_ie_e(q_ie_e_);
  confusion::Pose<T> T_ie_e(t_ie_e, q_ie_e);

  Eigen::Matrix<T, DOF, 1> b_q;
  b_q << T(0), b_q_[0], b_q_[1], b_q_[2], b_q_[3], b_q_[4], T(0);

  T t = T(odometryMeas_->t()) + dt_[0];

  Eigen::Matrix<T, DOF, 1> q;
  if (!odometryMeas_->jointOdometryConfig_.armOdometryBuffer.get(
          t, odometryMeas_->bufferSeedIndex_, q)) {
    std::cout << "ERROR: Getting odometry measurement failed while computing "
                 "residual. t="
              << odometryMeas_->t() << ", dt=" << dt_[0] << ", t_front="
              << odometryMeas_->jointOdometryConfig_.armOdometryBuffer.front().t
              << ", t_back="
              << odometryMeas_->jointOdometryConfig_.armOdometryBuffer.back().t
              << std::endl;
    return false;
  }
  q += b_q;
  //  std::cout << "q: " << q << std::endl;

  confusion::Pose<T> T_b_e_est = T_b_ib * T_w_ib.inverse() * T_w_ie * T_ie_e;

  Eigen::Map<Eigen::Matrix<T, 6, 1>> e(residual_);

  computeWeightedResidual(q, T_b_e_est, e);

#ifdef COST_DEBUG
  Eigen::Matrix<double, 6, 1> res;
  confusion::getDoubles(residual_, 6, res.data());
  std::cout << "odometry cost: " << res.transpose() << std::endl;
#endif
  return true;
}

}  // namespace confusion

#endif  // CONFUSION_ARMODOMETRYCOST_H
