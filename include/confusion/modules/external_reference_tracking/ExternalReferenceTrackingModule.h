/*
 * Copyright 2018 Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_ERTMODULE_H_
#define INCLUDE_CONFUSION_ERTMODULE_H_

#include <confusion/BatchFusor.h>
#include <confusion/ConFusor.h>
#include <confusion/Logger.h>
#include <confusion/utilities/utilities.h>

#include <Eigen/Core>
#include <boost/circular_buffer.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <mutex>
#include <thread>

#include "confusion/models/PoseMeas.h"
#include "confusion/models/PositionMeas.h"

namespace confusion {

class ExternalReferenceTrackingModule {
 public:
  enum GravityAlignmentStrategy {
    ERT_OPT_GRAVITY = 0,  // Optimize the 2dim direction of the gravity vector
    ERT_ROTATE_FRAME  // Optimize 2dim of the attitude of the first reference
                      // frame to give the gravity alignment
  };

  ExternalReferenceTrackingModule(ConFusor& conFusor,
                                  const boost::property_tree::ptree& pt,
                                  int poseMeasIndex,
                                  bool* newMeasReceivedFlag = nullptr);

  ~ExternalReferenceTrackingModule() {}

  bool initializeSensorframeOffset(std::string sensorname);
  // Used to initialize ROS subscribers in the derived class
  virtual void initializeSensorframeOffserDerived(
      const std::pair<const std::string, boost::property_tree::ptree>&
          subTree) {}

  // Return true if the measurement is added for fusion
  bool addMeasurement(const double& t, const std::string& referenceFrameName,
                      const std::string& sensorFrameName,
                      const confusion::Pose<double>& T_wa_ba);

  // todo Careful! These can only be called from the Estimation thread. Is there
  // a good way to ensure this?
  void postprocessingAfterOptimization(
      const confusion::StateVector* stateVector);

  void printParameters();

  void initializeFirstRefFrameOffset(
      const std::shared_ptr<ErtMeasBase>& ertMeasPtr,
      const confusion::Pose<double>& T_w_ref, GravityAlignmentStrategy grav);
  bool processPoseMeasBeforeAssigningToState(
      const std::shared_ptr<ErtMeasBase>& ertMeasPtr, const double& t,
      const Pose<double>& T_w_body);

  // This is just here to allow the derived Ros version to publish a tf for the
  // raw measurements
  virtual void processPoseMeasBeforeAssigningToStateDerived(
      const std::shared_ptr<ErtMeasBase>& ertMeasPtr, const double& t,
      const Pose<double>& T_w_body) {}

  // Can be called asynchronously to ConFusor
  void resetSensor(const std::string& sensorName);

  // Only call from estimator thread!
  void resetSensorNow(const std::string& sensorName);

  // Used to configure the sensor frame offsets for extrinsic calibration
  // Note that this should not be called while sensor fusion is running, since
  // it modifies that state of the optimized parameters (possibly)
  // asynchronously.
  void setExtrinsicsUnconstForBatchSolve();
  void resetExtrinsicsConstAfterBatchSolve();

  void setVerbose(bool verbose) { verbose_ = verbose; }

  confusion::ConFusor& conFusor_;

  bool referenceFrameOffsetsEmpty() const {
    return referenceFrameOffsets_.empty();
  }
  bool getReferenceFrameOffset(const std::string& frameName,
                               confusion::Pose<double>& T_world_ref) const;
  bool getSensorFrameOffset(const std::string& frameName,
                            confusion::Pose<double>& T_body_sensor) const;
  bool getSensorScale(const std::string& frameName, double& scale) const;

  std::map<std::string, std::shared_ptr<confusion::Pose<double>>>*
  getReferenceFrameOffsetsMapPointer() {
    return &referenceFrameOffsets_;
  }

  // Can be used to update the initial guess of a sensor frame offset at run
  // time. Will only be accepted if that sensor isn't already active.
  void setSensorFrameOffsetInitialGuess(
      const std::string& sensorFrameName,
      const confusion::Pose<double>& T_body_sensor);
  std::vector<std::string> getSensorFrameNames();

 protected:
  boost::property_tree::ptree pt_;

  // Copy of the optimized parameters for asynchronous queries during
  // optimization
  mutable std::mutex parameterQueryMtx_;
  std::map<std::string, confusion::Pose<double>> referenceFrameOffsetsCopy_;
  std::map<std::string, confusion::Pose<double>> sensorFrameOffsetsCopy_;
  std::map<std::string, double> sensorPoseMeasScalesCopy_;

  void resetSensorInternal(
      const std::string& sensorName);  // Only call from estimator thread!
  void disableSensor(const std::string& sensorName);
  void activateSensor(
      const std::string& sensorName, const std::string& referenceFrameName,
      const std::shared_ptr<confusion::Pose<double>>& T_w_ref_init,
      bool useWorldFixedParameterization = false);

  // Vector of offsets from external reference frames to the estimator world
  // frame (T_world_ref). A new element is only added when that reference frame
  // has been initialized and the sensor is being used for fusion.
  // todo Put everything in one bin so that fewer lookups are required.

  // Indexed by reference frame name
  std::map<std::string, std::shared_ptr<confusion::Pose<double>>>
      referenceFrameOffsets_;

  // Vector of offsets from the body to other sensor frames (T_body_sensor),
  // indexed by sensor frame name. New elements are added when the first
  // measurements are received, even if the sensor isn't yet being used.
  std::map<std::string, std::shared_ptr<confusion::Pose<double>>>
      sensorFrameOffsets_;
  std::map<std::string, std::shared_ptr<double>> sensorPoseMeasScales_;
  std::map<std::string, std::shared_ptr<confusion::ErtMeasConfig>>
      sensorPoseMeasConfigs_;

  // This can be used to indicate when new tag measurements have been added to
  // the ConFusor to tell ongoing optimizations to stop early
  bool* newMeasReceivedFlag_ = nullptr;
  int poseMeasIndex_;

  bool verbose_ = false;
  bool optGravity_ = false;

  // For resetting sensors in a thread-safe manner
  std::mutex sensorResetMutex_;
  std::deque<std::string> sensorsToReset_;

  // For handling which sensor/reference frame defines that static world
  // reference
  bool ertFrameDefinesStaticWorld_ = false;
  std::string sensorThatDefinesStaticWorld_;
  GravityAlignmentStrategy gravityAlignmentStrategy_;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_ERTMODULE_H_ */
