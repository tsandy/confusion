/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_IMUMODULE_H
#define CONFUSION_IMUMODULE_H

#include <boost/property_tree/ptree.hpp>

#include "confusion/ConFusor.h"
#include "confusion/models/ImuMeas.h"
#include "confusion/utilities/ImuPropagator.h"

namespace confusion {

class ImuModule {
 public:
  ImuModule(ConFusor& conFusor, boost::property_tree::ptree& pt,
            bool forwardPropagate);

  void imuCallback(const std::shared_ptr<ImuMeas>& imuMeas);

  void setImuStateEstimate(const ImuStateParameters& estState);
  bool getImuStateAtTime(const double& t, ImuStateParameters& propState);

 private:
  ConFusor& conFusor_;

  ImuPropagator imuPropagator_;
  bool forwardPropagateState_ =
      false;  ///< Determines if IMU measurements are buffered for forward
              ///< propagation asynchronous to optimizations
  bool tracking_ = false;

  Eigen::Vector2d gravity_rot_;
  ImuCalibration imuCalibration_;
};

}  // namespace confusion

#endif  // CONFUSION_IMUMODULE_H
