/*
 * april_tag_detector.hpp
 *
 * Uses functions from feature_detection node written by TSandy
 * (https://bitbucket.org/adrlab/feature_extraction)
 *
 *  Created on: Jun 2, 2017
 *      Author: Manuel Lussi <mlussi@ethz.ch>
 */

#ifndef APRIL_TAG_DETECTOR_HPP_
#define APRIL_TAG_DETECTOR_HPP_

#include "confusion/modules/apriltag/external/apriltag.h"
#include "confusion/modules/apriltag/external/tag36h11.h"
#include "confusion/modules/apriltag/external/tag36h10.h"
#include "confusion/modules/apriltag/external/tag36artoolkit.h"
#include "confusion/modules/apriltag/external/tag25h9.h"
#include "confusion/modules/apriltag/external/tag25h7.h"

#include <iostream>
#include <vector>
#include <string>
#include <Eigen/Core>

#include "opencv2/opencv.hpp"

struct TagDetection {
	int id;
	double corners[4][2];
	int image_seq;

	TagDetection();
	TagDetection(const apriltag_detection_t& det, int seq_);
};

void convertTagDetections(zarray_t* detections, std::vector<TagDetection>& tagDetections, int seq = -1);

//This is copied from the external version used in rcars because it was easier to understand
Eigen::Matrix4d getRelativeTransform(TagDetection td, double tag_size,
		double fx, double fy, double cx, double cy);


class AprilTagDetector {
public:
	AprilTagDetector(const std::string& tagFamily, int numThreads);
	~AprilTagDetector();
	void detectTags(const cv::Mat& img, std::vector<TagDetection>& tagDetections, int seq = -1);
	void drawTags(cv::Mat& img, const std::vector<TagDetection>& tagDetections);

	apriltag_detector_t* td;
	apriltag_family_t* tf;
};


#endif /* APRIL_TAG_DETECTOR_HPP_ */
