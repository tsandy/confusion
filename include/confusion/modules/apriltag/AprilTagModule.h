/*
 * Copyright 2018 Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_EXAMPLES_APRILTAGINTERFACE_H_
#define INCLUDE_CONFUSION_EXAMPLES_APRILTAGINTERFACE_H_

#include <confusion/utilities/utilities.h>

#include <Eigen/Core>
#include <boost/circular_buffer.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <mutex>
#include <thread>

#include "confusion/ConFusor.h"
#include "confusion/models/TagMeas.h"
#include "confusion/modules/apriltag/apriltag_utils.h"
#include "confusion/modules/apriltag/external/april_tag_detector.hpp"

namespace confusion {

class AprilTagModule {
 public:
  typedef std::map<std::string, std::shared_ptr<confusion::Pose<double>>>
      FrameOffsetMap;

  AprilTagModule(ConFusor* conFusorPtr, std::string dataFilePath,
                 const std::string& configFileName, int tagMeasIndex_,
                 bool* newMeasReceivedFlag = nullptr);

  ~AprilTagModule();

  void setCameraCalibration(const Eigen::Matrix<double, 3, 4>& projMat);

  void processTagDetections(double t, std::vector<TagDetection>& tagDetections);

  void imageCallback(const double& t, const cv::Mat& image);
  void tagDetection(const double& t, const cv::Mat& image);

  void copyOutEstimateAfterOptimization();
  void projectTagsIntoImage(const confusion::Pose<double>& T_w_i,
                            const std::string& cameraName, cv::Mat& image);

  void setVerbose(bool verbose) { verbose_ = verbose; }

  std::map<std::string, std::shared_ptr<confusion::Pose<double>>>*
  getTagReferenceFrameOffsetsMapPointer() {
    return &referenceFrameOffsets_;
  }

  ConFusor* conFusorPtr_;

  AprilTagDetector tagDetector_;
  AprilTagParameters aprilTagParameters_;

  // These maps do the bookkeeping on the various frame offsets for the fusion
  // problem.
  // todo These should be private.
  FrameOffsetMap
      referenceFrameOffsets_;  // Vector of offsets from external reference
                               // frames to the estimator world frame
  FrameOffsetMap sensorFrameOffsets_;  // Vector of offsets from the IMU to
                                       // other sensor frames

  // This can be used to indicate when new tag measurements have been added to
  // the ConFusor to tell ongoing optimizations to stop early
  bool* newMeasReceivedFlag_ = nullptr;

 private:
  // Copy optimized parameters here for asynchonous queries (e.g. for
  // visualization at high rate)
  std::mutex estimateMutex_;
  std::map<std::string, confusion::Pose<double>> referenceFrameOffsetsOut_;
  std::map<std::string, confusion::Pose<double>> sensorFrameOffsetsOut_;

  std::string confusionPath_;
  int tagMeasIndex_;
  bool tagDetectionRunning_ = false;
  bool verbose_ = false;
  double maxImageDt_ = 0.1;  //[Hz]
  double t_last_image_ = 0.0;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_EXAMPLES_APRILTAGINTERFACE_H_ */
