/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_TEST_APRILTAG_UTILS_H_
#define CONFUSION_TEST_APRILTAG_UTILS_H_

#include <Eigen/Core>
#include <memory>
#include <opencv2/opencv.hpp>
#include <vector>

#include "confusion/utilities/Pose.h"

namespace confusion {

struct TagMeasCalibration {
  Eigen::Matrix<double, 3, 4> projMat_;
  std::array<Eigen::Vector3d, 4> t_t_corner_;
  double w_cx_;
  double w_cy_;
  bool tagUseLossFunction_ = false;
  double tagLossCoefficient_ = 0.0;
  double tagSize_;
  double ftr_init_stddev_;
};

struct AprilTagParameters {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  void initialize();

  bool initialized_ = false;

  // The index of the translational parameter tied to the imu frame rigidly
  // connected to the camera used to observe the tags
  int stateParameterIndex_ = 0;
  double tag_corner_stddev_;

  double t_c_i_init_stddev;  //[m]
  double q_c_i_init_stddev;  //[rad]

  std::string imuFrameName;
  std::string cameraFrameName;

  TagMeasCalibration tagMeasCalibration_;
};

Eigen::Matrix4d getRelativeTransform(
    const std::array<std::array<double, 2>, 4>& corners, double tag_size,
    double fx, double fy, double cx, double cy);

bool readTagPoses(const std::string& fname,
                  std::map<std::string, std::shared_ptr<Pose<double>>>&
                      externalReferenceFrames);

// Note that this does not write the gravity vector orientation when OPT_GRAVITY
// is defined in ImuState.h
void writeTagPoses(const std::string& fname,
                   const std::map<std::string, std::shared_ptr<Pose<double>>>&
                       externalReferenceFrames);

}  // namespace confusion

#endif /* CONFUSION_TEST_APRILTAG_UTILS_H_ */
