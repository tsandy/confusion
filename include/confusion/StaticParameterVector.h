/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_STATICPARAMETERVECTOR_H_
#define INCLUDE_CONFUSION_STATICPARAMETERVECTOR_H_

#include <list>
#include <map>

#include "confusion/Parameter.h"

namespace confusion {

class StaticParameterVector {
 public:
  StaticParameterVector() = default;

  // Copy constructor only used in BatchFusor.
  StaticParameterVector(const StaticParameterVector& sp);

  StaticParameterVector& operator=(const StaticParameterVector& spv);

  size_t numParameters() const;

  void addParameter(const confusion::Parameter& param);

  confusion::Parameter* getParameter(double* dataPtr);

  void deactivateParameters();

  void removeParameter(double* dataPtr);

  void attachStaticParameterRandomWalkProcess(double* data,
                                              const double& processNoise);

  void detachStaticParameterRandomWalkProcess(double* data);

  void print() const;

  bool check() const;

  size_t globalSize() const;
  std::size_t size() const;

  std::map<double*, confusion::Parameter>::iterator begin();
  std::map<double*, confusion::Parameter>::iterator end();
  std::map<double*, confusion::Parameter>::const_iterator begin() const;
  std::map<double*, confusion::Parameter>::const_iterator end() const;

 private:
  std::map<double*, confusion::Parameter> parameterMap_;
  size_t globalSize_ = 0;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_STATICPARAMETERVECTOR_H_ */
