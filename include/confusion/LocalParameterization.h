/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_LOCALPARAMETERIZATION_H_
#define INCLUDE_CONFUSION_LOCALPARAMETERIZATION_H_

#include <ceres/ceres.h>

#include <Eigen/Core>
#include <string>
#include <vector>

#include "confusion/utilities/distances.h"
#include "confusion/utilities/rotation_utils.h"

namespace confusion {

// todo Could template on the local and global sizes to make the Jacobians fixed
// size.

/**
 * @brief Define a local parameterization for a parameter block.
 *
 * A base class for parameter block local parameterizations. This extends the
 * LocalParameterization of Ceres (see the relevent section of their
 * documentation) to also include a boxMinus function for use to express the
 * change in parameters in local space in the PriorCost. See QuatParam and its
 * unit tests for an example implementation and usage.
 */
class LocalParameterizationBase : public ceres::LocalParameterization {
 public:
  /**
   * @brief Computes the distance between two parameter values in local
   * (tangent) space.
   * @param xi Left-hand-side parameter
   * @param xj Right-hand-side parameter
   * @param xi_minus_xj LHS [boxminus] RHS
   */
  virtual void boxMinus(const double* xi, const double* xj,
                        double* xi_minus_xj) const = 0;

  /**
   * @brief Computes the derivative of the boxminus operation with respect to
   * the left-hand-side parameter
   * @param xi Left-hand-side parameter
   * @param xj Right-hand-side parameter
   * @return The derivative of LHS [boxminus] RHS with repsect to LHS parameter
   */
  virtual Eigen::MatrixXd boxMinusJacobianLeft(double const* xi,
                                               double const* xj) const = 0;

  /**
   * @brief Computes the derivative of the boxminus operation with respect to
   * the right-hand-side parameter
   * @param xi Left-hand-side parameter
   * @param xj Right-hand-side parameter
   * @return The derivative of LHS [boxminus] RHS with repsect to RHS parameter
   */
  virtual Eigen::MatrixXd boxMinusJacobianRight(double const* xi,
                                                double const* xj) const = 0;
};

class QuatParam : public LocalParameterizationBase {
 public:
  ~QuatParam() {}

  bool Plus(const double* x_raw, const double* delta_raw,
            double* x_plus_delta_raw) const;

  bool ComputeJacobian(const double* x, double* jacobian) const;

  int GlobalSize() const { return 4; }
  int LocalSize() const { return 3; }

  void boxMinus(double const* xi, double const* xj, double* dist) const;

  Eigen::MatrixXd boxMinusJacobianLeft(double const* xi,
                                       double const* xj) const;

  Eigen::MatrixXd boxMinusJacobianRight(double const* xi,
                                        double const* xj) const;
};

class FixedYawParameterization : public LocalParameterizationBase {
 public:
  ~FixedYawParameterization() {}

  bool Plus(const double* x_raw, const double* delta_raw,
            double* x_plus_delta_raw) const;

  bool ComputeJacobian(const double* x, double* jacobian) const;

  int GlobalSize() const { return 4; }
  int LocalSize() const { return 2; }

  void boxMinus(double const* xi, double const* xj, double* dist) const;

  Eigen::MatrixXd boxMinusJacobianLeft(double const* xi,
                                       double const* xj) const;

  Eigen::MatrixXd boxMinusJacobianRight(double const* xi,
                                        double const* xj) const;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_LOCALPARAMETERIZATION_H_ */
