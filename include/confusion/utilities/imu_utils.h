/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_IMU_UTILS_H_
#define INCLUDE_CONFUSION_IMU_UTILS_H_

#include <Eigen/Geometry>
#include <deque>
#include <memory>

#include "confusion/ProcessMeasurement.h"
#include "confusion/models/ImuMeas.h"
#include "confusion/utilities/Pose.h"
#include "confusion/utilities/rotation_utils.h"

namespace confusion {

struct ImuStateParameters {
  double t() { return t_; }

  void print(const std::string& prefix = "") const {
    std::cout << "ImuStateParameters at t=" << t_ << "\n";
    T_w_i_.print();
    std::cout << "angVel: " << angVel_.transpose() << std::endl;
    std::cout << "linVel: " << linVel_.transpose() << std::endl;
    std::cout << "accelBias: " << accelBias_.transpose() << std::endl;
    std::cout << "gyroBias: " << gyroBias_.transpose() << std::endl;
  }

  double t_;
  Pose<double> T_w_i_;
  Eigen::Vector3d angVel_;
  Eigen::Vector3d linVel_;
  Eigen::Vector3d accelBias_;
  Eigen::Vector3d gyroBias_;
};

/**
 * Forward propagate an orientation q0 by a measured angular velocity w_i (with
 * an additive bias b_g), over the time intercal of length dt. Derivatives wrt
 * q0, w_i, and b_g computed in parallel.
 * @param q0 Initial orientation
 * @param b_g gyroscope bias, expressed in body-fixed frame
 * @param w_i Measured angular velociy in body-fixed frame
 * @param dt Time interval [sec]
 * @param q1 Returns the orientation at time t0+dt (the propagated q is returned
 * separately since the prior orientation needs to be used later in the
 * integrate function)
 * @param dq1_dq0 Returns the derivative of the resulting orientation wrt the
 * initial orientation
 * @param dq1_dwi Returns the derivative of the resulting orientation wrt the
 * angular velocity measurement
 * @param dq1_dbg Returns the derivative of the resulting orientation wrt the
 * gyro bias
 */
template <typename T>
void propagate_q(Eigen::Quaternion<T>& q0, const Eigen::Matrix<T, 3, 1>& b_g,
                 const Eigen::Matrix<T, 3, 1>& w_i, const T& dt,
                 Eigen::Quaternion<T>& q1, Eigen::Matrix<T, 4, 4>& dq1_dq0,
                 Eigen::Matrix<T, 4, 3>& dq1_dwi,
                 Eigen::Matrix<T, 4, 3>& dq1_dbg);

/**
 * Forward propagate an orientation q0 by a measured angular velocity w_i (with
 * an additive bias b_g), over the time intercal of length dt. Derivatives wrt
 * q0, w_i, and b_g computed in parallel.
 * @param q0 Initial orientation
 * @param b_g gyroscope bias, expressed in body-fixed frame
 * @param w_i Measured angular velociy in body-fixed frame
 * @param dt Time interval [sec]
 * @param q1 Returns the orientation at time t0+dt (the propagated q is returned
 * separately since the prior orientation needs to be used later in the
 * integrate function)
 * @param ddq1_ddq0 Returns the derivative of the resulting orientation wrt the
 * initial orientation
 * @param ddq1_dwi Returns the derivative of the resulting orientation wrt the
 * angular velocity measurement
 * @param ddq1_dbg Returns the derivative of the resulting orientation wrt the
 * gyro bias
 */
template <typename T>
void propagate_q_tangentSpace(Eigen::Quaternion<T>& q0,
                              const Eigen::Matrix<T, 3, 1>& b_g,
                              const Eigen::Matrix<T, 3, 1>& w_i, const T& dt,
                              Eigen::Quaternion<T>& q1,
                              Eigen::Matrix<T, 3, 3>& ddq1_ddq0,
                              Eigen::Matrix<T, 3, 3>& ddq1_dwi,
                              Eigen::Matrix<T, 3, 3>& ddq1_dbg);

/**
 * Forward propagate an orientation q0 by a measured angular velocity w_i (with
 * an additive bias b_g), over the time intercal of length dt. No derviatives
 * computed in parallel.
 * @param q0 Initial orientation
 * @param b_g gyroscope bias, expressed in body-fixed frame
 * @param w_i Measured angular velociy in body-fixed frame
 * @param dt Time interval [sec]
 * @param q1 Returns the orientation at time t0+dt (the propagated q is returned
 * separately since the prior orientation needs to be used later in the
 * integrate function)
 */
template <typename T>
void propagate_q_no_jacob(Eigen::Quaternion<T>& q0,
                          const Eigen::Matrix<T, 3, 1>& b_g,
                          const Eigen::Matrix<T, 3, 1>& w_i, const T& dt,
                          Eigen::Quaternion<T>& q1);

// This one uses trapezoidal integration
template <typename T>
void propagate_q_no_jacob_trap(Eigen::Quaternion<T>& q0,
                               const Eigen::Matrix<T, 3, 1>& b_g,
                               const Eigen::Matrix<T, 3, 1>& w0_,
                               const Eigen::Matrix<T, 3, 1>& w1_, const T& dt,
                               Eigen::Quaternion<T>& q1);

/**
 * Forward propagate the velocity of a body, expressed in a gravity-fixed frame
 * by linear acceleration measurement a_i (with an additive bias b_a and
 * expressed in a body-fixed frame) over the time dt. Does not compute
 * jacobians.
 * @param v ///< Velocity at time t0
 * @param q ///< Orientation at time t0, expressed in world frame
 * @param b_a ///< Accelerometer bias, expressed in body-fixed frame [m/sec2]
 * @param a_i ///< Linear acceleration measured, expressed in body-fixed frame
 * [m/sec2]
 * @param g_w ///< Gravity expressed in world frame [m/sec2]
 * @param dt ///< Time interval [sec]
 */
template <typename T>
void propagate_v_no_jacob(Eigen::Matrix<T, 3, 1>& v,
                          const Eigen::Quaternion<T>& q,
                          const Eigen::Matrix<T, 3, 1>& b_a,
                          const Eigen::Matrix<T, 3, 1>& a_i,
                          const Eigen::Matrix<T, 3, 1>& g_w, const T& dt);

// This one used trapezoidal integration
template <typename T>
void propagate_v_no_jacob_trap(Eigen::Matrix<T, 3, 1>& v,
                               const Eigen::Quaternion<T>& q0,
                               const Eigen::Quaternion<T>& q1,
                               const Eigen::Matrix<T, 3, 1>& b_a,
                               const Eigen::Matrix<T, 3, 1>& a_0,
                               const Eigen::Matrix<T, 3, 1>& a_1,
                               const Eigen::Matrix<T, 3, 1>& g_w, const T& dt);

/**
 * Forward propagate the velocity of a body, expressed in a gravity-fixed frame
 * by linear acceleration measurement a_i (with an additive bias b_a and
 * expressed in a body-fixed frame) over the time dt. Computes the derivatives
 * of v1 wrt q, b_a, and a_i.
 * @param v ///< Velocity at time t0
 * @param q ///< Orientation at time t0, expressed in world frame
 * @param b_a ///< Accelerometer bias, expressed in body-fixed frame [m/sec2]
 * @param a_i ///< Linear acceleration measured, expressed in body-fixed frame
 * [m/sec2]
 * @param g_w ///< Gravity expressed in world frame [m/sec2]
 * @param dt ///< Time interval [sec]
 * @param dv1_dq ///< Derivative of v1 wrt body orientation
 * @param dv1_dba ///< Derivative of v1 wrt accel bias
 * @param dv1_dai ///< Derivative of v1 wrt acceleration measurement
 */
template <typename T>
void propagate_v(Eigen::Matrix<T, 3, 1>& v, const Eigen::Quaternion<T>& q,
                 const Eigen::Matrix<T, 3, 1>& b_a,
                 const Eigen::Matrix<T, 3, 1>& a_i,
                 const Eigen::Matrix<T, 3, 1>& g_w, const T& dt,
                 Eigen::Matrix<T, 3, 4>& dv1_dq,
                 Eigen::Matrix<T, 3, 3>& dv1_dba,
                 Eigen::Matrix<T, 3, 3>& dv1_dai);

/**
 * Forward propagate the velocity of a body, expressed in a gravity-fixed frame
 * by linear acceleration measurement a_i (with an additive bias b_a and
 * expressed in a body-fixed frame) over the time dt. Computes the derivatives
 * of v1 wrt q, b_a, a_i, and g_w.
 * @param v ///< Velocity at time t0
 * @param q ///< Orientation at time t0, expressed in world frame
 * @param b_a ///< Accelerometer bias, expressed in body-fixed frame [m/sec2]
 * @param a_i ///< Linear acceleration measured, expressed in body-fixed frame
 * [m/sec2]
 * @param g_w ///< Gravity expressed in world frame [m/sec2]
 * @param dt ///< Time interval [sec]
 * @param dv1_dq ///< Derivative of v1 wrt body orientation
 * @param dv1_dba ///< Derivative of v1 wrt accel bias
 * @param dv1_dai ///< Derivative of v1 wrt acceleration measurement
 * * @param dv1_dgw ///< Derivative of v1 wrt gravity
 */
template <typename T>
void propagate_v(Eigen::Matrix<T, 3, 1>& v, const Eigen::Quaternion<T>& q,
                 const Eigen::Matrix<T, 3, 1>& b_a,
                 const Eigen::Matrix<T, 3, 1>& a_i,
                 const Eigen::Matrix<T, 3, 1>& g_w, const T& dt,
                 Eigen::Matrix<T, 3, 4>& dv1_dq,
                 Eigen::Matrix<T, 3, 3>& dv1_dba,
                 Eigen::Matrix<T, 3, 3>& dv1_dai,
                 Eigen::Matrix<T, 3, 3>& dv1_dgw);

/**
 * Forward propagate the pose of a body due to the specified IMU measurements
 * over time dt. Also propagate the covariance of the body pose and compute
 * the derivative of the resulting pose with respect to the previous one.
 * @param p_ Position of the body at t0 [m]
 * @param q_ Orientation of the body at t0
 * @param v_ Velocity of the body at t0 [m/sec]
 * @param ba_ Accelerometer bias, expressed in body-fixed frame [m/sec2]
 * @param bg_ Gyro bias, expressed in body-fixed frame [rad/sec]
 * @param meas_ IMU measurement at t0
 * @param gw_ Gravity, expressed in world-fixed frame
 * @param dt_ Time interval
 * @param cov_x_ Covariance of the state, to be propagated from t0 to t0+dt
 * @param dx1_dx0 Derivative of the state at t1 wrt the state at t0. The state
 * is [p,q,v,ba,bg]
 * @param cov_imu_meas_ Covariance of the imu measurement, ordered [w, a, bg,
 * ba].
 */
template <typename T>
void integrate(
    Eigen::Matrix<T, 3, 1>& p_, Eigen::Quaternion<T>& q_,
    Eigen::Matrix<T, 3, 1>& v_, Eigen::Matrix<T, 3, 1>& ba_,
    Eigen::Matrix<T, 3, 1>& bg_,
    const std::shared_ptr<ProcessMeasurement> meas_,
    const Eigen::Matrix<T, 3, 1>& gw_, const T& dt_,
    Eigen::Matrix<T, 16, 16>&
        cov_x_,  // Used for calculating the stiffness matrix
    Eigen::Matrix<T, 16, 16>& dx1_dx0,  // Used for Jacobian calculation
    const Eigen::Matrix<T, 12, 12>& cov_imu_meas_);

/**
 * Forward propagate the pose of a body due to the specified IMU measurements
 * over time dt. Also propagate the covariance of the body pose and compute
 * the derivative of the resulting pose with respect to the previous one.
 * Also compute dx1_dgw, for use when gravity is also optimized online.
 * @param p_ Position of the body at t0 [m]
 * @param q_ Orientation of the body at t0
 * @param v_ Velocity of the body at t0 [m/sec]
 * @param ba_ Accelerometer bias, expressed in body-fixed frame [m/sec2]
 * @param bg_ Gyro bias, expressed in body-fixed frame [rad/sec]
 * @param meas_ IMU measurement at t0
 * @param gw_ Gravity, expressed in world-fixed frame
 * @param dt_ Time interval
 * @param cov_x_ Covariance of the state, to be propagated from t0 to t0+dt
 * @param dx1_dx0 Derivative of the state at t1 wrt the state at t0. The state
 * is [p,q,v,ba,bg]
 * @param dx1_dgw_ Derivative of the state at t1 wrt the gravity vector. The
 * state is [p,q,v,ba,bg]
 * @param cov_imu_meas_ Covariance of the imu measurement, ordered [w, a, bg,
 * ba].
 */
template <typename T>
void integrate_opt_gravity(
    Eigen::Matrix<T, 3, 1>& p_, Eigen::Quaternion<T>& q_,
    Eigen::Matrix<T, 3, 1>& v_, Eigen::Matrix<T, 3, 1>& ba_,
    Eigen::Matrix<T, 3, 1>& bg_,
    const std::shared_ptr<ProcessMeasurement> meas_,
    const Eigen::Matrix<T, 3, 1>& gw_, const T& dt_,
    Eigen::Matrix<T, 16, 16>&
        cov_x_,  // Used for calculating the stiffness matrix
    Eigen::Matrix<T, 16, 16>& dx1_dx0,  // Used for Jacobian calculation
    Eigen::Matrix<T, 16, 3>& dx1_dgw_,  // Used for Jacobian calculation
    const Eigen::Matrix<T, 12, 12>& cov_imu_meas_);

template <typename T>
bool integrate_no_jacob(Eigen::Matrix<T, 3, 1>& p_, Eigen::Quaternion<T>& q_,
                        Eigen::Matrix<T, 3, 1>& v_, Eigen::Matrix<T, 3, 1>& ba_,
                        Eigen::Matrix<T, 3, 1>& bg_, const ImuMeas* meas_,
                        const Eigen::Matrix<T, 3, 1>& gw_, const T& dt_);

template <typename T>
bool integrate_no_jacob(Eigen::Matrix<T, 3, 1>& p_, Eigen::Quaternion<T>& q_,
                        Eigen::Matrix<T, 3, 1>& v_, Eigen::Matrix<T, 3, 1>& ba_,
                        Eigen::Matrix<T, 3, 1>& bg_,
                        const std::shared_ptr<ProcessMeasurement>& meas_,
                        const Eigen::Matrix<T, 3, 1>& gw_, const T& dt_);

// This can be used in State::createNextState for spawning new state instances
// from a chain of IMU measurements.
inline void forwardPropagateImuMeas(
    const std::deque<std::shared_ptr<ProcessMeasurement>>& imuMeasurements,
    const double& t_in, const double& t_des, const Eigen::Vector3d& g_w,
    Pose<double>& T_w_i, Eigen::Vector3d& linVel, Eigen::Vector3d& accelBias,
    Eigen::Vector3d& gyroBias) {
  // Do some timing checks
  //	if (imuMeasurements.front()->t() > t_in) {
  // TODO I made this check less aggressive because of the logic problem in
  // adding process measurements in MeasMan
  if (imuMeasurements.front()->t() - t_in > 0.01) {
    std::cout << "forwardPropagateImuMeas: The current state is older than the "
                 "first IMU measurement. t_imu_front-t_state="
              << imuMeasurements.front()->t() - t_in
              << "; t_imu_back-t_imu_front="
              << imuMeasurements.back()->t() - imuMeasurements.front()->t()
              << std::endl;
  }
  if (t_des > imuMeasurements.back()->t() + 0.01) {
    std::cout << "forwardPropagateImuMeas: The last imu measurement is "
              << t_des - imuMeasurements.back()->t()
              << " sec past the lastest IMU measurement. t_des=" << t_des
              << "; t_imu_back:" << imuMeasurements.back()->t() << std::endl;
  }

  std::deque<std::shared_ptr<ProcessMeasurement>>::const_iterator imu_current =
      imuMeasurements.begin();

  // Move to the imu measurement at or before t_in
  while ((imu_current + 1) != imuMeasurements.end() &&
         (*(imu_current + 1))->t() <= t_in)
    ++imu_current;

  // Propagate from ti_ to t_imu_1
  if ((imu_current + 1) != imuMeasurements.end() &&
      (*imu_current)->t() <= t_in) {
    integrate_no_jacob(T_w_i.trans, T_w_i.rot, linVel, accelBias, gyroBias,
                       *imu_current, g_w, (*(imu_current + 1))->t() - t_in);
    ++imu_current;
  } else {
    // This when there is no imu measurement preceeding t_
    integrate_no_jacob(T_w_i.trans, T_w_i.rot, linVel, accelBias, gyroBias,
                       *imu_current, g_w, (*imu_current)->t() - t_in);
  }
  // std::cout << "cov_x out 1:\n" << cov_x << std::endl << std::endl;
  // Iterate through the imu measurements
  while ((imu_current + 1) != imuMeasurements.end() &&
         (*(imu_current + 1))->t() <= t_des) {
    integrate_no_jacob(T_w_i.trans, T_w_i.rot, linVel, accelBias, gyroBias,
                       *imu_current, g_w,
                       (*(imu_current + 1))->t() - (*imu_current)->t());
    ++imu_current;
  }

  // Propagate the last chunk of time. Don't do anything else if the last IMU
  // measurement is directly at t_des
  if ((*imu_current)->t() < t_des) {
    integrate_no_jacob(T_w_i.trans, T_w_i.rot, linVel, accelBias, gyroBias,
                       *imu_current, g_w, t_des - (*imu_current)->t());
  }
}

// Estimate the IMU attitude from the accelerometer measurement. Assumes that
// the true acceleration is small relative to gravity. The yaw orientation is
// unobservable from the accelerometer measurement alone.
inline Eigen::Quaterniond estimateImuOrientation(Eigen::Vector3d accel) {
  return Eigen::Quaterniond::FromTwoVectors(accel, Eigen::Vector3d(0, 0, 1));
}

}  // namespace confusion

#include "confusion/utilities/impl/imu_utils.h"

#endif /* INCLUDE_CONFUSION_IMU_UTILS_H_ */
