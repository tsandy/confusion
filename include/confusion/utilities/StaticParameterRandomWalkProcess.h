/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_MODELS_STATICPARAMETERRANDOMWALKPROCESS_H_
#define INCLUDE_CONFUSION_MODELS_STATICPARAMETERRANDOMWALKPROCESS_H_

#include <ceres/ceres.h>

#include <Eigen/Core>

#include "confusion/LocalParameterization.h"

namespace confusion {

// todo Should make another version for N many state/static parameters with
// dynamic cost function size and inheriting from ProcessChain for general usage

/**
 * Random walk process chain, to apply to a single static parameter
 */
class StaticParameterRandomWalkProcess : public ceres::CostFunction {
 public:
  StaticParameterRandomWalkProcess(double* priorParameterData,
                                   double* optParameterData,
                                   LocalParameterizationBase* parameterization,
                                   const double& processNoise);

  StaticParameterRandomWalkProcess(double* priorParameterData,
                                   double* optParameterData,
                                   const size_t& globalSize,
                                   const double& processNoise);

  void addCostFunctionToProblem(ceres::Problem* problem);

  void updateDt(const double& dt);

  // Parameter order is x_prior, x_opt
  bool Evaluate(double const* const* x, double* residuals,
                double** jacobians) const final;

 protected:
  LocalParameterizationBase* parameterization_;
  const size_t localSize_;     ///< Parameter local size
  const size_t globalSize_;    ///< Parameter global size
  const double processNoise_;  //< Process white noise. Units [unit of parameter
                               /// sqrt(sec)]
  double w_ =
      0.0;  ///< Process noise variance inverse-square-root
            ///< w=1/sigma_x/sqrt(dt). Units [1 / unit of parameter/ sqrt(sec)].

  std::vector<double*> parameterBlocks_;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_MODELS_STATICPARAMETERRANDOMWALKPROCESS_H_ */
