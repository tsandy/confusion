/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_UTILS_POSE_H_
#define CONFUSION_UTILS_POSE_H_

#include <confusion/utilities/distances.h>
#include <confusion/utilities/rotation_utils.h>

#include <Eigen/Geometry>
#include <iostream>

namespace confusion {

/**
 * @class Pose
 *
 * @brief A class to represent the 3D pose of a frame or object
 *
 * The class uses passive rotation and gives the same behavior as if using
 * 4x4 transformation matrices.
 * Designed for use in Ceres Sovler.
 * Templated on type for use with auto-differentiation.
 *
 * In this documentation the following conventions are used: T_a_b is the pose
 * of b expressed in frame a. a_t_b is the position of b expressed in frame a.
 */
template <typename T>
class Pose {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  /**
   * Construct from raw elements of the pose (translation and quaternion)
   * @param x [m]
   * @param y [m]
   * @param z [m]
   * @param qw
   * @param qx
   * @param qy
   * @param qz
   */
  Pose(T x, T y, T z, T qw, T qx, T qy, T qz);

  /**
   * Construct from eigen types
   * @param trans [m] x 3
   * @param rot [quaternion]
   */
  Pose(const Eigen::Matrix<T, 3, 1>& trans, const Eigen::Quaternion<T> rot);

  /**
   * Construct a Pose from a homogeneous transform
   * @param transformation - 4x4 transformation matrix
   */
  Pose(const Eigen::Matrix<T, 4, 4>& transformation);

  Pose();

  // Allows for easy conversion from double to ceres::Jet types, but not
  // vice-versa
  template <typename OtherT>
  Pose(const Pose<OtherT>& p);

  // Allows for easy conversion from double to ceres::Jet types, but not
  // vice-versa
  template <typename OtherT>
  Pose& operator=(const Pose<OtherT>& p);

  /**
   * Get the inverse of the pose. rot = q.conjugate(), trans = -q.conjugate() *
   * trans.
   *
   * inverse(T_A_B) = T_B_A
   *
   * @return
   */
  Pose<T> inverse() const;

  /**
   * Standard multiplication of transformations. T_A_C = T_A_B * T_B_C.
   * @param p_in
   * @return p_out = this * p_in
   */
  Pose<T> operator*(const Pose<T>& p_in) const;

  /**
   * Tranform a point in space to the frame of the pose. p_A = T_A_B * p_B
   * @param p_in
   * @return p_out = this * p_in
   */
  Eigen::Matrix<T, 3, 1> operator*(const Eigen::Matrix<T, 3, 1>& p_in) const;

  /**
   * Utility for quickly printing the contents of a pose
   */
  void print() const;

  /**
   * Utility for quickly printing the contents of a pose
   * @param prefix This is printed before the contents of the pose
   */
  void print(std::string prefix) const;

  /**
   * Get a motion transform for the pose. This can be used to express the
   * velocity of a rigid body about a different frame attached to that body.
   * Agrees with Robcogen conventions. The 6D velocity vector is NOT a spatial
   * velocity, just a stacked 3D angular velocity and 3D linear velocity.
   *
   * @return MT Can be used as a_vel_w_a = MT(T_a_b) * b_vel_w_b
   */
  Eigen::Matrix<T, 6, 6> toMotionTransform();

  // This rotates a velocity vector (a stacked angular velocity and linear
  // velocity). It simply rotates each component of the velocity, so it should
  // NOT be used with spatial velocities, as are typically used in RBD dynamics
  // calculations.
  Eigen::Matrix<double, 6, 1> rotateVelocityVector(
      Eigen::Matrix<double, 6, 1> v_in) const;

  // Gives the spatial vector transformation matrix along with its derviative,
  // given the velocity of the object The returned matrix T transforms spatial
  // velocities in the base frame to spatial velocities in the object frame todo
  // Looks like there might be an error in here. Use with caution!
  void toSpatialVectorTransform(Eigen::Matrix<T, 6, 6>& T_in,
                                Eigen::Matrix<T, 6, 6>& Td,
                                const Eigen::Matrix<T, 6, 1>& xd) const;

  /**
   * Convenience function to set the elemenets of the pose from a 4x4
   * homogeneous transform
   * @param transformation Transformation matrix (4x4)
   */
  void setFromHomogTrans(const Eigen::Matrix<T, 4, 4>& transformation);

  // Interpolate between two poses. t is in [0,1], where 0 would return this,
  // and 1 would return P1. Quaternions interpolated using slerp.
  Pose<T> interpolate(T t, Pose<T> P1) const;

  Eigen::Matrix<T, 4, 4> toTransformationMatrix() const;

  // Get the distance in tangent space between two poses. Ordered [drot; dtrans]
  // d = T_w_a * T_w_b^-1
  Eigen::Matrix<T, 6, 1> distance(Pose<T> P1) const;

  // Get the distance in global space between two poses. Ordered [drot; dtrans]
  // This requires more computation than distance, but is accurate in the
  // presence of large rotational differences
  Eigen::Matrix<T, 6, 1> distanceGlobal(Pose<T> P1) const;

 public:
  Eigen::Matrix<T, 3, 1> trans;  ///< position
  Eigen::Quaternion<T> rot;      ///< orientation (stored as a quaternion)
};

// Get the pose of the first tag seen, given the current estimate of the
// orientation of the world frame. The world frame is rotated in the x and y
// directions (rotation in the z-direction is unobservable). todo Find a better
// place for this.
template <typename T>
Pose<T> getTagPoseFromRot(const Pose<T>& T_w_t_init,
                          const Eigen::Matrix<T, 2, 1>& firstTagRot);

}  // namespace confusion

#include "confusion/utilities/impl/Pose.h"

#endif  // CONFUSION_UTILS_POSE_H_
