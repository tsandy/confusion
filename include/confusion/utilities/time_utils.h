//
// Created by tim on 17.01.20.
//

#ifndef CONFUSION_TIME_UTILS_H
#define CONFUSION_TIME_UTILS_H

#include <chrono>
#include <iostream>
#include <thread>

namespace confusion {

class Rate {
 public:
  Rate(const double& freq);

  void sleep();

  void reset();

  void setFreq(const double& freq);

  std::chrono::nanoseconds dt() const { return dt_; }

 private:
  std::chrono::high_resolution_clock::time_point t_last_;
  double freq_ = 1.0;
  std::chrono::nanoseconds dt_ = std::chrono::nanoseconds((long int)1e9);
};

class Timer {
 public:
  Timer();

  double getDuration();

  void reset();

 private:
  std::chrono::high_resolution_clock::time_point t_start_;
};

class RateLimiter {
 public:
  RateLimiter(const double& freq);

  void setFrequency(const double& freq);

  // Return true if there has been enough time since the last accepted cycle
  bool check();

  void reset();

 private:
  Timer timer_;
  double dt_;  // [sec]
};

double cpuTimeNow();

}  // namespace confusion

#endif  // CONFUSION_TIME_UTILS_H
