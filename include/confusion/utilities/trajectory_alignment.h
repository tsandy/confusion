/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_TRAJECTORYALIGNMENT_H_
#define CONFUSION_TRAJECTORYALIGNMENT_H_

#include <ceres/ceres.h>

#include "confusion/LocalParameterization.h"
#include "confusion/models/TrajectoryAlignmentCost.h"
#include "confusion/utilities/Pose.h"
#include "confusion/utilities/ceres_utils.h"

namespace confusion {

class AlignTrajCallback : public ceres::IterationCallback {
 public:
  AlignTrajCallback(Eigen::Matrix<double, 6, 1>& sum_error_,
                    Pose<double>& T_wref_wmeas_, Pose<double>& T_imeas_iref_,
                    double& delta_t_, double& scale_, Eigen::MatrixXd& errors_);

  virtual ceres::CallbackReturnType operator()(
      const ceres::IterationSummary& summary);

  Eigen::Matrix<double, 6, 1>& sum_error;
  Pose<double>& T_wref_wmeas;
  Pose<double>& T_imeas_iref;
  double& delta_t;
  double& scale;
  Eigen::MatrixXd& errors;
  int count = 0;
};

// todo Make this a class with initialize, optimize, post-process, etc functions
// todo Remove the error output arguments in the cost and optionally get the
// residuals after solving via problem::Evaluate
/**
 * Find the spacial and temporal offsets to align two estimated trajectories.
 * Quantities optimized for are: T_wref_wmeas : Pose of the reference world
 * frame with respect to the measurement world frame T_imeas_iref : Pose of the
 * reference target frame with respect to the measurement target frame dt : Time
 * offset between the two trajectories (t_ref = t_meas + dt) scale : Scale
 * difference between the two trajectories The user can specify which parameters
 * should be solved for. When all parameters are being solved for it is assumed
 * that a good initial value for T_imeas_iref is passed. T_wref_wmeas and dt are
 * initialized using the start of the passed trajectories.
 * @param referenceTrajectory
 * @param measuredTrajectory
 * @param optWorldFrames Flags indicating which parameters should be optimized
 * @param optTargetFrames
 * @param optTimeDelay
 * @param optScale
 * @param refDropTime Ignore measurement poses that do not have a corresponding
 * reference pose within this neighboring time window
 * @param T_wref_wmeas Resulting world frame offset
 * @param T_imeas_iref Resulting target frame offset -- A good initial value
 * should be passed (will be Identity when the same target frame is considered
 * in both trajectories)
 * @param delta_t Resulting time offset -- Initialized using the first timestamp
 * of each trajectory
 * @param scale Resulting scale factor
 * @param trans_stddev To set the relative weighting of trans/rot errors
 * @param rot_stddev
 * @param errors_ The final error values can be optionally output to this matrix
 * for post-processing and analysis
 * @param lossCoeff Cauchy loss coefficient used for outlier rejection
 * (optional)
 * @return referenceTrajectory with the resulting offsets applied
 */
std::vector<TrajectoryPoint> alignTrajectory(
    std::vector<TrajectoryPoint>& referenceTrajectory,
    std::vector<TrajectoryPoint>& measuredTrajectory, bool optWorldFrames,
    bool optTargetFrames, bool optTimeDelay, bool optScale, double refDropTime,
    Pose<double>& T_wref_wmeas, Pose<double>& T_imeas_iref, double& delta_t,
    double& scale, const double trans_stddev, const double rot_stddev,
    Eigen::MatrixXd* errors_ = NULL, double* lossCoeff = NULL);

}  // namespace confusion

#endif /* CONFUSION_TRAJECTORYALIGNMENT_H_ */
