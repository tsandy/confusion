/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

namespace confusion {


inline Eigen::Vector3d quat_to_rpy(Eigen::Quaterniond quat) {
  Eigen::Matrix3d rotMat = quat.toRotationMatrix();
  return rotMat.eulerAngles(0, 1, 2);
}

inline Eigen::Quaterniond rpy_to_quat(double roll, double pitch, double yaw) {
  Eigen::Matrix3d rot_mat;
  rot_mat = Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX())
            * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY())
            * Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ());

  return Eigen::Quaterniond(rot_mat);
}

inline Eigen::Quaterniond rpy_to_quat(Eigen::Vector3d rpy) {
  Eigen::Matrix3d rot_mat;
  rot_mat = Eigen::AngleAxisd(rpy(0), Eigen::Vector3d::UnitX())
            * Eigen::AngleAxisd(rpy(1), Eigen::Vector3d::UnitY())
            * Eigen::AngleAxisd(rpy(2), Eigen::Vector3d::UnitZ());

  return Eigen::Quaterniond(rot_mat);
}

template<typename T>
Eigen::Matrix<T, 3, 1> gravityVec(T const *r, const T g_mag) {
  Eigen::Matrix<T, 3, 1> g;
  T cos_r1 = cos(r[1]);
  g(0) = sin(r[1]) * g_mag;
  g(1) = -sin(r[0]) * cos_r1 * g_mag;
  g(2) = cos(r[0]) * cos_r1 * g_mag;
  return g;
}

inline Eigen::Matrix<double, 3, 2> gravityVec_jacob(double const *r, const double g_mag) {
  Eigen::Matrix<double, 3, 2> dg_dr;
  dg_dr << 0.0, cos(r[1]) * g_mag,
      -cos(r[0]) * cos(r[1]) * g_mag, sin(r[0]) * sin(r[1]) * g_mag,
      -sin(r[0]) * cos(r[1]) * g_mag, -cos(r[0]) * sin(r[1]) * g_mag;
  return dg_dr;
}

template<typename T>
void QuatProduct_jacob_left(const Eigen::Quaternion<T> &z,
                                   const Eigen::Quaternion<T> &w, Eigen::Matrix<T, 4, 4> &dzw_dz) {
  //The output derivative rows and cols are ordered [w,x,y,z]!
  dzw_dz(0, 0) = w.w();
  dzw_dz(0, 1) = -w.x();
  dzw_dz(0, 2) = -w.y();
  dzw_dz(0, 3) = -w.z();
  dzw_dz(1, 0) = w.x();
  dzw_dz(1, 1) = w.w();
  dzw_dz(1, 2) = w.z();
  dzw_dz(1, 3) = -w.y();
  dzw_dz(2, 0) = w.y();
  dzw_dz(2, 1) = -w.z();
  dzw_dz(2, 2) = w.w();
  dzw_dz(2, 3) = w.x();
  dzw_dz(3, 0) = w.z();
  dzw_dz(3, 1) = w.y();
  dzw_dz(3, 2) = -w.x();
  dzw_dz(3, 3) = w.w();
}

template<typename T>
void QuatProduct_jacob_right(const Eigen::Quaternion<T> &z,
                                    const Eigen::Quaternion<T> &w, Eigen::Matrix<T, 4, 4> &dzw_dw) {
  //The output derivative rows and cols are ordered [w,x,y,z]!
  dzw_dw(0, 0) = z.w();
  dzw_dw(0, 1) = -z.x();
  dzw_dw(0, 2) = -z.y();
  dzw_dw(0, 3) = -z.z();
  dzw_dw(1, 0) = z.x();
  dzw_dw(1, 1) = z.w();
  dzw_dw(1, 2) = -z.z();
  dzw_dw(1, 3) = z.y();
  dzw_dw(2, 0) = z.y();
  dzw_dw(2, 1) = z.z();
  dzw_dw(2, 2) = z.w();
  dzw_dw(2, 3) = -z.x();
  dzw_dw(3, 0) = z.z();
  dzw_dw(3, 1) = -z.y();
  dzw_dw(3, 2) = z.x();
  dzw_dw(3, 3) = z.w();
}

template<typename T>
void QuatRotatePoint_jacob_left(const Eigen::Quaternion<T> &q,
                                       const Eigen::Matrix<T, 3, 1> &pt, Eigen::Matrix<T, 3, 4> &dr_dq) {
  //Jacobian is ordered [w,x,y,z]
  dr_dq(0, 0) = T(2) * (-q.z() * pt[1] + q.y() * pt[2]);
  dr_dq(0, 1) = T(2) * (q.y() * pt[1] + q.z() * pt[2]);
  dr_dq(0, 2) = T(2) * (-T(2) * q.y() * pt[0] + q.x() * pt[1] + q.w() * pt[2]);
  dr_dq(0, 3) = T(2) * (-T(2) * q.z() * pt[0] - q.w() * pt[1] + q.x() * pt[2]);
  dr_dq(1, 0) = T(2) * (q.z() * pt[0] - q.x() * pt[2]);
  dr_dq(1, 1) = T(2) * (q.y() * pt[0] - T(2) * q.x() * pt[1] - q.w() * pt[2]);
  dr_dq(1, 2) = T(2) * (q.x() * pt[0] + q.z() * pt[2]);
  dr_dq(1, 3) = T(2) * (q.w() * pt[0] - T(2) * q.z() * pt[1] + q.y() * pt[2]);
  dr_dq(2, 0) = T(2) * (-q.y() * pt[0] + q.x() * pt[1]);
  dr_dq(2, 1) = T(2) * (q.z() * pt[0] + q.w() * pt[1] - T(2) * q.x() * pt[2]);
  dr_dq(2, 2) = T(2) * (-q.w() * pt[0] + q.z() * pt[1] - T(2) * q.y() * pt[2]);
  dr_dq(2, 3) = T(2) * (q.x() * pt[0] + q.y() * pt[1]);
}

template<typename T>
void QuatRotatePoint_jacob_right(const Eigen::Quaternion<T> &q,
                                        const Eigen::Matrix<T, 3, 1> &pt, Eigen::Matrix<T, 3, 3> &dr_dpt) {
  dr_dpt(0, 0) = T(1) - T(2) * (q.y() * q.y() + q.z() * q.z());
  dr_dpt(0, 1) = T(2) * (q.x() * q.y() - q.w() * q.z());
  dr_dpt(0, 2) = T(2) * (q.w() * q.y() + q.x() * q.z());
  dr_dpt(1, 0) = T(2) * (q.w() * q.z() + q.x() * q.y());
  dr_dpt(1, 1) = T(1) - T(2) * (q.x() * q.x() + q.z() * q.z());
  dr_dpt(1, 2) = T(2) * (q.y() * q.z() - q.w() * q.x());
  dr_dpt(2, 0) = T(2) * (q.x() * q.z() - q.w() * q.y());
  dr_dpt(2, 1) = T(2) * (q.w() * q.x() + q.y() * q.z());
  dr_dpt(2, 2) = T(1) - T(2) * (q.x() * q.x() + q.y() * q.y());
}

template<typename T>
void quatToRotationMatrix(const Eigen::Quaternion<T> &q, Eigen::Matrix<T, 3, 3> &R) {
  // Make convenient names for elements of q.
  T a = q.w();
  T b = q.x();
  T c = q.y();
  T d = q.z();
  // This is not to eliminate common sub-expression, but to
  // make the lines shorter so that they fit in 80 columns! (from Ceres code...)
  T aa = a * a;
  T ab = a * b;
  T ac = a * c;
  T ad = a * d;
  T bb = b * b;
  T bc = b * c;
  T bd = b * d;
  T cc = c * c;
  T cd = c * d;
  T dd = d * d;

  R(0, 0) = aa + bb - cc - dd;
  R(0, 1) = T(2.0) * (bc - ad);
  R(0, 2) = T(2.0) * (ac + bd);
  R(1, 0) = T(2.0) * (ad + bc);
  R(1, 1) = aa - bb + cc - dd;
  R(1, 2) = T(2.0) * (cd - ab);
  R(2, 0) = T(2.0) * (bd - ac);
  R(2, 1) = T(2.0) * (ab + cd);
  R(2, 2) = aa - bb - cc + dd;
}

template<typename T>
void calc_dq_dR(const Eigen::Matrix<T, 3, 3> &R, Eigen::Matrix<T, 4, 9> &dq_dR) {
  const T trace = R(0, 0) + R(1, 1) + R(2, 2);
  if (trace >= T(0.0)) {
    T c = T(1.0) + trace;
    T c1 = T(0.5) / sqrt(c);
    T c2 = T(0.5) * c1;
    T c3 = -c1 / T(2.0) / c;

    dq_dR(0, 0) = c2;
    dq_dR(0, 1) = T(0.0);
    dq_dR(0, 2) = T(0.0);
    dq_dR(0, 3) = T(0.0);
    dq_dR(0, 4) = c2;
    dq_dR(0, 5) = T(0.0);
    dq_dR(0, 6) = T(0.0);
    dq_dR(0, 7) = T(0.0);
    dq_dR(0, 8) = c2;

    dq_dR(1, 0) = (R(2, 1) - R(1, 2)) * c3;
    dq_dR(1, 1) = T(0.0);
    dq_dR(1, 2) = T(0.0);
    dq_dR(1, 3) = T(0.0);
    dq_dR(1, 4) = (R(2, 1) - R(1, 2)) * c3;
    dq_dR(1, 5) = -c1;
    dq_dR(1, 6) = T(0.0);
    dq_dR(1, 7) = c1;
    dq_dR(1, 8) = (R(2, 1) - R(1, 2)) * c3;

    dq_dR(2, 0) = (R(0, 2) - R(2, 0)) * c3;
    dq_dR(2, 1) = T(0.0);
    dq_dR(2, 2) = c1;
    dq_dR(2, 3) = T(0.0);
    dq_dR(2, 4) = (R(0, 2) - R(2, 0)) * c3;
    dq_dR(2, 5) = T(0.0);
    dq_dR(2, 6) = -c1;
    dq_dR(2, 7) = T(0.0);
    dq_dR(2, 8) = (R(0, 2) - R(2, 0)) * c3;

    dq_dR(3, 0) = (R(1, 0) - R(0, 1)) * c3;
    dq_dR(3, 1) = -c1;
    dq_dR(3, 2) = T(0.0);
    dq_dR(3, 3) = c1;
    dq_dR(3, 4) = (R(1, 0) - R(0, 1)) * c3;
    dq_dR(3, 5) = T(0.0);
    dq_dR(3, 6) = T(0.0);
    dq_dR(3, 7) = T(0.0);
    dq_dR(3, 8) = (R(1, 0) - R(0, 1)) * c3;
  } else {
    int i = 0;
    if (R(1, 1) > R(0, 0)) {
      i = 1;
    }

    if (R(2, 2) > R(i, i)) {
      i = 2;
    }

    const int j = (i + 1) % 3;
    const int k = (j + 1) % 3;
    T c = R(i, i) - R(j, j) - R(k, k) + 1.0;
    T c1 = T(0.5) / sqrt(c);
    T c2 = T(0.5) * c1;
    T c3 = -c1 / T(2.0) / c;

    //todo Is it possible to do this without making an extra copy?
    //First fill in with [0->9]=[ii,ij,ik,ji...]
    Eigen::Matrix<T, 4, 9> dq_dR_ijk;
    dq_dR_ijk(0, 0) = (R(k, j) - R(j, k)) * c3;
    dq_dR_ijk(0, 1) = T(0.0);
    dq_dR_ijk(0, 2) = T(0.0);
    dq_dR_ijk(0, 3) = T(0.0);
    dq_dR_ijk(0, 4) = T(-1.0) * (R(k, j) - R(j, k)) * c3;
    dq_dR_ijk(0, 5) = -c1;
    dq_dR_ijk(0, 6) = T(0.0);
    dq_dR_ijk(0, 7) = c1;
    dq_dR_ijk(0, 8) = T(-1.0) * (R(k, j) - R(j, k)) * c3;

    dq_dR_ijk(i + 1, 0) = c2;
    dq_dR_ijk(i + 1, 1) = T(0.0);
    dq_dR_ijk(i + 1, 2) = T(0.0);
    dq_dR_ijk(i + 1, 3) = T(0.0);
    dq_dR_ijk(i + 1, 4) = -c2;
    dq_dR_ijk(i + 1, 5) = T(0.0);
    dq_dR_ijk(i + 1, 6) = T(0.0);
    dq_dR_ijk(i + 1, 7) = T(0.0);
    dq_dR_ijk(i + 1, 8) = -c2;

    dq_dR_ijk(j + 1, 0) = (R(j, i) + R(i, j)) * c3;
    dq_dR_ijk(j + 1, 1) = c1;
    dq_dR_ijk(j + 1, 2) = T(0.0);
    dq_dR_ijk(j + 1, 3) = c1;
    dq_dR_ijk(j + 1, 4) = T(-1.0) * (R(j, i) + R(i, j)) * c3;
    dq_dR_ijk(j + 1, 5) = T(0.0);
    dq_dR_ijk(j + 1, 6) = T(0.0);
    dq_dR_ijk(j + 1, 7) = T(0.0);
    dq_dR_ijk(j + 1, 8) = T(-1.0) * (R(j, i) + R(i, j)) * c3;

    dq_dR_ijk(k + 1, 0) = (R(k, i) + R(i, k)) * c3;
    dq_dR_ijk(k + 1, 1) = T(0.0);
    dq_dR_ijk(k + 1, 2) = c1;
    dq_dR_ijk(k + 1, 3) = T(0.0);
    dq_dR_ijk(k + 1, 4) = T(-1.0) * (R(k, i) + R(i, k)) * c3;
    dq_dR_ijk(k + 1, 5) = T(0.0);
    dq_dR_ijk(k + 1, 6) = c1;
    dq_dR_ijk(k + 1, 7) = T(0.0);
    dq_dR_ijk(k + 1, 8) = T(-1.0) * (R(k, i) + R(i, k)) * c3;

    //Reorder terms to match the true i,j,k structure
    if (i == 1) {
      dq_dR.row(0) << dq_dR_ijk(0, 8), dq_dR_ijk(0, 6), dq_dR_ijk(0, 7), dq_dR_ijk(0, 2), dq_dR_ijk(0, 0), dq_dR_ijk(0,
                                                                                                                     1), dq_dR_ijk(
              0,
              5), dq_dR_ijk(0, 3), dq_dR_ijk(0, 4);
      dq_dR.row(1) << dq_dR_ijk(1, 8), dq_dR_ijk(1, 6), dq_dR_ijk(1, 7), dq_dR_ijk(1, 2), dq_dR_ijk(1, 0), dq_dR_ijk(1,
                                                                                                                     1), dq_dR_ijk(
              1,
              5), dq_dR_ijk(1, 3), dq_dR_ijk(1, 4);
      dq_dR.row(2) << dq_dR_ijk(2, 8), dq_dR_ijk(2, 6), dq_dR_ijk(2, 7), dq_dR_ijk(2, 2), dq_dR_ijk(2, 0), dq_dR_ijk(2,
                                                                                                                     1), dq_dR_ijk(
              2,
              5), dq_dR_ijk(2, 3), dq_dR_ijk(2, 4);
      dq_dR.row(3) << dq_dR_ijk(3, 8), dq_dR_ijk(3, 6), dq_dR_ijk(3, 7), dq_dR_ijk(3, 2), dq_dR_ijk(3, 0), dq_dR_ijk(3,
                                                                                                                     1), dq_dR_ijk(
              3,
              5), dq_dR_ijk(3, 3), dq_dR_ijk(3, 4);
    } else if (i == 2) {
      dq_dR.row(0) << dq_dR_ijk(0, 4), dq_dR_ijk(0, 5), dq_dR_ijk(0, 3), dq_dR_ijk(0, 7), dq_dR_ijk(0, 8), dq_dR_ijk(0,
                                                                                                                     6), dq_dR_ijk(
              0,
              1), dq_dR_ijk(0, 2), dq_dR_ijk(0, 0);
      dq_dR.row(1) << dq_dR_ijk(1, 4), dq_dR_ijk(1, 5), dq_dR_ijk(1, 3), dq_dR_ijk(1, 7), dq_dR_ijk(1, 8), dq_dR_ijk(1,
                                                                                                                     6), dq_dR_ijk(
              1,
              1), dq_dR_ijk(1, 2), dq_dR_ijk(1, 0);
      dq_dR.row(2) << dq_dR_ijk(2, 4), dq_dR_ijk(2, 5), dq_dR_ijk(2, 3), dq_dR_ijk(2, 7), dq_dR_ijk(2, 8), dq_dR_ijk(2,
                                                                                                                     6), dq_dR_ijk(
              2,
              1), dq_dR_ijk(2, 2), dq_dR_ijk(2, 0);
      dq_dR.row(3) << dq_dR_ijk(3, 4), dq_dR_ijk(3, 5), dq_dR_ijk(3, 3), dq_dR_ijk(3, 7), dq_dR_ijk(3, 8), dq_dR_ijk(3,
                                                                                                                     6), dq_dR_ijk(
              3,
              1), dq_dR_ijk(3, 2), dq_dR_ijk(3, 0);
    } else {
      dq_dR = dq_dR_ijk;
    }
  }
}

template<typename T>
Eigen::Quaternion<T> quaternionBoxPlus(const Eigen::Quaternion<T> &q, const Eigen::Matrix<T, 3, 1> &delta) {
  Eigen::Quaternion<T> q_out;

  const T delta_norm = delta.norm();

  if (delta_norm > T(0.0)) {
    const T sin_delta_by_delta = sin(T(0.5) * delta_norm) / delta_norm;
    Eigen::Quaternion<T> tmp(cos(0.5 * delta_norm),
                             sin_delta_by_delta * delta[0],
                             sin_delta_by_delta * delta[1],
                             sin_delta_by_delta * delta[2]);
    q_out = tmp * q;
  } else {
    q_out = q;
  }

  return q_out;
}

inline Eigen::Matrix<double, 4, 3> quaternionBoxPlusJacob(const Eigen::Quaterniond &q) {
  Eigen::Matrix<double, 4, 3> dqout_ddq;

  //Note that this gives the jacobian in order [x,y,z,w] to reflect eigen's underlying data structure
  dqout_ddq << q.w(), q.z(), -q.y(),  // x
      -q.z(), q.w(), q.x(),  // y
      q.y(), -q.x(), q.w(),  // z
      -q.x(), -q.y(), -q.z();  // w

  dqout_ddq *= 0.5;

  return dqout_ddq;
}

inline Eigen::Matrix<double, 4, 2> quaternionBoxPlusJacobFixedYaw(const Eigen::Quaterniond &q) {
  Eigen::Matrix<double, 4, 2> dqout_ddq;

  //Note that this gives the jacobian in order [x,y,z,w] to reflect eigen's underlying data structure
  dqout_ddq << q.w(), q.z(),  // x
      -q.z(), q.w(),  // y
      q.y(), -q.x(),  // z
      -q.x(), -q.y();  // w

  dqout_ddq *= 0.5;

  return dqout_ddq;
}

template<typename T>
Eigen::Quaternion<T> slerp(const Eigen::Quaternion<T> q0, const Eigen::Quaternion<T> q1, const T t) {
  using std::acos;
  using std::sin;
  using std::abs;
  static const T one = T(1) - Eigen::NumTraits<T>::epsilon();
  T d = q0.dot(q1);
  T absD = abs(d);

  T scale0;
  T scale1;

  if (absD >= one) {
    scale0 = T(1) - t;
    scale1 = t;
  } else {
    // theta is the angle between the 2 quaternions
    T theta = acos(absD);
    T sinTheta = sin(theta);

    scale0 = sin((T(1) - t) * theta) / sinTheta;
    scale1 = sin((t * theta)) / sinTheta;
  }
  if (d < T(0)) scale1 = -scale1;

  return Eigen::Quaternion<T>(scale0 * q0.coeffs() + scale1 * q1.coeffs());
}

template<typename T>
Eigen::Matrix<T, 4, 1> calulateDqDt(Eigen::Quaternion<T> q, Eigen::Matrix<T, 3, 1> omega) {
  Eigen::Quaternion<T> omega_(T(0.0), omega(0), omega(1), omega(2));

  //todo Could save a few multiplications by using the quatproduct jacobian directly so I can avoid the omega_.w computations
  Eigen::Quaternion<T> qq = q * omega_;
  Eigen::Matrix<T, 4, 1> dq_dt = T(0.5) * qq.coeffs();

  return dq_dt;
}

inline Eigen::Matrix<double, 6, 6> rigidBodyVelocityTransform(Eigen::Vector3d w_pos_a_b) {
  Eigen::Matrix<double, 6, 6> VT(Eigen::Matrix<double, 6, 6>::Identity());
  Eigen::Matrix<double, 3, 3> t_cross;
  t_cross << 0.0, -w_pos_a_b(2), w_pos_a_b(1),
      w_pos_a_b(2), 0.0, -w_pos_a_b(0),
      -w_pos_a_b(1), w_pos_a_b(0), 0.0;
  VT.block<3, 3>(3, 0) = t_cross;

  return VT;
}

inline double initializeGpsReferenceFrameOffset(const Eigen::Vector3d &t_w_r0,
                                                const Eigen::Vector3d &t_w_r1,
                                                const Eigen::Vector3d &t_g_r0,
                                                const Eigen::Vector3d &t_g_r1,
                                                Eigen::Vector3d &t_w_g,
                                                double &theta_w_g) {
  // Get the unit vectors in the direction of the displacements in the two frames (in the horizontal plane)
  Eigen::Vector2d vr_g = t_g_r1.head<2>() - t_g_r0.head<2>();
  vr_g.normalize();
  Eigen::Vector2d vr_w = t_w_r1.head<2>() - t_w_r0.head<2>();
  vr_w.normalize();
  std::cout << "vr_g: " << vr_g.transpose() << std::endl;
  std::cout << "vr_w: " << vr_w.transpose() << std::endl;

  // Get theta to align the two displacement vectors (dot product)
  theta_w_g = acos(vr_g(0) * vr_w(0) + vr_g(1) * vr_w(1));
  if (vr_g(0)*vr_w(1) - vr_g(1)*vr_w(0) < 0.0) // Check v_normal (dot) vr_g (cross) vr_w
    theta_w_g *= -1.0;

  // Get the position offset as the average of the two t_w_g's from the measurements
  Eigen::Matrix2d rot_w_g;
  rot_w_g << cos(theta_w_g), -sin(theta_w_g), sin(theta_w_g), cos(theta_w_g);
  Eigen::Vector2d t_w_g0 = t_w_r0.head<2>() + rot_w_g * -t_g_r0.head<2>();
  Eigen::Vector2d t_w_g1 = t_w_r1.head<2>() + rot_w_g * -t_g_r1.head<2>();
  t_w_g.head<2>() = (t_w_g0 + t_w_g1) / 2.0;

  // Get the height offset from the average of the measurements
  t_w_g(2) = (t_w_r0(2) + t_w_r1(2) - t_g_r0(2) - t_g_r1(2)) / 2.0;

  double agreement = (t_w_g0 - t_w_g1).norm(); // Distance [m] between the aigned measurements

  return agreement;
}

} // namespace confusion