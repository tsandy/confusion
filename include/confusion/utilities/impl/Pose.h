/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

namespace confusion {

template <typename T>
Pose<T>::Pose(T x, T y, T z, T qw, T qx, T qy, T qz)
    : trans(x, y, z), rot(qw, qx, qy, qz) {}

template <typename T>
Pose<T>::Pose(const Eigen::Matrix<T, 3, 1>& trans, const Eigen::Quaternion<T> rot)
    : trans(trans), rot(rot) {}

template <typename T>
Pose<T>::Pose(const Eigen::Matrix<T, 4, 4>& transformation) {
  trans = transformation.template block<3, 1>(0, 3) / transformation(3, 3);
  Eigen::Matrix<T, 3, 3> rotMat = transformation.block(0, 0, 3, 3);
  rot = Eigen::Quaternion<T>(rotMat);
}

template <typename T>
Pose<T>::Pose() : trans(T(0.0), T(0.0), T(0.0)), rot(T(1.0), T(0.0), T(0.0), T(0.0)) {}

template <typename T>
template <typename OtherT>
Pose<T>::Pose(const Pose<OtherT>& p)
    : trans(T(p.trans.x()), T(p.trans.y()), T(p.trans.z())),
      rot(T(p.rot.w()), T(p.rot.x()), T(p.rot.y()), T(p.rot.z())) {}

template <typename T>
template <typename OtherT>
Pose<T>& Pose<T>::operator=(const Pose<OtherT>& p) {
  trans =
      Eigen::Matrix<T, 3, 1>(T(p.trans.x()), T(p.trans.y()), T(p.trans.z()));
  rot = Eigen::Quaternion<T>(T(p.rot.w()), T(p.rot.x()), T(p.rot.y()),
                             T(p.rot.z()));

  return *this;
}

template <typename T>
Pose<T> Pose<T>::inverse() const {
  Pose<T> output;
  output.rot = rot.conjugate();
  output.trans = output.rot * trans;
  output.trans *= T(-1.0);

  return output;
}

template <typename T>
Pose<T> Pose<T>::operator*(const Pose<T>& p_in) const {
  Pose<T> p_out;
  p_out.rot = rot * p_in.rot;

  p_out.trans = trans + rot * p_in.trans;

  return p_out;
}

template <typename T>
Eigen::Matrix<T, 3, 1> Pose<T>::operator*(const Eigen::Matrix<T, 3, 1>& p_in) const {
  Eigen::Matrix<T, 3, 1> p_out;
  p_out = rot * p_in;
  p_out += trans;

  return p_out;
}

template <typename T>
void Pose<T>::print() const {
  printf("t: [%f,%f,%f]; q: [%f,%f,%f,%f]\n", trans[0], trans[1], trans[2],
         rot.w(), rot.x(), rot.y(), rot.z());
}

template <typename T>
void Pose<T>::print(std::string prefix) const {
  printf("%s --- t: [%f,%f,%f]; q: [%f,%f,%f,%f]\n", prefix.c_str(), trans[0],
         trans[1], trans[2], rot.w(), rot.x(), rot.y(), rot.z());
}

template <typename T>
Eigen::Matrix<T, 6, 6> Pose<T>::toMotionTransform() {
  Eigen::Matrix<T, 6, 6> MT;

  Eigen::Matrix<T, 3, 3> R = rot.toRotationMatrix();

  Eigen::Matrix<T, 3, 3> t_cross;
  t_cross << T(0), -trans(2), trans(1), trans(2), T(0), -trans(0), -trans(1),
      trans(0), T(0);

  MT.block(0, 0, 3, 3) = R;
  MT.block(0, 3, 3, 3) = Eigen::Matrix<T, 3, 3>::Zero();
  MT.block(3, 0, 3, 3) = t_cross * R;
  MT.block(3, 3, 3, 3) = R;

  return MT;
}

template <typename T>
Eigen::Matrix<double, 6, 1> Pose<T>::rotateVelocityVector(
    Eigen::Matrix<double, 6, 1> v_in) const {
  Eigen::Matrix<double, 6, 1> v_out;
  v_out.head<3>() = rot * v_in.head<3>();
  v_out.tail<3>() = rot * v_in.tail<3>();
  return v_out;
}

template <typename T>
void Pose<T>::toSpatialVectorTransform(Eigen::Matrix<T, 6, 6>& T_in,
                              Eigen::Matrix<T, 6, 6>& Td,
                              const Eigen::Matrix<T, 6, 1>& xd) const {
  Eigen::Matrix<T, 3, 3> w_cross;
  w_cross << 0, -xd(2), xd(1), xd(2), 0, -xd(0), -xd(1), xd(0), 0;

  Eigen::Matrix<T, 3, 3> Rinv = (rot.inverse()).toRotationMatrix();

  T_in.block(0, 0, 3, 3) = Rinv;
  T_in.block(0, 3, 3, 3) = Eigen::Matrix<T, 3, 3>::Zero();
  T_in.block(3, 0, 3, 3) = Eigen::Matrix<T, 3, 3>::Zero();  //-Rinv * d_cross;
  T_in.block(3, 3, 3, 3) = Rinv;

  // Robcogen Jacobians do not compute spatial velocities, but stacked rot and
  // trans velocity vectors!
  Td.block(0, 0, 3, 3) =
      Rinv * w_cross.transpose();  // dRotMat = w_cross * RotMat, so
                                   // dRotMatInv = RotMatInv * w_crossTranspose
  Td.block(0, 3, 3, 3) = Eigen::Matrix<T, 3, 3>::Zero();
  Td.block(3, 0, 3, 3) =
      Eigen::Matrix<T, 3, 3>::Zero();  //-Rinv * w_cross.transpose() * d_cross
                                       //- Rinv * v_cross;
  Td.block(3, 3, 3, 3) = Rinv * w_cross.transpose();
}

template <typename T>
void Pose<T>::setFromHomogTrans(const Eigen::Matrix<T, 4, 4>& transformation) {
  trans = transformation.block(0, 3, 3, 1) / transformation(3, 3);
  rot = Eigen::Quaterniond(transformation.block(0, 0, 3, 3));
}

template <typename T>
Pose<T> Pose<T>::interpolate(T t, Pose<T> P1) const {
  Pose<T> Pout;
  Pout.trans = trans + (P1.trans - trans) * t;
  Pout.rot = slerp(rot, P1.rot, t);
  return Pout;
}

template <typename T>
Eigen::Matrix<T, 4, 4> Pose<T>::toTransformationMatrix() const {
  Eigen::Matrix<T, 4, 4> tmat;
  tmat.template topLeftCorner<3, 3>() = rot.toRotationMatrix();
  tmat.template topRightCorner<3, 1>() = trans;
  tmat.row(3) << 0, 0, 0, 1;
  return tmat;
}

template <typename T>
Eigen::Matrix<T, 6, 1> Pose<T>::distance(Pose<T> P1) const {
  Eigen::Matrix<T, 6, 1> d;
  QuatDistance(rot, P1.rot, d.data());
  VectorDistance(trans.data(), P1.trans.data(), d.data() + 3);
  return d;
}

template <typename T>
Eigen::Matrix<T, 6, 1> Pose<T>::distanceGlobal(Pose<T> P1) const {
  Eigen::Matrix<T, 6, 1> d;
  QuatDistanceGlobal(rot, P1.rot, d.data());
  VectorDistance(trans.data(), P1.trans.data(), d.data() + 3);
  return d;
}

template <typename T>
Pose<T> getTagPoseFromRot(const Pose<T>& T_w_t_init,
                          const Eigen::Matrix<T, 2, 1>& firstTagRot) {
  T phi_roll = firstTagRot(0) / T(2.0);
  T phi_pitch = firstTagRot(1) / T(2.0);
  Eigen::Quaternion<T> dq_roll(cos(phi_roll), sin(phi_roll), T(0.0), T(0.0));
  Eigen::Quaternion<T> dq_pitch(cos(phi_pitch), T(0.0), sin(phi_pitch), T(0.0));
  return Pose<T>(T_w_t_init.trans, dq_roll * dq_pitch * T_w_t_init.rot);
}

} // namespace confusion