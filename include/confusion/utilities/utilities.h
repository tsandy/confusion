/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_UTILITIES_H_
#define INCLUDE_CONFUSION_UTILITIES_H_

#include <ceres/ceres.h>

#include <Eigen/Core>
#include <chrono>

namespace confusion {

/**
 * Iteration callback to tell ceres to stop solving the current problem because
 * either the node was terminated with ctrl-c or the boolean flag passed
 * requests it
 */
class SolverOverrunCallback : public ceres::IterationCallback {
 public:
  SolverOverrunCallback(const bool& abortSolver, const int minIterations = 0);

  ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary);

  const bool& abortSolver_;
  const int minIterations_;
};

/**
 * Iteration callback to abort optimization if it had been running longer than a
 * desired duration.
 * Pass a reference to the time when each optimization is started. This needs to
 * be set to the current time manually before calling ConFusor::optimize each
 * time.
 */
class SolverTimeoutCallback : public ceres::IterationCallback {
 public:
  SolverTimeoutCallback(
      const std::chrono::time_point<std::chrono::high_resolution_clock>&
          t_start,
      const double& maxOptimizationDuration, const int minIterations = 0);

  ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary);

  const std::chrono::time_point<std::chrono::high_resolution_clock>& t_start_;
  std::chrono::duration<double> maxOptimizationDuration_;  ///< [sec}
  const int minIterations_;
};

// H and b should be passed in the order H = [H_marg_marg, H marg_stay;
// H_stay_marg, H_stay_stay]
/**
 * Marginalize out part of the H dx = b problem using Gaussian elimination.
 * margDim rows are removed from the end of the dx vector, so H and b should
 * already be re-ordered as desired.
 *
 * \param H H matrix (dimension N x N)
 * \param b b vector (dimension N x 1)
 * \param margDim The number of rows of the state vector to marginalize
 * \param Wout Weighting matrix to be used in the prior constraint after
 * marginalization \param eout Error offset vector to be used in the prior
 * constraint after marginalization \param HstarOut Optionally output the
 * resulting Hstar matrix, which can be used later to later factor the prior
 * constraint \param bstarOut Optionally output the resulting bstar vector,
 * which can be used later to later factor the prior constraint
 */
bool schurMarginalize(const Eigen::MatrixXd& H, const Eigen::VectorXd& b,
                      const size_t& margDim, Eigen::MatrixXd& Wout,
                      Eigen::VectorXd& eout,
                      Eigen::MatrixXd* HstarOut = nullptr,
                      Eigen::VectorXd* bstarOut = nullptr);

Eigen::PermutationMatrix<Eigen::Dynamic> getPermutation(
    int dim, const std::vector<std::pair<int, int>>& segmentPositionAndSize);

void reorderNormalEquations(
    Eigen::MatrixXd& Hstar, Eigen::VectorXd& bstar,
    const std::vector<std::pair<int, int>>& segmentPositionAndSize);

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_UTILITIES_H_ */
