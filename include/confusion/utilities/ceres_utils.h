/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_CERES_UTILS_H_
#define CONFUSION_CERES_UTILS_H_

#include <ceres/ceres.h>
#include <confusion/utilities/Pose.h>

#include <Eigen/Geometry>

namespace confusion {

// A collection of random helper functions to use with Ceres Solver

// Easy way to extract the raw double values of ceres::Jet types
inline void getDouble(const double& in, double& out) { out = in; }
template <int N>
inline void getDouble(const ceres::Jet<double, N>& in, double& out) {
  out = in.a;
}
inline double getDouble(const double in) { return in; }
template <int N>
inline double getDouble(const ceres::Jet<double, N> in) {
  return in.a;
}

inline void getDoubles(const double* in, int dim, double* out) {
  for (int i = 0; i < dim; ++i) {
    out[i] = in[i];
  }
}
template <int N>
inline void getDoubles(const ceres::Jet<double, N>* in, int dim, double* out) {
  for (int i = 0; i < dim; ++i) {
    out[i] = in[i].a;
  }
}

// Easy way to extract the raw double values of a pose in a cost function (for
// example to print the values with std::cout)
inline Pose<double> getDoublesPose(const Pose<double> P) { return P; }

template <int N>
inline Pose<double> getDoublesPose(const Pose<ceres::Jet<double, N>> P) {
  Pose<double> Pout;
  getDoubles(P.trans.data(), 3, Pout.trans.data());
  getDoubles(P.rot.coeffs().data(), 4, Pout.rot.coeffs().data());
  return Pout;
}

// Return true if successful
inline bool getCovariance(ceres::Problem& problem, const double* param1,
                          const size_t param1_size, const double* param2,
                          size_t param2_size, std::string ss,
                          Eigen::MatrixXd* cov_out = nullptr) {
  ceres::Covariance::Options cov_options;
  ceres::Covariance covariance(cov_options);
  std::vector<std::pair<const double*, const double*>> covariance_blocks;
  covariance_blocks.push_back(std::make_pair(param1, param2));
  if (covariance.Compute(covariance_blocks, &problem)) {
    double cov_array[param1_size * param2_size];
    covariance.GetCovarianceBlockInTangentSpace(param1, param2, cov_array);
    Eigen::Map<Eigen::MatrixXd> cov(cov_array, param1_size, param2_size);
    if (cov_out) *cov_out = cov;
    return true;
  }
  return false;
}

// Return true if successful
// Templates necessary to support static-dimensional eigen matrices.
template <int dim_1, int dim_2, int dim_3, int dim_4>
inline bool getCovariance(
    ceres::Problem& problem, const Eigen::Matrix<double, dim_1, 1>& param_1,
    const Eigen::Matrix<double, dim_2, 1>& param_2, const std::string ss = "",
    Eigen::Matrix<double, dim_3, dim_4>* cov_out = nullptr) {
  ceres::Covariance::Options cov_options;
  ceres::Covariance covariance(cov_options);
  std::vector<std::pair<const double*, const double*>> covariance_blocks;
  covariance_blocks.push_back(std::make_pair(param_1.data(), param_2.data()));
  if (covariance.Compute(covariance_blocks, &problem)) {
    double cov_array[dim_3 * dim_4];
    covariance.GetCovarianceBlockInTangentSpace(param_1.data(), param_2.data(),
                                                cov_array);
    Eigen::Map<Eigen::Matrix<double, dim_3, dim_4>> cov(cov_array, dim_3,
                                                        dim_4);
    if (cov_out) *cov_out = cov;
    return true;
  }
  return false;
}

inline bool getCovariance(ceres::Problem& problem, const Pose<double>& p,
                          const std::string ss = "",
                          Eigen::Matrix<double, 6, 6>* cov_out = nullptr) {
  ceres::Covariance::Options cov_options;
  ceres::Covariance covariance(cov_options);
  std::vector<std::pair<const double*, const double*>> covariance_blocks;
  covariance_blocks.push_back(std::make_pair(p.trans.data(), p.trans.data()));
  covariance_blocks.push_back(
      std::make_pair(p.trans.data(), p.rot.coeffs().data()));
  covariance_blocks.push_back(
      std::make_pair(p.rot.coeffs().data(), p.trans.data()));
  covariance_blocks.push_back(
      std::make_pair(p.rot.coeffs().data(), p.rot.coeffs().data()));
  if (covariance.Compute(covariance_blocks, &problem)) {
    if (cov_out) {
      double cov_trans_trans[3 * 3];
      covariance.GetCovarianceBlockInTangentSpace(
          p.trans.data(), p.trans.data(), cov_trans_trans);
      Eigen::Map<Eigen::Matrix<double, 3, 3>> cov1(cov_trans_trans, 3, 3);
      cov_out->block<3, 3>(0, 0) = cov1;

      double cov_trans_rot[3 * 3];
      covariance.GetCovarianceBlockInTangentSpace(
          p.trans.data(), p.rot.coeffs().data(), cov_trans_rot);
      Eigen::Map<Eigen::Matrix<double, 3, 3>> cov2(cov_trans_rot, 3, 3);
      cov_out->block<3, 3>(0, 3) = cov2;

      double cov_rot_trans[3 * 3];
      covariance.GetCovarianceBlockInTangentSpace(
          p.rot.coeffs().data(), p.trans.data(), cov_rot_trans);
      Eigen::Map<Eigen::Matrix<double, 3, 3>> cov3(cov_rot_trans, 3, 3);
      cov_out->block<3, 3>(3, 0) = cov3;

      double cov_rot_rot[3 * 3];
      covariance.GetCovarianceBlockInTangentSpace(
          p.rot.coeffs().data(), p.rot.coeffs().data(), cov_rot_rot);
      Eigen::Map<Eigen::Matrix<double, 3, 3>> cov4(cov_rot_rot, 3, 3);
      cov_out->block<3, 3>(3, 3) = cov4;
    }
    return true;
  }
  return false;
}

// Build a dense eigen matrix from the ceres CRSMatrix datatype
template <typename T>
inline void buildDenseMatrix(
    const ceres::CRSMatrix Ain,
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& A) {
  A.resize(Ain.num_rows, Ain.num_cols);
  A.setZero();
  for (int r = 0; r < Ain.num_rows; ++r) {
    for (int i = Ain.rows[r]; i < Ain.rows[r + 1]; ++i) {
      A(r, Ain.cols[i]) = T(Ain.values[i]);
    }
  }
}

/**
 * Protect divide by zero with the sqrt of an auto-diff type.
 * This effectively truncates the Jacobian of sqrt(x) as x->0
 * The value of sqrt(x=0) is not impacted
 * @param error
 */
inline void safeSqrt(double& error) {
  if (error < 1e-10) error = 1e-10;
}
template <int N>
inline void safeSqrt(ceres::Jet<double, N>& error) {
  if (error.a < 1e-10) error.a = 1e-10;
}

}  // namespace confusion

#endif /* CONFUSION_CERES_UTILS_H_ */
