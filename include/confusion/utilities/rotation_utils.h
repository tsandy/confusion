/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_ROTATION_UTILS_H_
#define CONFUSION_ROTATION_UTILS_H_

#include <iostream>
#include <Eigen/Geometry>

namespace confusion {

Eigen::Vector3d quat_to_rpy(Eigen::Quaterniond quat);

Eigen::Quaterniond rpy_to_quat(double roll, double pitch, double yaw);

Eigen::Quaterniond rpy_to_quat(Eigen::Vector3d rpy);

/**
 * Get the 3D gravity vector, given the pitch, roll, and magnitude of it.
 * This is used when optimizing only the roll and pitch of the gravity vector.
 * @param r [pitch, roll] of gravity [rad]
 * @param g_mag Magnitude of gravity
 * @return Gravity vector 3x[m/sec2]
 */
template <typename T>
Eigen::Matrix<T, 3, 1> gravityVec(T const* r, const T g_mag);

/**
 * Computes the derivative of the gravity vector wrt the roll and pitch angles
 * @param r
 * @param g_mag
 * @return
 */
Eigen::Matrix<double, 3, 2> gravityVec_jacob(double const* r,
                                             const double g_mag);

template <typename T>
void QuatProduct_jacob_left(const Eigen::Quaternion<T>& z,
                            const Eigen::Quaternion<T>& w,
                            Eigen::Matrix<T, 4, 4>& dzw_dz);

template <typename T>
void QuatProduct_jacob_right(const Eigen::Quaternion<T>& z,
                             const Eigen::Quaternion<T>& w,
                             Eigen::Matrix<T, 4, 4>& dzw_dw);

template <typename T>
void QuatRotatePoint_jacob_left(const Eigen::Quaternion<T>& q,
                                const Eigen::Matrix<T, 3, 1>& pt,
                                Eigen::Matrix<T, 3, 4>& dr_dq);

template <typename T>
void QuatRotatePoint_jacob_right(const Eigen::Quaternion<T>& q,
                                 const Eigen::Matrix<T, 3, 1>& pt,
                                 Eigen::Matrix<T, 3, 3>& dr_dpt);

// Code copied from ceres/rotation.h to use eigen datatypes
template <typename T>
void quatToRotationMatrix(const Eigen::Quaternion<T>& q,
                          Eigen::Matrix<T, 3, 3>& R);

/**
 * Compute the derivative of a quaternion with respect to the rotation matrix
 * which was used to compute it.
 * @param R Rotation matrix used to get q
 * @param dq_dR
 */
template <typename T>
void calc_dq_dR(const Eigen::Matrix<T, 3, 3>& R, Eigen::Matrix<T, 4, 9>& dq_dR);

/**
 * Compute the quaternion resulting from rotating q by some delta, expressed in
 * the tangent space of q. This is used in the EigenQuaternionParameterization.
 * @param q
 * @param delta
 * @return Return q [boxPlus] delta
 */
template <typename T>
Eigen::Quaternion<T> quaternionBoxPlus(const Eigen::Quaternion<T>& q,
                                       const Eigen::Matrix<T, 3, 1>& delta);

// Computes the derivative of the q', resulting from q' = q [boxplus] dq, wrt dq
Eigen::Matrix<double, 4, 3> quaternionBoxPlusJacob(const Eigen::Quaterniond& q);

// Computes the derivative of the q', resulting from q' = q [boxplus] dq, wrt
// dq. This one considers when the yaw orientation is fixed. Used in
// FixedYawParameterization.
Eigen::Matrix<double, 4, 2> quaternionBoxPlusJacobFixedYaw(
    const Eigen::Quaterniond& q);

// This was copied from Eigen/Quaternion.h because it didn't work with
// ceres::Jet data-types.
template <typename T>
Eigen::Quaternion<T> slerp(const Eigen::Quaternion<T> q0,
                           const Eigen::Quaternion<T> q1, const T t);

/**
 * Get the derivative of a quaternion, with the body moving with angular
 * velocity omega, with respect to time. The derivative is returned in order
 * [x,y,z,w]
 *
 * \param q Orientation of the body
 * \param omega Angular velocity of the body
 */
template <typename T>
Eigen::Matrix<T, 4, 1> calulateDqDt(Eigen::Quaternion<T> q,
                                    Eigen::Matrix<T, 3, 1> omega);

/**
 * Is used to transform the velocity of a body with respect to another point on
 * that body. Can be used as: w_vel_w_a = VT(t_w_b-t_w_a) * w_vel_w_b where a
 * and b are frames attached to a rigid body and the velocities are expressed
 * with respect to reference frame w. Or w_vel_w_a = VT(q_w_a * t_a_b) *
 * w_vel_w_b.
 *
 * \param w_pos_a_b Position of frame b with respect to frame a, expressed in
 * frame w
 */
Eigen::Matrix<double, 6, 6> rigidBodyVelocityTransform(
    Eigen::Vector3d w_pos_a_b);

/**
 * Initialize the offset from the world frame to the gps frame in 2d from two
 * sets of measurement/estimate.
 * @param t_w_r0
 * @param t_w_r1
 * @param t_g_r0
 * @param t_g_r1
 * @return
 */
double initializeGpsReferenceFrameOffset(const Eigen::Vector3d& t_w_r0,
                                         const Eigen::Vector3d& t_w_r1,
                                         const Eigen::Vector3d& t_g_r0,
                                         const Eigen::Vector3d& t_g_r1,
                                         Eigen::Vector3d& t_w_g,
                                         double& theta_w_g);

}  // namespace confusion

#include "confusion/utilities/impl/rotation_utils.h"

#endif /* CONFUSION_ROTATION_UTILS_H_ */
