/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_BATCHFUSOR_H_
#define INCLUDE_CONFUSION_BATCHFUSOR_H_

//#define OPT_DEBUG

#include <ceres/ceres.h>

#include <deque>
#include <memory>

#include "confusion/MeasurementManager.h"
#include "confusion/ProcessChain.h"
#include "confusion/StateVector.h"
#include "confusion/StaticParameterVector.h"
#include "confusion/UpdateMeasurement.h"
#include "confusion/utilities/ceres_utils.h"

namespace confusion {

class BatchFusor {
 public:
  BatchFusor(StateVector& stateVector, StaticParameterVector& staticParameters);

  ~BatchFusor();

  void setVerbose() { verbose_ = true; }
  void unsetVerbose() { verbose_ = false; }

  void setSolverOptions(int numThreads, int maxIterations);

  void createStatesFromMeasurements(
      std::deque<std::shared_ptr<ProcessMeasurement>>& processMeasurements,
      std::deque<std::shared_ptr<UpdateMeasurement>>& updateMeasurements);

  void buildProblem();
  void optimize();

  void getResiduals(std::vector<Eigen::VectorXd>& processResiduals,
                    std::vector<Eigen::VectorXd>& updateResiduals);

  void reset() { stateVector_.reset(); }

  void setIterationCallback(std::shared_ptr<ceres::IterationCallback> callback);

  void printParameterCovariance(double* d, std::string paramName);

  StateVector& stateVector_;
  StaticParameterVector& staticParameters_;

  ceres::Problem* problem_ = nullptr;
  ceres::Solver::Summary summary_;
  std::shared_ptr<ceres::IterationCallback> callback_;
  ceres::Solver::Options solverOptions_;

 private:
  bool verbose_ = false;
};

}  // namespace confusion

#endif /* INCLUDE_CONFUSION_BATCHFUSOR_H_ */
