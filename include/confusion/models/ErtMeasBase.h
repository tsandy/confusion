/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_ERTMEASBASE_H
#define CONFUSION_ERTMEASBASE_H

#include "confusion/UpdateMeasurement.h"
#include "confusion/models/ErtMeasConfig.h"
#include "confusion/utilities/Pose.h"

namespace confusion {

class ErtMeasBase : public confusion::UpdateMeasurement {
 public:
  ErtMeasBase(const double t, const std::string& referenceFrameName,
              const std::string& sensorFrameName, const ErtMeasConfig& config,
              const std::string& measName, const int measurement_type = 0,
              int starting_state_param_index = 0);

  void setFrameNames(const std::string& referenceFrameName,
                     const std::string& sensorFrameName);

  void assignExternalReferenceFrame(
      std::shared_ptr<confusion::Pose<double>> erf);

  virtual confusion::Pose<double> getMeasuredPose() const = 0;

  /**
   * This is called when trying to assign the measurement to the state, if the
   * external reference frame offset has not yet been initialized. If it is too
   * soon to initialize, return a nullptr.
   * @param T_w_body Estimate of the body pose at the time of the measurement
   * @return Pointer to the initial value for T_w_ref. If the reference frame
   * offset shouldn't be initialized, return a nullptr.
   */
  virtual std::shared_ptr<confusion::Pose<double>>
  initializeReferenceFrameOffset(const double& t,
                                 const Pose<double>& T_w_body) = 0;

  const ErtMeasConfig& config() const { return config_; }
  const std::string& referenceFrameName() const { return referenceFrameName_; }
  const std::string& sensorFrameName() const { return sensorFrameName_; }

 protected:
  const ErtMeasConfig& config_;
  std::string referenceFrameName_;  ///< Name of the reference frame. Used to
                                    ///< link the measurement to the correct
                                    ///< external frame offset.
  std::string sensorFrameName_;     ///< Name of the linked child frame

  // Indicates the index of the state parameters which contains the position of
  // the measured body pose. For a full pose, the orientation must then be the
  // next parameter.
  const int starting_state_param_index_;

  std::shared_ptr<confusion::Pose<double>> T_w_ref_ptr_;
};

}  // namespace confusion

#endif  // CONFUSION_ERTMEASBASE_H
