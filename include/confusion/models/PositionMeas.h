/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_POSITIONMEAS_H
#define INCLUDE_CONFUSION_POSITIONMEAS_H

#include "confusion/UpdateMeasurement.h"
#include "confusion/models/ErtMeasBase.h"
#include "confusion/utilities/Pose.h"

namespace confusion {

class PositionMeas;

/**
 * Computes an error between the estimated target position (extracted from the
 * state) and the measured target position (measured with some arbitrary
 * external tracking system). We measure t_wa_ba. We estimate T_w_i. w: World
 * frame of the estimator wa: World frame of the external tracking system i:
 * Frame of the IMU which is tracked by the estimator ba: Target point of the
 * external tracking system. This must be ridigly coupled to the IMU frame. We
 * can additionally estimate the two offsets to the external measurements:
 * T_w_wa: World frame offset. If gravity aligned, you could use a
 * confusion::FixedYawParameterization on this offset's orientation with
 * GRAVITY_OPT off t_i_ba: Target position offset
 */
class PositionCost {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  explicit PositionCost(PositionMeas* pose_meas) : positionMeas_(pose_meas) {}
  ~PositionCost() = default;

  template <typename T>
  bool operator()(T const* t_w_i_, T const* q_w_i_, T const* t_w_wa_,
                  T const* q_w_wa_, T const* t_i_ba_, T* residual_) const;

 private:
  PositionMeas* positionMeas_;
};

/**
 * Measurement from an external tracking system
 */
class PositionMeas : public ErtMeasBase {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  PositionMeas(const double t, const std::string& referenceFrameName,
               const std::string& sensorFrameName,
               Eigen::Vector3d& t_imu_sensor, const Eigen::Vector3d& t_wa_ba,
               const ErtMeasConfig& config, const int measurement_type = 0,
               int starting_state_param_index = 0)
      : ErtMeasBase(t, referenceFrameName, sensorFrameName, config, "Position",
                    measurement_type, starting_state_param_index),
        t_imu_sensor_(t_imu_sensor),
        t_wa_ba_(t_wa_ba) {}

  bool createCostFunction(
      std::unique_ptr<ceres::CostFunction>& costFunctionPtr,
      std::unique_ptr<ceres::LossFunction>& lossFunctionPtr,
      std::vector<size_t>& stateParameterIndexVector,
      std::vector<double*>& staticParameterDataVector) final {
    stateParameterIndexVector.push_back(starting_state_param_index_);
    stateParameterIndexVector.push_back(starting_state_param_index_ + 1);

    staticParameterDataVector.push_back(T_w_ref_ptr_->trans.data());
    staticParameterDataVector.push_back(T_w_ref_ptr_->rot.coeffs().data());
    staticParameterDataVector.push_back(t_imu_sensor_.data());

    std::unique_ptr<ceres::CostFunction> costFunctionPtr_(
        new ceres::AutoDiffCostFunction<PositionCost, 3, 3, 4, 3, 4, 3>(
            new PositionCost(this)));
    costFunctionPtr = std::move(costFunctionPtr_);

    if (!config_.useLossFunction)
      lossFunctionPtr.reset();
    else {
      std::unique_ptr<ceres::LossFunction> lossFunctionPtr_(
          new ceres::HuberLoss(config_.lossCoefficient));
      lossFunctionPtr = std::move(lossFunctionPtr_);
    }

    return true;
  }

  std::shared_ptr<confusion::Pose<double>> initializeReferenceFrameOffset(
      const double& t, const Pose<double>& T_w_body) final {
    if (!config_.gpsRefFrameInitializer_.startStateSet_) {
      config_.gpsRefFrameInitializer_.t0_ = t;
      config_.gpsRefFrameInitializer_.t_w_sensor0_ = T_w_body * t_imu_sensor_;
      config_.gpsRefFrameInitializer_.t_wa_ba0_ = t_wa_ba_;
      config_.gpsRefFrameInitializer_.startStateSet_ = true;
    } else if (t - config_.gpsRefFrameInitializer_.t0_ > 60.0) {
      // Reset the starting state if its been too long.
      config_.gpsRefFrameInitializer_.t0_ = t;
      config_.gpsRefFrameInitializer_.t_w_sensor0_ = T_w_body * t_imu_sensor_;
      config_.gpsRefFrameInitializer_.t_wa_ba0_ = t_wa_ba_;
    } else {
      // The robot needs to move one meter before the reference frame is
      // initialized.
      if ((t_wa_ba_ - config_.gpsRefFrameInitializer_.t_wa_ba0_).norm() > 1.0) {
        Eigen::Vector3d t_w_sensor1 = T_w_body * t_imu_sensor_;
        Eigen::Vector3d t_wa_ba1_ = t_wa_ba_;

        Eigen::Vector3d t_w_g;
        Eigen::Vector3d rpy_w_g;
        rpy_w_g.setZero();
        std::cout << "initializeGpsReferenceFrameOffset with:\nt_w_sensor0="
                  << config_.gpsRefFrameInitializer_.t_w_sensor0_.transpose()
                  << ", t_w_sensor1=" << t_w_sensor1.transpose()
                  << "\nt_g_sensor0="
                  << config_.gpsRefFrameInitializer_.t_wa_ba0_.transpose()
                  << ", t_q_sensor1=" << t_wa_ba1_.transpose() << std::endl;
        double res = initializeGpsReferenceFrameOffset(
            config_.gpsRefFrameInitializer_.t_w_sensor0_, t_w_sensor1,
            config_.gpsRefFrameInitializer_.t_wa_ba0_, t_wa_ba1_, t_w_g,
            rpy_w_g(2));
        std::cout << "initializeGpsReferenceFrameOffset returned " << res
                  << ". t_w_g=" << t_w_g.transpose()
                  << ", theta_w_g=" << rpy_w_g(2) << std::endl;

        if (res < 0.1) {  // Make the threshold configurable. This is the
                          // discrepancy between the two aligned poses [m].
          config_.gpsRefFrameInitializer_.startStateSet_ =
              false;  // So that we will start the search over if the sensor is
                      // reset
          return std::make_shared<confusion::Pose<double>>(
              t_w_g, confusion::rpy_to_quat(rpy_w_g));
        }

        std::cout << "initializeGpsReferenceFrameOffset did not find a "
                     "sufficiently accurate solution. Will wait for more robot "
                     "motion and try again..."
                  << std::endl;

        // Reset the starting state
        config_.gpsRefFrameInitializer_.t0_ = t;
        config_.gpsRefFrameInitializer_.t_w_sensor0_ = T_w_body * t_imu_sensor_;
        config_.gpsRefFrameInitializer_.t_wa_ba0_ = t_wa_ba_;
      }
    }

    return nullptr;
  }

  confusion::Pose<double> getMeasuredPose() const final {
    return confusion::Pose<double>(t_wa_ba_, Eigen::Quaterniond::Identity());
  }

  int residualDimension() final { return 3; }

  const Eigen::Vector3d& t_wa_ba() const { return t_wa_ba_; }
  const Eigen::Vector3d& t_imu_sensor() const { return t_imu_sensor_; }

 private:
  const Eigen::Vector3d t_wa_ba_;  // Measured position
  Eigen::Vector3d&
      t_imu_sensor_;  // Target frame offset to IMU (only used to set the
                      // address of the parameters internally)
};

template <typename T>
bool PositionCost::operator()(T const* t_w_i_, T const* q_w_i_,
                              T const* t_w_wa_, T const* q_w_wa_,
                              T const* t_i_ba_, T* residual_) const {
#ifdef COST_DEBUG
  std::cout << "Starting pose cost computation" << std::endl;
#endif

  Eigen::Matrix<T, 3, 1> t_w_i(t_w_i_);
  Eigen::Quaternion<T> q_w_i(q_w_i_);
  confusion::Pose<T> T_w_i(t_w_i, q_w_i);

  Eigen::Matrix<T, 3, 1> t_w_wa(t_w_wa_);
  Eigen::Quaternion<T> q_w_wa(q_w_wa_);
  confusion::Pose<T> T_w_wa(t_w_wa, q_w_wa);

  Eigen::Matrix<T, 3, 1> t_i_ba(t_i_ba_);

  // Get the estimated measurement
  Eigen::Matrix<T, 3, 1> t_wa_ba_est = T_w_wa.inverse() * T_w_i * t_i_ba;

  // Compute the residual
  confusion::VectorDistance(positionMeas_->t_wa_ba().data(), t_wa_ba_est.data(),
                            residual_);

  residual_[0] *= T(positionMeas_->config().w_trans);
  residual_[1] *= T(positionMeas_->config().w_trans);
  residual_[2] *= T(positionMeas_->config().w_trans);

#ifdef COST_DEBUG
  std::cout << "Done PositionCost" << std::endl;

  Eigen::Matrix<double, 3, 1> res;
  confusion::getDoubles(residual_, 3, res.data());
  std::cout << "Position cost = [" << res.transpose() << "]" << std::endl;
#endif

  return true;
}

}  // namespace confusion

#endif  // INCLUDE_CONFUSION_POSITIONMEAS_H
