/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONFUSION_IMU_CHAIN_H_
#define CONFUSION_IMU_CHAIN_H_

#include <Eigen/Core>
#include <deque>

#include "ceres/ceres.h"
#include "confusion/ProcessChain.h"
#include "confusion/utilities/ceres_utils.h"
#include "confusion/utilities/distances.h"
#include "confusion/utilities/imu_utils.h"
#include "confusion/utilities/rotation_utils.h"

namespace confusion {

// A complicated structure is required here to support different type ceres cost
// functions depending on whether the gravity direction attitude is being
// estimated while using a common cost function evaluator function.

class ImuChain;

bool evaluateImuChainCost(ImuChain* imuChain, double const* const* x,
                          double* residuals, double** jacobians);

class ImuChainCostFuntion
    : public ceres::SizedCostFunction<15, 3, 4, 3, 3, 3, 3, 4, 3, 3, 3> {
 public:
  ImuChainCostFuntion(ImuChain* imuChain);

  /**
   * Compute the error. This also computes the analytical Jacobians. See the
   * Ceres documentation for AnalyticalCostFunction for implementation details.
   * NOTE: The cost function assumes that the cost weighting (i.e. the stiffness
   * matrix) is constant. This seems to be a sufficiently good approximation.
   * @param parameters
   * @param residuals
   * @param jacobians
   * @return
   */
  bool Evaluate(double const* const* parameters, double* residuals,
                double** jacobians) const;

  ImuChain* imuChain_;
};

class ImuChainCostFuntionOptGravity : public ceres::CostFunction {
 public:
  ImuChainCostFuntionOptGravity(ImuChain* imuChain);

  /**
   * Compute the error. This also computes the analytical Jacobians. See the
   * Ceres documentation for AnalyticalCostFunction for implementation details.
   * NOTE: The cost function assumes that the cost weighting (i.e. the stiffness
   * matrix) is constant. This seems to be a sufficiently good approximation.
   * @param parameters
   * @param residuals
   * @param jacobians
   * @return
   */
  bool Evaluate(double const* const* parameters, double* residuals,
                double** jacobians) const;

  ImuChain* imuChain_;
};

/**
 * This class is an error term for a chain of IMU measurements between two state
 * instances. The state is: [w_pos_i, w_quat_i, w_linVel_i, accelBias,
 * gyroBias]. It assumes that the IMU state appears at the front of the state
 * vector. If this is not the case, a derived version can be created with a
 * derived initialize() function.
 */
class ImuChain : public ProcessChain {
 public:
  /**
   * Constructor
   * @param optGravity Set true when the roll and pitch of the gravity vector
   * are used as optimized parameters. When false, the world reference frame
   * must be aligned with gravity.
   * @param startingStateParamIndex The state parameter indices linked to this
   * 		measurement model start at the specified index. It is then assumed
   * that the IMU translation, rotation, linear velocity, accel bias, and gyro
   * 		bias are ordered sequentially in this order starting from that
   * index.
   */
  ImuChain(bool optGravity = true, int startingStateParamIndex = 0);

  bool createCostFunction(std::unique_ptr<ceres::CostFunction>& costFunctionPtr,
                          std::unique_ptr<ceres::LossFunction>& lossFunctionPtr,
                          std::vector<size_t>& stateParameterIndexVector,
                          std::vector<double*>& staticParameterDataVector);

  int residualDimension();

  void setForUnitTestTrue();

  bool optGravity() const { return optGravity_; }
  bool forUnitTest() const { return forUnitTest_; }

 protected:
  const bool optGravity_;
  const int startingStateParamIndex_;
  bool forUnitTest_ = false;
};

}  // namespace confusion

#endif  // CONFUSION_IMU_CHAIN_H_
