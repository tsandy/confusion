/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_CONFUSION_POSEMEAS_H
#define INCLUDE_CONFUSION_POSEMEAS_H

#include "confusion/UpdateMeasurement.h"
#include "confusion/models/ErtMeasBase.h"
#include "confusion/utilities/Pose.h"

namespace confusion {

class PoseMeas;

/**
 * Computes an error between the estimated target pose (extracted from the
 * state) and the measured target pose (measured with some arbitrary external
 * tracking system). We measure T_wa_ta. We estimate T_w_i. w: World frame of
 * the estimator wa: World frame of the external tracking system i: Frame of the
 * IMU which is tracked by the estimator ta: Traget frame of the external
 * tracking system. This frame must be ridigly coupled to the IMU frame. We can
 * additionally estimate the two offsets to the external measurements: T_w_wa:
 * World frame offset. If gravity aligned, you could use a
 * confusion::FixedYawParameterization on this offset's orientation with
 * GRAVITY_OPT off T_i_ba: Target frame offset
 */
class PoseCost {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  PoseCost(PoseMeas* pose_meas) : pose_meas_(pose_meas) {}
  ~PoseCost(){};

  template <typename T>
  bool operator()(T const* t_w_i_, T const* q_w_i_, T const* t_w_wa_,
                  T const* q_w_wa_, T const* t_i_ba_, T const* q_i_ba_,
                  T* residual_) const;

  template <typename T>
  bool operator()(T const* t_w_i_, T const* q_w_i_, T const* t_w_wa_,
                  T const* q_w_wa_, T const* t_i_ba_, T const* q_i_ba_,
                  T const* scale_, T* residual_) const;

 private:
  PoseMeas* pose_meas_;
};

/**
 * Measurement from an external tracking system
 */
class PoseMeas : public ErtMeasBase {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  PoseMeas(const double t, const std::string referenceFrameName,
           const std::string& sensorFrameName,
           confusion::Pose<double>& T_imu_sensor,
           const confusion::Pose<double>& T_wa_ba, const ErtMeasConfig& config,
           const int measurement_type = 0, int starting_state_param_index = 0)
      : ErtMeasBase(t, referenceFrameName, sensorFrameName, config, "Pose",
                    measurement_type, starting_state_param_index),
        T_imu_sensor_(T_imu_sensor),
        T_wa_ba_(T_wa_ba),
        scale_(NULL) {}

  PoseMeas(const double t, const std::string referenceFrameName,
           const std::string& sensorFrameName,
           confusion::Pose<double>& T_imu_sensor,
           const confusion::Pose<double>& T_wa_ba, const ErtMeasConfig& config,
           double* scale, const int measurement_type = 0,
           int starting_state_param_index = 0)
      : ErtMeasBase(t, referenceFrameName, sensorFrameName, config, "Pose",
                    measurement_type, starting_state_param_index),
        T_imu_sensor_(T_imu_sensor),
        T_wa_ba_(T_wa_ba),
        scale_(scale),
        optimize_scale_(true) {}

  bool createCostFunction(
      std::unique_ptr<ceres::CostFunction>& costFunctionPtr,
      std::unique_ptr<ceres::LossFunction>& lossFunctionPtr,
      std::vector<size_t>& stateParameterIndexVector,
      std::vector<double*>& staticParameterDataVector) final {
    stateParameterIndexVector.push_back(starting_state_param_index_);
    stateParameterIndexVector.push_back(starting_state_param_index_ + 1);

    staticParameterDataVector.push_back(T_w_ref_ptr_->trans.data());
    staticParameterDataVector.push_back(T_w_ref_ptr_->rot.coeffs().data());
    staticParameterDataVector.push_back(T_imu_sensor_.trans.data());
    staticParameterDataVector.push_back(T_imu_sensor_.rot.coeffs().data());

    std::unique_ptr<ceres::CostFunction> costFunctionPtr_(
        new ceres::AutoDiffCostFunction<PoseCost, 6, 3, 4, 3, 4, 3, 4>(
            new PoseCost(this)));
    costFunctionPtr = std::move(costFunctionPtr_);

    if (!config_.useLossFunction)
      lossFunctionPtr.reset();
    else {
      std::unique_ptr<ceres::LossFunction> lossFunctionPtr_(
          new ceres::HuberLoss(config_.lossCoefficient));
      lossFunctionPtr = std::move(lossFunctionPtr_);
    }

    return true;
  }

  std::shared_ptr<confusion::Pose<double>> initializeReferenceFrameOffset(
      const double& t, const Pose<double>& T_w_body) final {
    return std::make_shared<confusion::Pose<double>>(T_w_body * T_imu_sensor_ *
                                                     T_wa_ba_.inverse());
  }

  confusion::Pose<double> getMeasuredPose() const final { return T_wa_ba_; }

  confusion::Pose<double> getMeasuredImuPose() {
    if (!T_w_ref_ptr_) {
      std::cout << "ERROR: PoseMeas::getMeasuredImuPose called with invalid "
                   "T_w_ref_ptr_!"
                << std::endl;
      return {};
    }
    return (*T_w_ref_ptr_) * T_wa_ba_ * T_imu_sensor_.inverse();
  }

  int residualDimension() final { return 6; }

  const confusion::Pose<double>& T_wa_ba() const { return T_wa_ba_; }
  const confusion::Pose<double>& T_imu_sensor() const { return T_imu_sensor_; }

 private:
  const confusion::Pose<double> T_wa_ba_;  ///< Measured pose
  confusion::Pose<double>&
      T_imu_sensor_;  ///< Target frame offset to IMU (only used to set the
                      ///< address of the parameters internally)
  double* scale_;     ///< Scale applied on translation. Usually 1.
  bool optimize_scale_ = false;
};

template <typename T>
bool PoseCost::operator()(T const* t_w_i_, T const* q_w_i_, T const* t_w_wa_,
                          T const* q_w_wa_, T const* t_i_ba_, T const* q_i_ba_,
                          T* residual_) const {
#ifdef COST_DEBUG
  std::cout << "Starting pose cost computation" << std::endl;
#endif

  Eigen::Matrix<T, 3, 1> t_w_i(t_w_i_);
  Eigen::Quaternion<T> q_w_i(q_w_i_);
  confusion::Pose<T> T_w_i(t_w_i, q_w_i);

  Eigen::Matrix<T, 3, 1> t_w_wa(t_w_wa_);
  Eigen::Quaternion<T> q_w_wa(q_w_wa_);
  confusion::Pose<T> T_w_wa(t_w_wa, q_w_wa);

  Eigen::Matrix<T, 3, 1> t_i_ba(t_i_ba_);
  Eigen::Quaternion<T> q_i_ba(q_i_ba_);
  confusion::Pose<T> T_i_ba(t_i_ba, q_i_ba);

  // Get the estimated measurement
  confusion::Pose<T> T_wa_ba_est = T_w_wa.inverse() * T_w_i * T_i_ba;

  // Compute the residuals
  confusion::VectorDistance(pose_meas_->T_wa_ba().trans.data(),
                            T_wa_ba_est.trans.data(), residual_);
  confusion::QuatDistance(pose_meas_->T_wa_ba().rot, T_wa_ba_est.rot,
                          residual_ + 3);

  residual_[0] *= T(pose_meas_->config().w_trans);
  residual_[1] *= T(pose_meas_->config().w_trans);
  residual_[2] *= T(pose_meas_->config().w_trans);
  residual_[3] *= T(pose_meas_->config().w_rot);
  residual_[4] *= T(pose_meas_->config().w_rot);
  residual_[5] *= T(pose_meas_->config().w_rot);

#ifdef COST_DEBUG
  std::cout << "Done PoseCost" << std::endl;

  Eigen::Matrix<double, 6, 1> res;
  confusion::getDoubles(residual_, 6, res.data());
  std::cout << "Pose cost = [" << res.transpose() << "]" << std::endl;
#endif

  return true;
}

template <typename T>
bool PoseCost::operator()(T const* t_w_i_, T const* q_w_i_, T const* t_w_wa_,
                          T const* q_w_wa_, T const* t_i_ba_, T const* q_i_ba_,
                          T const* scale_, T* residual_) const {
#ifdef COST_DEBUG
  std::cout << "Starting pose cost computation" << std::endl;
#endif

  Eigen::Matrix<T, 3, 1> t_w_i(t_w_i_);
  Eigen::Quaternion<T> q_w_i(q_w_i_);
  confusion::Pose<T> T_w_i(t_w_i, q_w_i);

  Eigen::Matrix<T, 3, 1> t_w_wa(t_w_wa_);
  Eigen::Quaternion<T> q_w_wa(q_w_wa_);
  confusion::Pose<T> T_w_wa(t_w_wa, q_w_wa);

  Eigen::Matrix<T, 3, 1> t_i_ba(t_i_ba_);
  Eigen::Quaternion<T> q_i_ba(q_i_ba_);
  confusion::Pose<T> T_i_ba(t_i_ba, q_i_ba);

  // Get the estimated measurement
  confusion::Pose<T> T_wa_ba_est = T_w_wa.inverse() * T_w_i * T_i_ba;

  // Compute the residuals
  confusion::ScaledVectorDistance(pose_meas_->T_wa_ba().trans.data(),
                                  T_wa_ba_est.trans.data(), scale_, residual_);
  confusion::QuatDistance(pose_meas_->T_wa_ba().rot, T_wa_ba_est.rot,
                          residual_ + 3);

  residual_[0] *= T(pose_meas_->config().w_trans);
  residual_[1] *= T(pose_meas_->config().w_trans);
  residual_[2] *= T(pose_meas_->config().w_trans);
  residual_[3] *= T(pose_meas_->config().w_rot);
  residual_[4] *= T(pose_meas_->config().w_rot);
  residual_[5] *= T(pose_meas_->config().w_rot);

#ifdef COST_DEBUG
  std::cout << "Done PoseCost" << std::endl;

  Eigen::Matrix<double, 6, 1> res;
  confusion::getDoubles(residual_, 6, res.data());
  std::cout << "Pose cost = [" << res.transpose() << "]" << std::endl;
#endif

  return true;
}

}  // namespace confusion

#endif  // INCLUDE_CONFUSION_POSEMEAS_H
