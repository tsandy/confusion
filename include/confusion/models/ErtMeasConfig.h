/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef CONFUSION_ERTMEASCONFIG_H
#define CONFUSION_ERTMEASCONFIG_H

namespace confusion {

struct GpsRefFrameInitializer {
  Eigen::Vector3d t_w_sensor0_, t_w_sensor1_; ///< Measurements
  Eigen::Vector3d t_wa_ba0_, t_wa_ba1_; ///< Estimates
  double t0_, t1_;
  bool startStateSet_ = false;
};

struct ErtMeasConfig {
  std::string sensorFrameName_;
  std::string referenceFrameName_;
  bool active_ = false;

  double w_trans;                         // [m] Translational inverse standard deviation
  double w_rot;                           // [rad] Rotational inverse standard deviation
  bool useLossFunction = false;           // Use a Huber loss function for the residual
  double lossCoefficient = 0.0;           // Huber loss coefficient
  double sensorOffset_trans_init_stddev;  // [m] Initial certainty in the translational part of the extrinsic calibration
  double sensorOffset_rot_init_stddev;    // [m] Initial certainty in the rotational part of the extrinsic calibration
  bool optTranslationalExtrinsic = false; // Optimize the translational part of the extrinsic calibration online
  bool optRotationalExtrinsic = false;    // Optimize the rotational part of the extrinsic calibration online
  bool optScale = false;                  // Optimize scale online
  bool maxRateSpecified = false;          // Indicates if measurements should only be accepted at a desired max rate
  double minMeasInterval;                 // The minimum time interval between accepted measurements, to limit the max measurement rate.
  double tLastMeas = 0.0;                 // Timestamp of the last measurement used in the estimator.
  bool useRefeferenceFrameOffsetRwp = false;  //Use a random walk process on the reference frame
  double refOffsetTransRwpProcessNoise;  // t_w_wref random walk process noise [m * sqrt(sec)]
  double refOffsetRotRwpProcessNoise;  // q_w_wref random walk process noise [m * sqrt(sec)]
  bool positionOnly_ = false;  // Only a position measurement is given
  std::string sensorFrameNameForTf_;  // The user can optionally override the sensor frame name for the published tf. To be used if that tf is being published somewhere else.

  bool useRefOffsetInitStddev_ = false;  // Indicates if an initial constraint should be applied to the reference frame offset once created
  double refOffsetTransInitStddev_;  // [m] Initial confidence in the translational part of the offset between the world and the sensor reference frame
  double refOffsetRotInitStddev_;  // [m] Initial confidence in the rotational part of the offset between the world and the sensor reference frame

  mutable GpsRefFrameInitializer gpsRefFrameInitializer_; // The PositionMeas needs to modify this to store information about the last measurements received before initialization.
};

} // namespace confusion

#endif //CONFUSION_ERTMEASCONFIG_H
