/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */

#ifndef CONFUSION_LOCAL_PARAMETERIZATION_TEST_FIXTURE_H_
#define CONFUSION_LOCAL_PARAMETERIZATION_TEST_FIXTURE_H_

#include <gtest/gtest.h>

#include "confusion/LocalParameterization.h"

namespace confusion {

class LocalParameterizationTest : public ::testing::Test {
 protected:
  void Initialize(const std::shared_ptr<LocalParameterizationBase>& local_param,
                  double dx_max_step, bool normalize_params);

  void TestLocalParameterizationBoxPlusJacobian();

  void TestLocalParameterizationBoxMinusJacobianLeft();

  void TestLocalParameterizationBoxMinusJacobianRight();

  // Note that this test doesn't have to pass for the local parameterization to
  // be valid. This just tests if the plus and minus operators are consistent
  // with eachother, which isn't required for use in sensor fusion.
  void TestLocalParameterizationPlusThenMinus();

 private:
  void TestLocalParameterizationBoxMinusJacobian(bool test_left_param_jacobian);

  static std::string PrintForBoxPlusJacobianTest(
      Eigen::VectorXd x_in, Eigen::VectorXd dx, Eigen::VectorXd x_out,
      Eigen::VectorXd ddx, Eigen::VectorXd x_out_pert, Eigen::VectorXd dx_out,
      Eigen::VectorXd dxout_test);

  static std::string PrintForBoxMinusJacobianTest(
      Eigen::VectorXd x0, Eigen::VectorXd x1,
      Eigen::VectorXd random_parameter_step, bool test_left_param_jacobian,
      Eigen::VectorXd x_pert, Eigen::VectorXd dx, Eigen::VectorXd dx_pert,
      Eigen::VectorXd ddx, Eigen::VectorXd ddx_est);

  std::string PrintForPlusThenMinusTest(
      Eigen::VectorXd x_in, Eigen::VectorXd x_out,
      Eigen::VectorXd dx, Eigen::VectorXd dx_calc);

  std::shared_ptr<LocalParameterizationBase> local_param_;

  double dx_max_step_;
  bool normalize_params_;

  double tolerance = 1e-9;
  int num_tests = 10;
};

}  // namespace confusion

#endif  // CONFUSION_LOCAL_PARAMETERIZATION_TEST_FIXTURE_H_
