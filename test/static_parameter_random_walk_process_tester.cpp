/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ceres/gradient_checker.h>
#include <gtest/gtest.h>

#include "confusion/LocalParameterization.h"
#include "confusion/utilities/StaticParameterRandomWalkProcess.h"

void TestRWPs(confusion::StaticParameterRandomWalkProcess& rwp_uniform,
              std::vector<double*>& parameter_blocks) {
  constexpr bool kDebugPrint = false;
  ceres::NumericDiffOptions numeric_diff_options;
  ceres::GradientChecker gradient_checker(&rwp_uniform, nullptr,
                                          numeric_diff_options);
  ceres::GradientChecker::ProbeResults probe_results;
  gradient_checker.Probe(parameter_blocks.data(), 1.0e-9, &probe_results);
  if (kDebugPrint) {
    std::cout << "error log: " << probe_results.error_log << std::endl;
    std::cout << "max error = " << probe_results.maximum_relative_error
              << std::endl;
  }
  EXPECT_TRUE(probe_results.return_value);
}

TEST(positions_jacobian_test, ShouldPass) {
  Eigen::Vector3d pos_prior, pos_estimated, offset;
  pos_prior.setRandom();
  offset.setConstant(0.1);
  pos_estimated = pos_prior + offset;

  double process_noise = 1.0;

  size_t global_size = pos_prior.size();
  confusion::StaticParameterRandomWalkProcess rwp_uniform(
      pos_prior.data(), pos_estimated.data(), global_size, process_noise);

  rwp_uniform.updateDt(1.0);

  // Check gradients
  std::vector<double*> parameter_blocks;
  parameter_blocks.push_back(pos_prior.data());
  parameter_blocks.push_back(pos_estimated.data());

  TestRWPs(rwp_uniform, parameter_blocks);
}

TEST(quaternions_jacobian_test, ShouldPass) {
  Eigen::Quaterniond pos_prior, pos_estimated;
  pos_prior.coeffs().setRandom();
  pos_prior.normalize();
  pos_estimated.coeffs().setRandom();
  pos_estimated.normalize();

  double process_noise = 1.0;
  confusion::QuatParam quat_param;

  confusion::StaticParameterRandomWalkProcess rwp(pos_prior.coeffs().data(),
                                                  pos_estimated.coeffs().data(),
                                                  &quat_param, process_noise);

  rwp.updateDt(1.0);

  // Check gradients
  std::vector<double*> parameter_blocks;
  parameter_blocks.push_back(pos_prior.coeffs().data());
  parameter_blocks.push_back(pos_estimated.coeffs().data());

  TestRWPs(rwp, parameter_blocks);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  std::cout.precision(8);

  google::InitGoogleLogging(argv[0]);
  FLAGS_stderrthreshold = 0;
  FLAGS_colorlogtostderr = 1;

  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
