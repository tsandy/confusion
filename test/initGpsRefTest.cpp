//
// Created by tim on 05.09.19.
//

#include <confusion/utilities/Pose.h>
#include <confusion/utilities/rotation_utils.h>
#include <gtest/gtest.h>

const double _threshold_ = 1e-10;

void runTest(const Eigen::Vector3d& t_w_r0, const Eigen::Vector3d& t_w_r1,
             const Eigen::Vector3d& t_g_r0, const Eigen::Vector3d& t_g_r1,
             Eigen::Vector2d& t_w_g_expected, double& theta_w_g_expected) {
  Eigen::Vector3d t_w_g;
  double theta_w_g;
  double res = confusion::initializeGpsReferenceFrameOffset(
      t_w_r0, t_w_r1, t_g_r0, t_g_r1, t_w_g, theta_w_g);

  std::cout << "t_w_g: " << t_w_g.transpose() << "; theta_w_g: " << theta_w_g
            << "; res: " << res << std::endl;
  EXPECT_LT(fabs(theta_w_g - theta_w_g_expected), _threshold_);
  EXPECT_LT(fabs(t_w_g(0) - t_w_g_expected(0)), _threshold_);
  EXPECT_LT(fabs(t_w_g(1) - t_w_g_expected(1)), _threshold_);
}

TEST(initializeGpsReferenceFrameOffsetTest, ShouldPass) {
  std::cout << "\n\n" << std::endl;

  // Hand-made example problems
  Eigen::Vector2d t_w_g;
  t_w_g << 0, 1;
  double theta_w_g = 0.0;

  // First robot position at (1,0) in world
  Eigen::Vector3d t_w_r0;
  t_w_r0 << 1, 0, 0;
  Eigen::Vector3d t_g_r0;
  t_g_r0 << 1, -1, 0;

  // First robot position at (1,1) in world
  Eigen::Vector3d t_w_r1;
  t_w_r1 << 1, 1, 0;
  Eigen::Vector3d t_g_r1;
  t_g_r1 << 1, 0, 0;

  runTest(t_w_r0, t_w_r1, t_g_r0, t_g_r1, t_w_g, theta_w_g);

  // Rotate GPS frame and move it farther out
  t_w_g << 0, 2;
  theta_w_g = M_PI / 2.0;
  t_g_r0 << -2, -1, 0;
  t_g_r1 << -1, -1, 0;
  runTest(t_w_r0, t_w_r1, t_g_r0, t_g_r1, t_w_g, theta_w_g);

  // Random tests perfectly in the plane
  // I also tested with small perturbations in the Z-direction, but the test
  // fails due to the tight threshold.
  for (int i = 0; i < 10; ++i) {
    // Set a random T_w_g, with only small components out of the plane
    Eigen::Vector3d t_w_g;
    t_w_g.setRandom();
    t_w_g(2) = 0.0;  // t_w_g(2) *= 0.1;
    Eigen::Vector3d rpy_w_g;
    rpy_w_g.setRandom();
    rpy_w_g(0) = 0.0;
    rpy_w_g(1) = 0.0;  // rpy_w_g.head<2>() *= 0.001; //
    std::cout << "TEST -- t_w_g: " << t_w_g.transpose()
              << "; theta_w_g: " << rpy_w_g(2) << std::endl;

    confusion::Pose<double> T_w_g(t_w_g, confusion::rpy_to_quat(rpy_w_g));
    T_w_g.print("T_w_g");

    // Set two random robot positions at the same height
    t_w_r0.setRandom();
    t_w_r1.setRandom();
    t_w_r1(2) = t_w_r0(2);

    // Get the G positions
    Eigen::Vector3d t_g_r0 = T_w_g.inverse() * t_w_r0;
    Eigen::Vector3d t_g_r1 = T_w_g.inverse() * t_w_r1;
    std::cout << "t_w_r0: " << t_w_r0.transpose()
              << ", t_w_r1: " << t_w_r1.transpose()
              << "\nt_g_r0: " << t_g_r0.transpose()
              << ", t_g_r1: " << t_g_r1.transpose() << std::endl;

    // Check
    Eigen::Vector2d t_w_g_ = t_w_g.head<2>();
    runTest(t_w_r0, t_w_r1, t_g_r0, t_g_r1, t_w_g_, rpy_w_g(2));

    std::cout << std::endl;
  }
}

int main(int argc, char** argv) {
  srand(time(NULL));
  std::cout.precision(12);

  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}