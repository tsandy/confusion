//
// Created by tim on 05.09.19.
//

#include <gtest/gtest.h>
#include <confusion/utilities/distances.h>
#include <confusion/utilities/rotation_utils.h>

TEST(quaternionBoxPlusTest, ShouldPass) {
  for (int i=0; i<10; ++i) {
    Eigen::Matrix<double,3,1> dRot;
    dRot.setRandom();
    dRot *= 1e-5;

    Eigen::Quaterniond q0;
    q0.coeffs().setRandom();
    q0.normalize();

    Eigen::Quaterniond q1 = confusion::quaternionBoxPlus(q0, dRot);

    //Perturb dRot and compute cost
    Eigen::Matrix<double,3,1> pert;
    pert.setRandom();
    pert *= 1e-8;
    Eigen::Matrix<double,3,1> dRot_pert = dRot + pert;
    Eigen::Quaterniond q1_pert = confusion::quaternionBoxPlus(q0, dRot_pert);

    // Get derivative and compute estimated new cost
    Eigen::Matrix<double, 4, 3> dq_ddRot = confusion::quaternionBoxPlusJacob(q0);
    Eigen::Matrix<double, 4, 1> q1_est = q1.coeffs() + dq_ddRot * pert;

    // Check
    /*std::cout << "q0: " << q0.coeffs().transpose() << std::endl;
    std::cout << "q1: " << q1.coeffs().transpose() << std::endl;
    std::cout << "q1_pert: " << q1_pert.coeffs().transpose() << std::endl;
    std::cout << "q1_est: " << q1_est.transpose() << std::endl;
    std::cout << "change: " << (q1.coeffs() - q1_pert.coeffs()).transpose() << std::endl;
    std::cout << "error: " << (q1_est - q1_pert.coeffs()).transpose() << std::endl;*/
    for (int j=0; j<4; ++j) {
      EXPECT_LT(fabs(q1_pert.coeffs()(j) - q1_est(j)), 1e-12);
    }
  }
}

int main(int argc, char **argv) {
  srand(time(NULL));
  std::cout.precision(12);

  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}