/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */

#include "include_test/local_parameterization_test_fixture.h"

namespace confusion {

void LocalParameterizationTest::Initialize(
    const std::shared_ptr<LocalParameterizationBase>& local_param,
    double dx_max_step, bool normalize_params) {
  local_param_ = local_param;
  dx_max_step_ = dx_max_step;
  normalize_params_ = normalize_params;
}

void LocalParameterizationTest::TestLocalParameterizationBoxPlusJacobian() {
  for (int i = 0; i < num_tests; ++i) {
    Eigen::VectorXd x_in(local_param_->GlobalSize());
    x_in.setRandom();
    if (normalize_params_) {
      x_in.normalize();
    }

    Eigen::VectorXd dx(local_param_->LocalSize());
    dx.setRandom();
    dx *= dx_max_step_;

    Eigen::VectorXd x_out(local_param_->GlobalSize());
    EXPECT_TRUE(local_param_->Plus(x_in.data(), dx.data(), x_out.data()));

    // Perturb the local displacement and check the jacobian
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
        dxout_ddx(local_param_->GlobalSize(), local_param_->LocalSize());
    EXPECT_TRUE(local_param_->ComputeJacobian(x_in.data(), dxout_ddx.data()));

    Eigen::VectorXd ddx(local_param_->LocalSize());
    ddx.setRandom();
    ddx *= dx_max_step_;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
        dx_pert = dx + ddx;

    Eigen::VectorXd x_out_pert(local_param_->GlobalSize());
    EXPECT_TRUE(
        local_param_->Plus(x_in.data(), dx_pert.data(), x_out_pert.data()));
    Eigen::VectorXd dx_out = x_out_pert - x_out;

    Eigen::VectorXd dxout_test = dxout_ddx * ddx;
    for (int i = 0; i < local_param_->GlobalSize(); ++i) {
      EXPECT_LT(dx_out[i] - dxout_test[i], tolerance)
          << PrintForBoxPlusJacobianTest(x_in, dx, x_out, ddx, x_out_pert,
                                         dx_out, dxout_test);
    }
  }
}

void LocalParameterizationTest::
    TestLocalParameterizationBoxMinusJacobianLeft() {
  TestLocalParameterizationBoxMinusJacobian(true);
}

void LocalParameterizationTest::
    TestLocalParameterizationBoxMinusJacobianRight() {
  TestLocalParameterizationBoxMinusJacobian(false);
}

void LocalParameterizationTest::TestLocalParameterizationBoxMinusJacobian(
    bool test_left_param_jacobian) {
  for (int i = 0; i < num_tests; ++i) {
    Eigen::VectorXd x0(local_param_->GlobalSize());
    x0.setRandom();
    Eigen::VectorXd x1(local_param_->GlobalSize());
    x1.setRandom();
    if (normalize_params_) {
      x0.normalize();
      x1.normalize();
    }

    Eigen::VectorXd dx(local_param_->LocalSize());
    local_param_->boxMinus(x0.data(), x1.data(), dx.data());

    // Perturb x0 in local space
    Eigen::VectorXd random_parameter_step(local_param_->GlobalSize());
    random_parameter_step.setRandom();
    random_parameter_step *= dx_max_step_;

    constexpr double kDefaultNonzeroMatrixValue = 10.0;
    Eigen::VectorXd x_pert(local_param_->GlobalSize());
    x_pert.setConstant(kDefaultNonzeroMatrixValue);
    Eigen::VectorXd dx_pert(local_param_->LocalSize());
    dx_pert.setConstant(kDefaultNonzeroMatrixValue);
    Eigen::MatrixXd ddx_dx(local_param_->LocalSize(),
                           local_param_->GlobalSize());
    ddx_dx.setConstant(kDefaultNonzeroMatrixValue);
    if (test_left_param_jacobian) {
      x_pert = x0 + random_parameter_step;
      local_param_->boxMinus(x_pert.data(), x1.data(), dx_pert.data());
      ddx_dx = local_param_->boxMinusJacobianLeft(x0.data(), x1.data());
    } else {
      x_pert = x1 + random_parameter_step;
      local_param_->boxMinus(x0.data(), x_pert.data(), dx_pert.data());
      ddx_dx = local_param_->boxMinusJacobianRight(x0.data(), x1.data());
    }

    Eigen::VectorXd ddx = dx_pert - dx;
    Eigen::VectorXd ddx_est = ddx_dx * random_parameter_step;

    for (int i = 0; i < local_param_->LocalSize(); ++i) {
      EXPECT_LT(ddx[i] - ddx_est[i], tolerance)
          << PrintForBoxMinusJacobianTest(x0, x1, random_parameter_step,
                                          test_left_param_jacobian, x_pert, dx,
                                          dx_pert, ddx, ddx_est);
    }
  }
}

std::string LocalParameterizationTest::PrintForBoxPlusJacobianTest(
    Eigen::VectorXd x_in, Eigen::VectorXd dx, Eigen::VectorXd x_out,
    Eigen::VectorXd ddx, Eigen::VectorXd x_out_pert, Eigen::VectorXd dx_out,
    Eigen::VectorXd dxout_test) {
  std::stringstream ss_out;
  ss_out << "x_in: " << x_in.transpose() << ", dx=" << dx.transpose()
         << ", \nx_out=" << x_out.transpose() << ", ddx=" << ddx
         << ", x_out_pert=" << x_out_pert.transpose()
         << ", \ndxout=" << dx_out.transpose()
         << ", \ndxout_test=" << dxout_test.transpose();

  return ss_out.str();
}

std::string LocalParameterizationTest::PrintForBoxMinusJacobianTest(
    Eigen::VectorXd x0, Eigen::VectorXd x1,
    Eigen::VectorXd random_parameter_step, bool test_left_param_jacobian,
    Eigen::VectorXd x_pert, Eigen::VectorXd dx, Eigen::VectorXd dx_pert,
    Eigen::VectorXd ddx, Eigen::VectorXd ddx_est) {
  std::stringstream ss_out;
  ss_out.precision(12);
  ss_out << "x0: " << x0.transpose() << "\nx1: " << x1.transpose()
         << "\nrandom_parameter_step: " << random_parameter_step.transpose()
         << "; test_left=" << test_left_param_jacobian
         << "\nx_pert: " << x_pert.transpose() << "\ndx=" << dx.transpose()
         << "; dx_pert=" << dx_pert.transpose() << "; dx_est=" << dx + ddx_est
         << "\nddx=" << ddx.transpose() << "; ddx_est=" << ddx_est.transpose();

  return ss_out.str();
}

void LocalParameterizationTest::TestLocalParameterizationPlusThenMinus() {
  for (int i = 0; i < num_tests; ++i) {
    Eigen::VectorXd x_in(local_param_->GlobalSize());
    x_in.setRandom();
    if (normalize_params_) {
      x_in.normalize();
    }

    Eigen::VectorXd dx(local_param_->LocalSize());
    dx.setRandom();
    dx *= dx_max_step_;

    Eigen::VectorXd x_out(local_param_->GlobalSize());
    EXPECT_TRUE(local_param_->Plus(x_in.data(), dx.data(), x_out.data()));

    Eigen::VectorXd dx_calc(local_param_->LocalSize());
    local_param_->boxMinus(x_in.data(), x_out.data(), dx_calc.data());

    for (int i = 0; i < local_param_->LocalSize(); ++i) {
      EXPECT_LT(dx_calc[i] - dx[i], tolerance)
          << PrintForPlusThenMinusTest(x_in, x_out, dx,
                                          dx_calc);
    }
  }
}

std::string LocalParameterizationTest::PrintForPlusThenMinusTest(
    Eigen::VectorXd x_in, Eigen::VectorXd x_out,
    Eigen::VectorXd dx, Eigen::VectorXd dx_calc) {
  std::stringstream ss_out;
  ss_out.precision(12);
  ss_out << "x_in: " << x_in.transpose() << "\nx_out: " << x_out.transpose()
         << "\ndx: " << dx.transpose()
         << "\ndx_calc: " << dx_calc.transpose();

  return ss_out.str();
}

}  // namespace confusion