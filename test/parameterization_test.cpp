/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <gtest/gtest.h>

#include <Eigen/Core>

#include "confusion/LocalParameterization.h"
#include "include_test/local_parameterization_test_fixture.h"

using namespace confusion;

const double tolerance = 5e-8;
const double kDxMaxMagnitude = 1e-5;

TEST_F(LocalParameterizationTest, quat_param_test) {
  std::shared_ptr<QuatParam> quat_param =
      std::make_shared<QuatParam>();

  Initialize(quat_param, kDxMaxMagnitude, true);

  TestLocalParameterizationBoxPlusJacobian();
  TestLocalParameterizationBoxMinusJacobianLeft();
  TestLocalParameterizationBoxMinusJacobianRight();
}

TEST_F(LocalParameterizationTest, fixed_yaw_param_test) {
  std::shared_ptr<FixedYawParameterization> fixed_yaw_param =
      std::make_shared<FixedYawParameterization>();

  Initialize(fixed_yaw_param, kDxMaxMagnitude, true);

  TestLocalParameterizationBoxPlusJacobian();
  TestLocalParameterizationBoxMinusJacobianLeft();
  TestLocalParameterizationBoxMinusJacobianRight();
  TestLocalParameterizationPlusThenMinus();
}

int main(int argc, char** argv) {
  srand(time(NULL));
  std::cout.precision(12);

  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}