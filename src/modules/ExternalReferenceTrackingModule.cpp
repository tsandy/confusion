/*
 * Copyright 2018 Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/modules/external_reference_tracking/ExternalReferenceTrackingModule.h"

namespace confusion {

ExternalReferenceTrackingModule::ExternalReferenceTrackingModule(
    ConFusor& conFusor, const boost::property_tree::ptree& ptree,
    int poseMeasIndex, bool* newMeasReceivedFlag)
    : conFusor_(conFusor), poseMeasIndex_(poseMeasIndex) {
  std::cout << "[ExternalReferenceTrackingModule] PoseMeas index is "
            << poseMeasIndex_ << std::endl;
  if (newMeasReceivedFlag) newMeasReceivedFlag_ = newMeasReceivedFlag;

  pt_ = ptree;
#ifdef OPT_GRAVITY_ROT
  //	Eigen::MatrixXd gravityRotInitialWeighting(2,2);
  //	gravityRotInitialWeighting.setIdentity();
  //	gravityRotInitialWeighting /=
  // tagTrackerParameters_.tagMeasCalibration_.ftr_init_stddev;
  conFusor_.addStaticParameter(confusion::Parameter(
      gravity_rot_.data(), 2, "g_r"));  //, gravityRotInitialWeighting);
#endif

  // Initialize external posemeas sources and (optionally) ROS subscribers.
  for (const std::pair<const std::string, boost::property_tree::ptree>& child :
       pt_.get_child("external_pose_meas")) {
    std::string sensor_name = child.first.data();
    if (!initializeSensorframeOffset(sensor_name)) continue;

    initializeSensorframeOffserDerived(child);  // For ROS subscribers
  }

  std::cout << "[ExternalReferenceTrackingModule] Initialization complete."
            << std::endl;
}

bool ExternalReferenceTrackingModule::initializeSensorframeOffset(
    std::string sensor_name) {
  // get the configuration subtree of this sensorframe
  boost::property_tree::ptree config =
      pt_.get_child("external_pose_meas." + sensor_name);
  std::string frame = sensor_name;

  // The user can specify T_body_sensor or T_sensor_body.
  auto T_body_sensor = std::make_shared<confusion::Pose<double>>();

  bool positionOnly = config.get<bool>("position_only", false);
  std::string settings_key;
  if (positionOnly) {
    // We only need the position extrinsic, but it will be stored as a full pose
    // to simplify the interface
    settings_key = "t_body_" + frame;
    boost::optional<double> tx_body_sensor =
        config.get_optional<double>(settings_key + ".px");
    if (tx_body_sensor) {
      std::cout << "Found " << settings_key << std::endl;
      T_body_sensor->trans(0) = config.get<double>(settings_key + ".px");
      T_body_sensor->trans(1) = config.get<double>(settings_key + ".py");
      T_body_sensor->trans(2) = config.get<double>(settings_key + ".pz");
    } else {
      std::string settings_key_inv = "t_" + frame + "_body";
      boost::optional<double> tx_sensor_body =
          config.get_optional<double>(settings_key_inv + ".px");
      if (tx_sensor_body) {
        std::cout << "Found " << settings_key_inv << std::endl;
        confusion::Pose<double> T_sensor_body;
        T_sensor_body.trans(0) = config.get<double>(settings_key_inv + ".px");
        T_sensor_body.trans(1) = config.get<double>(settings_key_inv + ".py");
        T_sensor_body.trans(2) = config.get<double>(settings_key_inv + ".pz");

        *T_body_sensor = T_sensor_body.inverse();
      } else {
        std::cout << "ERROR: Neither " << settings_key << " nor "
                  << settings_key_inv
                  << " are specified in the config file for sensor " << frame
                  << ". Will not use that sensor." << std::endl;
        return false;
      }
    }
  } else {
    settings_key = "T_body_" + frame;
    boost::optional<double> tx_body_sensor =
        config.get_optional<double>(settings_key + ".px");
    if (tx_body_sensor) {
      std::cout << "Found " << settings_key << std::endl;
      T_body_sensor->trans(0) = config.get<double>(settings_key + ".px");
      T_body_sensor->trans(1) = config.get<double>(settings_key + ".py");
      T_body_sensor->trans(2) = config.get<double>(settings_key + ".pz");
      T_body_sensor->rot.w() = config.get<double>(settings_key + ".qw");
      T_body_sensor->rot.x() = config.get<double>(settings_key + ".qx");
      T_body_sensor->rot.y() = config.get<double>(settings_key + ".qy");
      T_body_sensor->rot.z() = config.get<double>(settings_key + ".qz");
      T_body_sensor->rot.normalize();
    } else {
      std::string settings_key_inv = "T_" + frame + "_body";
      boost::optional<double> tx_sensor_body =
          config.get_optional<double>(settings_key_inv + ".px");
      if (tx_sensor_body) {
        std::cout << "Found " << settings_key_inv << std::endl;
        confusion::Pose<double> T_sensor_body;
        T_sensor_body.trans(0) = config.get<double>(settings_key_inv + ".px");
        T_sensor_body.trans(1) = config.get<double>(settings_key_inv + ".py");
        T_sensor_body.trans(2) = config.get<double>(settings_key_inv + ".pz");
        T_sensor_body.rot.w() = config.get<double>(settings_key_inv + ".qw");
        T_sensor_body.rot.x() = config.get<double>(settings_key_inv + ".qx");
        T_sensor_body.rot.y() = config.get<double>(settings_key_inv + ".qy");
        T_sensor_body.rot.z() = config.get<double>(settings_key_inv + ".qz");
        T_sensor_body.rot.normalize();

        *T_body_sensor = T_sensor_body.inverse();
      } else {
        std::cout << "ERROR: Neither " << settings_key << " nor "
                  << settings_key_inv
                  << " are specified in the config file for sensor " << frame
                  << std::endl;
        return false;
      }
    }
  }
  T_body_sensor->print(settings_key);
  sensorFrameOffsets_[frame] = T_body_sensor;
  sensorFrameOffsetsCopy_[frame] =
      *T_body_sensor;  // These are set here already so the offset can be
                       // queried for e.g. setting the first state pose for
                       // tracking
  std::cout << "Sensor frame offset for " << sensor_name << " at addresses "
            << T_body_sensor->trans.data() << " and "
            << T_body_sensor->rot.coeffs().data() << std::endl;

  // scale defaults to 1
  auto scale = std::make_shared<double>(config.get<double>("scale", 1.0));
  sensorPoseMeasScales_[frame] = scale;

  auto pose_meas_config = std::make_shared<confusion::ErtMeasConfig>();
  pose_meas_config->sensorFrameName_ = sensor_name;
  pose_meas_config->w_trans =
      1.0 / config.get<double>("pose_meas_trans_stddev");
  pose_meas_config->sensorOffset_trans_init_stddev =
      config.get<double>("t_init_stddev");
  pose_meas_config->useLossFunction = config.get<bool>("use_loss_function");
  pose_meas_config->lossCoefficient = config.get<double>("loss_coefficient");
  pose_meas_config->optTranslationalExtrinsic =
      config.get<bool>("optimize_t_body");
  pose_meas_config->sensorFrameNameForTf_ =
      config.get<std::string>("sensor_frame_name_for_tf", frame);

  if (!positionOnly) {
    pose_meas_config->w_rot = 1.0 / config.get<double>("pose_meas_rot_stddev");
    pose_meas_config->sensorOffset_rot_init_stddev =
        config.get<double>("q_init_stddev");
    pose_meas_config->optRotationalExtrinsic =
        config.get<bool>("optimize_q_body");
    pose_meas_config->optScale = config.get<bool>("optimize_scale", false);
  }

  auto maxRate = config.get<int>("max_rate", -1);
  if (maxRate > 0) {
    pose_meas_config->maxRateSpecified = true;
    pose_meas_config->minMeasInterval = 1.0 / (double)maxRate;
  }

  pose_meas_config->useRefeferenceFrameOffsetRwp =
      config.get<bool>("use_reference_frame_offset_rwp", false);
  if (pose_meas_config->useRefeferenceFrameOffsetRwp) {
    pose_meas_config->refOffsetTransRwpProcessNoise =
        config.get<double>("ref_offset_trans_rwp_process_noise");
    pose_meas_config->refOffsetRotRwpProcessNoise =
        config.get<double>("ref_offset_rot_rwp_process_noise");
    std::cout << "Using a random walk process for the " << sensor_name
              << " reference frame offset" << std::endl;
  }
  pose_meas_config->positionOnly_ = positionOnly;

  boost::optional<double> refOffsetTransInitStddev =
      config.get_optional<double>("ref_offset_trans_init_stddev");
  if (refOffsetTransInitStddev) {
    pose_meas_config->useRefOffsetInitStddev_ = true;
    pose_meas_config->refOffsetTransInitStddev_ =
        config.get<double>("ref_offset_trans_init_stddev");
    pose_meas_config->refOffsetRotInitStddev_ =
        config.get<double>("ref_offset_rot_init_stddev");
    std::cout << "Will set an initial confidence on the " << sensor_name
              << " reference frame offset" << std::endl;
  }

  sensorPoseMeasConfigs_[frame] = pose_meas_config;

  // Optimization of translation
  if (pose_meas_config->optTranslationalExtrinsic) {
    Eigen::MatrixXd t_body_sensor_initial_weighting(3, 3);
    t_body_sensor_initial_weighting.setIdentity();
    t_body_sensor_initial_weighting /=
        pose_meas_config->sensorOffset_trans_init_stddev;
    confusion::Parameter tParam(T_body_sensor->trans.data(), 3,
                                "t_body_" + frame);
    tParam.setInitialConstraintWeighting(t_body_sensor_initial_weighting);
    conFusor_.addStaticParameter(tParam);
  } else {
    conFusor_.addStaticParameter(confusion::Parameter(
        T_body_sensor->trans.data(), 3, "t_body_" + frame, true));
  }

  // Optimization of rotation
  if (!positionOnly && pose_meas_config->optRotationalExtrinsic) {
    Eigen::MatrixXd q_body_sensor_initial_weighting(3, 3);
    q_body_sensor_initial_weighting.setIdentity();
    q_body_sensor_initial_weighting /=
        pose_meas_config->sensorOffset_rot_init_stddev;

    confusion::Parameter tParam(T_body_sensor->rot.coeffs().data(), 4,
                                "q_body_" + frame, false,
                                std::make_shared<confusion::QuatParam>());
    tParam.setInitialConstraintWeighting(q_body_sensor_initial_weighting);
    conFusor_.addStaticParameter(tParam);
  } else {
    conFusor_.addStaticParameter(confusion::Parameter(
        T_body_sensor->rot.coeffs().data(), 4, "q_body_" + frame, true,
        std::make_shared<confusion::QuatParam>()));
  }

  // Optimization of scale
  // todo This is not yet supported for optimization in PoseMeas!!
  if (!positionOnly) {
    if (pose_meas_config->optScale) {
      conFusor_.addStaticParameter(
          confusion::Parameter(scale.get(), 1, "scale_" + sensor_name, false));
    } else {
      conFusor_.addStaticParameter(
          confusion::Parameter(scale.get(), 1, "scale_" + sensor_name, true));
    }
  }

  return true;
}

void ExternalReferenceTrackingModule::setSensorFrameOffsetInitialGuess(
    const std::string& sensorFrameName,
    const confusion::Pose<double>& T_body_sensor) {
  // todo Make sure that the sensor isn't already being used for tracking?
  std::shared_ptr<confusion::Pose<double>> sensorOffsetPtr;
  try {
    sensorOffsetPtr = sensorFrameOffsets_.at(sensorFrameName);
  } catch (...) {
    std::cerr
        << "ERROR: "
           "ExternalReferenceTrackingModule::setSensorFrameOffsetInitialGuess "
           "called for unknown sensor frame"
        << sensorFrameName << std::endl;
    return;
  }

  *sensorOffsetPtr = T_body_sensor;
}

std::vector<std::string>
ExternalReferenceTrackingModule::getSensorFrameNames() {
  std::vector<std::string> sensorFrameNames;
  for (auto sf : sensorFrameOffsets_) {
    sensorFrameNames.push_back(sf.first);
  }

  return sensorFrameNames;
}

bool ExternalReferenceTrackingModule::addMeasurement(
    const double& t, const std::string& referenceFrameName,
    const std::string& sensorFrameName,
    const confusion::Pose<double>& T_wa_ba) {
  // Get the sensor configuration
  std::shared_ptr<confusion::ErtMeasConfig> poseMeasConfigPtr;
  try {
    poseMeasConfigPtr = sensorPoseMeasConfigs_.at(sensorFrameName);
  } catch (...) {
    std::cerr << "ERROR: PoseMeas received message with unknown childframe '"
              << sensorFrameName << "'. Throwing out the measurement."
              << std::endl;
    return false;
  }

  if (poseMeasConfigPtr->maxRateSpecified) {
    // Drop the measurement if it is too close to the last accepted measurement
    if (t - poseMeasConfigPtr->tLastMeas < poseMeasConfigPtr->minMeasInterval)
      return false;

    poseMeasConfigPtr->tLastMeas = t;
  }

  std::shared_ptr<confusion::Pose<double>> sensorOffsetPtr;
  std::shared_ptr<double> scalePtr;
  try {
    sensorOffsetPtr = sensorFrameOffsets_.at(sensorFrameName);
    poseMeasConfigPtr = sensorPoseMeasConfigs_.at(sensorFrameName);
    scalePtr = sensorPoseMeasScales_.at(sensorFrameName);
  } catch (...) {
    std::cerr
        << "ERROR: PoseMeas received message with unknown child_frame_id '"
        << sensorFrameName << "'. Throwing out the measurement." << std::endl;
    return false;
  }

  if (poseMeasConfigPtr->positionOnly_) {
    auto positionMeasPtr = std::make_shared<confusion::PositionMeas>(
        t, referenceFrameName, sensorFrameName, sensorOffsetPtr->trans,
        T_wa_ba.trans, *poseMeasConfigPtr,
        poseMeasIndex_);  // scalePtr.get(), poseMeasIndex_);
    conFusor_.addUpdateMeasurement(positionMeasPtr);
    if (newMeasReceivedFlag_) *newMeasReceivedFlag_ = true;
  } else {
    auto poseMeasPtr = std::make_shared<confusion::PoseMeas>(
        t, referenceFrameName, sensorFrameName, *sensorOffsetPtr, T_wa_ba,
        *poseMeasConfigPtr,
        poseMeasIndex_);  // scalePtr.get(), poseMeasIndex_);
    conFusor_.addUpdateMeasurement(poseMeasPtr);
    if (newMeasReceivedFlag_) *newMeasReceivedFlag_ = true;
  }

  if (verbose_) {
    std::cout << "PoesMeas received with stamp " << t << " from frame "
              << referenceFrameName << " to frame " << sensorFrameName
              << std::endl;
    T_wa_ba.print("T_wa_ba");
  }

  return true;
}

void ExternalReferenceTrackingModule::postprocessingAfterOptimization(
    const confusion::StateVector* stateVector) {
  std::deque<std::string> sensorsToReset;
  {
    std::lock_guard<std::mutex> lg(sensorResetMutex_);
    sensorsToReset.swap(sensorsToReset_);
    sensorsToReset_.clear();
  }

  while (!sensorsToReset.empty()) {
    resetSensorInternal(sensorsToReset.front());
    sensorsToReset.pop_front();
  }

  std::lock_guard<std::mutex> lg(parameterQueryMtx_);
  for (auto& externalReferenceFrame : referenceFrameOffsets_)
    referenceFrameOffsetsCopy_[externalReferenceFrame.first] =
        *externalReferenceFrame.second;
  for (auto& sensorFrame : sensorFrameOffsets_)
    sensorFrameOffsetsCopy_[sensorFrame.first] = *sensorFrame.second;
  for (auto& scaleParam : sensorPoseMeasScales_)
    sensorPoseMeasScalesCopy_[scaleParam.first] = *scaleParam.second;
}

bool ExternalReferenceTrackingModule::getReferenceFrameOffset(
    const std::string& frameName, confusion::Pose<double>& T_world_ref) const {
  std::lock_guard<std::mutex> lg(parameterQueryMtx_);
  try {
    T_world_ref = referenceFrameOffsetsCopy_.at(frameName);
  } catch (...) {
    return false;
  }

  return true;
}

bool ExternalReferenceTrackingModule::getSensorFrameOffset(
    const std::string& frameName,
    confusion::Pose<double>& T_body_sensor) const {
  std::lock_guard<std::mutex> lg(parameterQueryMtx_);
  try {
    T_body_sensor = sensorFrameOffsetsCopy_.at(frameName);
  } catch (...) {
    return false;
  }

  return true;
}

bool ExternalReferenceTrackingModule::getSensorScale(
    const std::string& frameName, double& scale) const {
  std::lock_guard<std::mutex> lg(parameterQueryMtx_);
  try {
    scale = sensorPoseMeasScalesCopy_.at(frameName);
  } catch (...) {
    return false;
  }

  return true;
}

void ExternalReferenceTrackingModule::initializeFirstRefFrameOffset(
    const std::shared_ptr<ErtMeasBase>& ertMeasPtr,
    const confusion::Pose<double>& T_w_ref, GravityAlignmentStrategy grav) {
  std::shared_ptr<confusion::Pose<double>> T_w_ref_ptr =
      std::make_shared<confusion::Pose<double>>(T_w_ref);
  activateSensor(ertMeasPtr->sensorFrameName(),
                 ertMeasPtr->referenceFrameName(), T_w_ref_ptr, true);
}

bool ExternalReferenceTrackingModule::processPoseMeasBeforeAssigningToState(
    const std::shared_ptr<ErtMeasBase>& ertMeasPtr, const double& t,
    const Pose<double>& T_w_body) {
  std::shared_ptr<confusion::Pose<double>> T_w_ref;
  try {
    T_w_ref = referenceFrameOffsets_.at(ertMeasPtr->referenceFrameName());
  } catch (...) {
    // Try to initialize the reference frame offset for a new sensor
    T_w_ref = ertMeasPtr->initializeReferenceFrameOffset(t, T_w_body);
    if (!T_w_ref) return false;

    if (ertFrameDefinesStaticWorld_ &&
        !ertMeasPtr->config().useRefeferenceFrameOffsetRwp) {
      std::cout << "Swapping the static reference frame from that of sensor "
                << sensorThatDefinesStaticWorld_
                << " to the new one without a RWP attached." << std::endl;

      // We want to switch the static reference frame so that the RWP frame is
      // free to move 1 - Reset the sensor which previously defined the static
      // world
      std::cout << "Step 1" << std::endl;
      std::shared_ptr<confusion::ErtMeasConfig> oldRefPoseMeasConfig =
          sensorPoseMeasConfigs_.at(sensorThatDefinesStaticWorld_);
      std::shared_ptr<confusion::Pose<double>> T_w_ref_old =
          referenceFrameOffsets_.at(oldRefPoseMeasConfig->referenceFrameName_);
      disableSensor(sensorThatDefinesStaticWorld_);

      // 2 - Add the new reference frame as fixed
      std::cout << "Step 2 adding params for ref frame "
                << ertMeasPtr->referenceFrameName() << " fixed to static world"
                << std::endl;
      activateSensor(ertMeasPtr->sensorFrameName(),
                     ertMeasPtr->referenceFrameName(), T_w_ref, true);

      // 3 - Re-add the old reference frame with the RWP attached
      std::cout << "Step 3" << std::endl;
      activateSensor(oldRefPoseMeasConfig->sensorFrameName_,
                     oldRefPoseMeasConfig->referenceFrameName_, T_w_ref_old);

      std::cout << "Swapped the static reference frame to that of sensor "
                << ertMeasPtr->sensorFrameName() << ". Ready to continue."
                << std::endl;
    } else {
      activateSensor(ertMeasPtr->sensorFrameName(),
                     ertMeasPtr->referenceFrameName(), T_w_ref);
    }

    T_w_ref->print("T_w_ref init");
  }

  ertMeasPtr->assignExternalReferenceFrame(T_w_ref);

  processPoseMeasBeforeAssigningToStateDerived(ertMeasPtr, t, T_w_body);

  return true;
}

void ExternalReferenceTrackingModule::resetSensor(
    const std::string& sensorName) {
  // Make sure the sensor isn't already being reset
  for (auto& nn : sensorsToReset_) {
    if (nn.compare(sensorName) == 0) {
      std::cout << "[ExternalReferenceTrackingModule] Received duplicate "
                   "requests to reset sensor "
                << sensorName << ". Dropping the new request." << std::endl;
      return;
    }
  }
  sensorsToReset_.push_back(sensorName);
}

void ExternalReferenceTrackingModule::resetSensorNow(
    const std::string& sensorName) {
  resetSensorInternal(sensorName);
}

void ExternalReferenceTrackingModule::resetSensorInternal(
    const std::string& sensorName) {
  std::cout << "[ExternalReferenceTrackingModule] Resetting sensor "
            << sensorName << std::endl;

  // Check if the linked reference frame is currently setting the static world
  // reference If it is, we will need to move the static reference to another
  // sensor reference frame
  bool setNewSensorThatDefinesStaticWorld = false;
  std::string newSensorThatDefinesStaticWorld;
  if (sensorName == sensorThatDefinesStaticWorld_) {
    // We need to switch the static frame. Look for another reference frame
    // without a RWP attached.
    for (auto& sensorConfig : sensorPoseMeasConfigs_) {
      if (sensorConfig.first.compare(sensorName) != 0 &&
          sensorConfig.second->active_ &&
          !sensorConfig.second->useRefeferenceFrameOffsetRwp) {
        // Set this sensor for the next static reference
        newSensorThatDefinesStaticWorld = sensorConfig.first;
        setNewSensorThatDefinesStaticWorld = true;
        break;
      }
    }

    if (!setNewSensorThatDefinesStaticWorld) {
      // Use the first active sensor found, even if it has a RWP attached
      for (auto& sensorConfig : sensorPoseMeasConfigs_) {
        if (sensorConfig.first.compare(sensorName) != 0 &&
            sensorConfig.second->active_) {
          // Set this sensor for the next static reference
          newSensorThatDefinesStaticWorld = sensorConfig.first;
          setNewSensorThatDefinesStaticWorld = true;
          break;
        }
      }
    }

    if (!setNewSensorThatDefinesStaticWorld) {
      std::cout
          << "[ExternalReferenceTrackingModule] ERROR: User requeted to reset "
             "the only active sensor. Doing this is not yet supported. Would "
             "be "
             "better just to stop and restart the ConFusor."
          << std::endl;
      return;
    }
  }

  disableSensor(sensorName);

  std::cout << "[ExternalReferenceTrackingModule] Sensor " << sensorName
            << " reset" << std::endl;

  // Set the new static world reference if needed
  if (setNewSensorThatDefinesStaticWorld) {
    std::cout << "[ExternalReferenceTrackingModule] Using the reference frame "
                 "of sensor "
              << newSensorThatDefinesStaticWorld
              << " as the static world reference." << std::endl;

    // Remove the reference frame for the targeted sensor
    std::shared_ptr<confusion::ErtMeasConfig> poseMeasConfigPtr =
        sensorPoseMeasConfigs_.at(newSensorThatDefinesStaticWorld);
    std::shared_ptr<confusion::Pose<double>> T_w_ref_old =
        referenceFrameOffsets_.at(poseMeasConfigPtr->referenceFrameName_);
    std::cout << "New static ref frame param addresses: "
              << T_w_ref_old->trans.data() << ", "
              << T_w_ref_old->rot.coeffs().data() << std::endl;
    disableSensor(newSensorThatDefinesStaticWorld);

    // Add the reference again with the world-fixed parameterization
    activateSensor(newSensorThatDefinesStaticWorld,
                   poseMeasConfigPtr->referenceFrameName_, T_w_ref_old, true);
  }
}

void ExternalReferenceTrackingModule::disableSensor(
    const std::string& sensorName) {
  std::shared_ptr<confusion::ErtMeasConfig> poseMeasConfigPtr;
  try {
    poseMeasConfigPtr = sensorPoseMeasConfigs_.at(sensorName);
  } catch (...) {
    std::cout << "[ExternalReferenceTrackingModule] WARNING: User requested to "
                 "reset sensor "
              << sensorName << " with no linked config. Dropping the request."
              << std::endl;
    return;
  }

  std::shared_ptr<confusion::Pose<double>> T_w_ref;
  try {
    T_w_ref = referenceFrameOffsets_.at(poseMeasConfigPtr->referenceFrameName_);
  } catch (...) {
    std::cout << "[ExternalReferenceTrackingModule] WARNING: User requested to "
                 "reset sensor "
              << sensorName
              << " with no reference frame offset. Dropping the request."
              << std::endl;
    return;
  }

  if (verbose_) {
    std::cout << "[ExternalReferenceTrackingModule] Disabling sensor "
              << sensorName << std::endl;
  }

  // Remove parameters from the sensor fusion problem
  // The sensor frame stays in the problem so it can be used if the sensor is
  // restarted
  std::vector<double*> staticParamsToRemove;
  staticParamsToRemove.push_back(T_w_ref->trans.data());
  staticParamsToRemove.push_back(T_w_ref->rot.coeffs().data());
  conFusor_.removeStaticParameters(staticParamsToRemove);

  // Remove the referenceFrameOffset. That will tell the system to reinitialize
  // the sensor when sufficient new measurements have been received.
  size_t res =
      referenceFrameOffsets_.erase(poseMeasConfigPtr->referenceFrameName_);
  poseMeasConfigPtr->active_ = false;
  if (res != 1) {
    std::cout << "[ExternalReferenceTrackingModule] WARNING: Removing the "
                 "requested reference frame actually removed "
              << res << " elements!?" << std::endl;
  }

  // Disable all active measurements from that sensor
  for (auto& state : conFusor_.stateVector_) {
    for (auto& updateMeas : state->updateMeasurements_[poseMeasIndex_]) {
      std::shared_ptr<confusion::ErtMeasBase> meas =
          std::dynamic_pointer_cast<confusion::ErtMeasBase>(updateMeas);
      if (!meas) {
        std::cout << "WARNING: Unexpected measurement type when parsing state "
                     "vector??"
                  << std::endl;
        continue;
      }

      if (meas->sensorFrameName().compare(sensorName) == 0) {
        meas->disable();
      }
    }
  }
}

void ExternalReferenceTrackingModule::activateSensor(
    const std::string& sensorName, const std::string& referenceFrameName,
    const std::shared_ptr<confusion::Pose<double>>& T_w_ref_init,
    bool useWorldFixedParameterization) {
  std::shared_ptr<confusion::ErtMeasConfig> poseMeasConfigPtr;
  try {
    poseMeasConfigPtr = sensorPoseMeasConfigs_.at(sensorName);
  } catch (...) {
    std::cout << "[ExternalReferenceTrackingModule] WARNING: User requested to "
                 "reset sensor "
              << sensorName << " with no linked config. Dropping the request."
              << std::endl;
    return;
  }
  poseMeasConfigPtr->referenceFrameName_ = referenceFrameName;
  poseMeasConfigPtr->active_ = true;
  referenceFrameOffsets_[poseMeasConfigPtr->referenceFrameName_] = T_w_ref_init;

  if (verbose_) {
    std::cout << "[ExternalReferenceTrackingModule] Activating sensor "
              << sensorName << "; useWorldFixedParameterization="
              << useWorldFixedParameterization << std::endl;
  }

  // Add the reference frame parameters to the sensor fusion problem
  if (useWorldFixedParameterization) {
    conFusor_.staticParameters_.addParameter(confusion::Parameter(
        T_w_ref_init->trans.data(), 3,
        "t_world_" + poseMeasConfigPtr->referenceFrameName_, true));
    if (gravityAlignmentStrategy_ == ERT_OPT_GRAVITY) {
      // Ref frame orientation is constant
      conFusor_.staticParameters_.addParameter(confusion::Parameter(
          T_w_ref_init->rot.coeffs().data(), 4,
          "q_world_" + poseMeasConfigPtr->referenceFrameName_, true,
          std::make_shared<confusion::QuatParam>()));
    } else {
      // Ref frame orientation is optimized to align gravity
      confusion::Parameter q_w_ref_param = confusion::Parameter(
          T_w_ref_init->rot.coeffs().data(), 4,
          "q_world_" + poseMeasConfigPtr->referenceFrameName_,
          std::make_shared<confusion::FixedYawParameterization>());

      if (poseMeasConfigPtr->useRefOffsetInitStddev_) {
        Eigen::MatrixXd q_init_cov(2, 2);
        q_init_cov.setIdentity();
        q_init_cov /= poseMeasConfigPtr->refOffsetRotInitStddev_;
        q_w_ref_param.setInitialConstraintWeighting(q_init_cov);
      }

      conFusor_.staticParameters_.addParameter(q_w_ref_param);
    }

    // Set the reference frame and status flags
    sensorThatDefinesStaticWorld_ = sensorName;
    ertFrameDefinesStaticWorld_ =
        poseMeasConfigPtr->useRefeferenceFrameOffsetRwp;

    if (ertFrameDefinesStaticWorld_)
      std::cout
          << "ERT Module is using a reference frame with a RWP attached as the "
             "static world reference. Will try to switch to another static "
             "fram as more sensors come online."
          << std::endl;
  } else {
    confusion::Parameter t_offset_param(
        T_w_ref_init->trans.data(), 3,
        "t_world_" + poseMeasConfigPtr->referenceFrameName_);
    confusion::Parameter q_offset_param(
        T_w_ref_init->rot.coeffs().data(), 4,
        "q_world_" + poseMeasConfigPtr->referenceFrameName_,
        std::make_shared<confusion::QuatParam>());

    // Optionally use an initial constraint. This is needed for initializing the
    // GPS reference frame since it isn't fully constrained by a single
    // measurement.
    if (poseMeasConfigPtr->useRefOffsetInitStddev_) {
      Eigen::MatrixXd t_init_cov(3, 3);
      t_init_cov.setIdentity();
      t_init_cov /= poseMeasConfigPtr->refOffsetTransInitStddev_;
      t_offset_param.setInitialConstraintWeighting(t_init_cov);

      Eigen::MatrixXd q_init_cov(3, 3);
      q_init_cov.setIdentity();
      q_init_cov /= poseMeasConfigPtr->refOffsetRotInitStddev_;
      q_offset_param.setInitialConstraintWeighting(q_init_cov);
    }

    conFusor_.staticParameters_.addParameter(t_offset_param);
    conFusor_.staticParameters_.addParameter(q_offset_param);
    std::cout << "Added external reference frame with name "
              << poseMeasConfigPtr->referenceFrameName_
              << " to the ExternalReferenceTrackingModule" << std::endl;

    if (poseMeasConfigPtr->useRefeferenceFrameOffsetRwp) {
      conFusor_.attachStaticParameterRandomWalkProcess(
          T_w_ref_init->trans.data(),
          poseMeasConfigPtr->refOffsetTransRwpProcessNoise);
      conFusor_.attachStaticParameterRandomWalkProcess(
          T_w_ref_init->rot.coeffs().data(),
          poseMeasConfigPtr->refOffsetRotRwpProcessNoise);
    }
  }
}

void ExternalReferenceTrackingModule::setExtrinsicsUnconstForBatchSolve() {
  for (auto& sensorFrame : sensorFrameOffsets_) {
    sensorFrame.second->print(sensorFrame.first);
    conFusor_.staticParameters_.getParameter(sensorFrame.second->trans.data())
        ->unsetConstant();
    conFusor_.staticParameters_
        .getParameter(sensorFrame.second->rot.coeffs().data())
        ->unsetConstant();
  }
}
void ExternalReferenceTrackingModule::resetExtrinsicsConstAfterBatchSolve() {
  for (auto& sensorFrame : sensorFrameOffsets_) {
    std::shared_ptr<confusion::ErtMeasConfig> poseMeasConfigPtr =
        sensorPoseMeasConfigs_.at(sensorFrame.first);
    if (!poseMeasConfigPtr->optTranslationalExtrinsic)
      conFusor_.staticParameters_
          .getParameter(sensorFrame.second->trans.data())
          ->setConstant();
    if (!poseMeasConfigPtr->optRotationalExtrinsic)
      conFusor_.staticParameters_
          .getParameter(sensorFrame.second->rot.coeffs().data())
          ->setConstant();
  }
}

void ExternalReferenceTrackingModule::printParameters() {
  std::cout << "External Reference Tracking parameters:\n";
  for (auto& sensorFrame : sensorFrameOffsets_) {
    sensorFrame.second->print("T_body_" + sensorFrame.first);
  }
  for (auto& refFrame : referenceFrameOffsets_) {
    refFrame.second->print("T_world_" + refFrame.first);
  }
}

}  // namespace confusion
