/*
 * Copyright 2018 Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/modules/apriltag/AprilTagModule.h"

namespace confusion {

AprilTagModule::AprilTagModule(ConFusor* conFusorPtr, std::string dataFilePath,
                               const std::string& configFileName,
                               int tagMeasIndex, bool* newMeasReceivedFlag)
    : tagDetector_("tag36h11", 2),
      conFusorPtr_(conFusorPtr),
      tagMeasIndex_(tagMeasIndex) {
  if (newMeasReceivedFlag) newMeasReceivedFlag_ = newMeasReceivedFlag;

  // Load config parameters from the desired file
  confusionPath_ = dataFilePath;
  boost::property_tree::ptree pt;
  boost::property_tree::read_info(configFileName, pt);

  aprilTagParameters_.tagMeasCalibration_.ftr_init_stddev_ =
      pt.get<double>("ftr_init_stddev");
  aprilTagParameters_.t_c_i_init_stddev = pt.get<double>("tci_init_stddev");
  aprilTagParameters_.q_c_i_init_stddev = pt.get<double>("qci_init_stddev");
  aprilTagParameters_.tagMeasCalibration_.tagSize_ = pt.get<double>("tagSize");
  aprilTagParameters_.tag_corner_stddev_ = pt.get<double>("tag_corner_stddev");
  aprilTagParameters_.tagMeasCalibration_.tagUseLossFunction_ =
      pt.get<bool>("use_loss_function", false);
  aprilTagParameters_.tagMeasCalibration_.tagLossCoefficient_ =
      pt.get<double>("loss_coefficient", 0.0);
  aprilTagParameters_.imuFrameName =
      pt.get<std::string>("imu_frame_name", "imu");
  aprilTagParameters_.cameraFrameName =
      pt.get<std::string>("camera_frame_name", "cam");
  aprilTagParameters_.stateParameterIndex_ =
      pt.get<int>("stateParameterIndex", 0);
  aprilTagParameters_.initialize();
  maxImageDt_ = 1.0 / pt.get<double>("maxImageRate");

  // Read the tag map from the text file if desired
  if (pt.get<bool>("getTagMapFromFile")) {
    confusion::Pose<double> T_b_ib;
    confusion::Pose<double> T_ie_e;
    Eigen::Matrix<double, 4, 1> b_q;
    std::string tagMapFilename =
        confusionPath_ + pt.get<std::string>("tagMapFilename");

    if (readTagPoses(tagMapFilename, referenceFrameOffsets_)) {
      // Add the tag poses to the static parameters
      Eigen::MatrixXd ftr_initialWeighting(2, 2);
      ftr_initialWeighting.setIdentity();
      ftr_initialWeighting /= pt.get<double>("ftr_prior_stddev");
      Eigen::MatrixXd t_w_t_initialWeighting(3, 3);
      t_w_t_initialWeighting.setIdentity();
      t_w_t_initialWeighting /= pt.get<double>("twt_prior_stddev");
      Eigen::MatrixXd q_w_t_initialWeighting(3, 3);
      q_w_t_initialWeighting.setIdentity();
      q_w_t_initialWeighting /= pt.get<double>("qwt_prior_stddev");

      // Initialize the first tag, which is either completely fixed or has two
      // rotational degrees of freedom
#ifdef OPT_GRAVITY  // *** Optionally defined in the ImuState header file
      conFusorPtr_->staticParameters_.addParameter(confusion::Parameter(
          referenceFrameOffsets_.begin()->second->trans.data(), 3,
          referenceFrameOffsets_.begin()->first + "_trans", true));
      conFusorPtr_->staticParameters_.addParameter(confusion::Parameter(
          referenceFrameOffsets_.begin()->second->rot.coeffs().data(), 4,
          referenceFrameOffsets_.begin()->first + "_rot", true,
          std::make_shared<confusion::QuatParam>()));
#else
      conFusorPtr_->staticParameters_.addParameter(confusion::Parameter(
          referenceFrameOffsets_.begin()->second->trans.data(), 3,
          referenceFrameOffsets_.begin()->first + "_trans", true));
      confusion::Parameter ftrParam(
          referenceFrameOffsets_.begin()->second->rot.coeffs().data(), 4,
          referenceFrameOffsets_.begin()->first + "_rot",
          std::make_shared<confusion::FixedYawParameterization>());
      ftrParam.setInitialConstraintWeighting(ftr_initialWeighting);
      conFusorPtr_->staticParameters_.addParameter(ftrParam);
#endif

      // Initialize all other reference frames
      for (const auto& externalReferenceFrame : referenceFrameOffsets_) {
        confusion::Parameter tagTransParam(
            externalReferenceFrame.second->trans.data(), 3,
            externalReferenceFrame.first + "_trans");
        tagTransParam.setInitialConstraintWeighting(t_w_t_initialWeighting);
        confusion::Parameter tagRotParam(
            externalReferenceFrame.second->rot.coeffs().data(), 4,
            externalReferenceFrame.first + "_rot",
            std::make_shared<confusion::QuatParam>());
        tagRotParam.setInitialConstraintWeighting(q_w_t_initialWeighting);
        conFusorPtr_->staticParameters_.addParameter(tagTransParam);
        conFusorPtr_->staticParameters_.addParameter(tagRotParam);
      }
    }
  }

  // Initialize camera/imu offset
  auto T_c_i_ptr = std::make_shared<confusion::Pose<double>>();
  T_c_i_ptr->trans(0) = pt.get<double>("T_c_i.px");
  T_c_i_ptr->trans(1) = pt.get<double>("T_c_i.py");
  T_c_i_ptr->trans(2) = pt.get<double>("T_c_i.pz");
  T_c_i_ptr->rot.w() = pt.get<double>("T_c_i.qw");
  T_c_i_ptr->rot.x() = pt.get<double>("T_c_i.qx");
  T_c_i_ptr->rot.y() = pt.get<double>("T_c_i.qy");
  T_c_i_ptr->rot.z() = pt.get<double>("T_c_i.qz");
  T_c_i_ptr->rot.normalize();
  sensorFrameOffsets_["cam"] = T_c_i_ptr;

  if (pt.get<bool>("optimizeTci")) {
    // Optimize Tci
    Eigen::MatrixXd t_c_i_initialWeighting(3, 3);
    t_c_i_initialWeighting.setIdentity();
    t_c_i_initialWeighting /= aprilTagParameters_.t_c_i_init_stddev;
    confusion::Parameter tciParam(T_c_i_ptr->trans.data(), 3, "t_c_i");
    tciParam.setInitialConstraintWeighting(t_c_i_initialWeighting);
    conFusorPtr_->addStaticParameter(tciParam);
    Eigen::MatrixXd q_c_i_initialWeighting(3, 3);
    q_c_i_initialWeighting.setIdentity();
    q_c_i_initialWeighting /= aprilTagParameters_.q_c_i_init_stddev;
    confusion::Parameter qciParam(T_c_i_ptr->rot.coeffs().data(), 4, "q_c_i",
                                  std::make_shared<confusion::QuatParam>());
    qciParam.setInitialConstraintWeighting(q_c_i_initialWeighting);
    conFusorPtr_->addStaticParameter(qciParam);
  } else {
    // Set up Tci parameters as constant
    conFusorPtr_->addStaticParameter(
        confusion::Parameter(T_c_i_ptr->trans.data(), 3, "T_c_i", true));
    conFusorPtr_->addStaticParameter(
        confusion::Parameter(T_c_i_ptr->rot.coeffs().data(), 4, "q_c_i", true,
                             std::make_shared<confusion::QuatParam>()));
  }
}

AprilTagModule::~AprilTagModule() {
  // Write tag poses to text file
  if (!referenceFrameOffsets_.empty()) {
    std::string tagMapFilename = confusionPath_ + "/tag_tracker_map.txt";
    writeTagPoses(tagMapFilename, referenceFrameOffsets_);
  }
}

void AprilTagModule::setCameraCalibration(
    const Eigen::Matrix<double, 3, 4>& projMat) {
  aprilTagParameters_.tagMeasCalibration_.projMat_ = projMat;
}

void AprilTagModule::processTagDetections(
    double t, std::vector<TagDetection>& tagDetections) {
  // Get the camera/imu offset pose
  std::shared_ptr<confusion::Pose<double>> T_c_i_ptr;
  try {
    T_c_i_ptr = sensorFrameOffsets_["cam"];
  } catch (const std::out_of_range& oor) {
    std::cerr
        << "ERROR: T_c_i is not initialized! Throwing out the tag measurements."
        << std::endl;
    return;
  }

  bool addedTagMeas = false;
  for (int i = 0; i < tagDetections.size(); ++i) {
    std::array<std::array<double, 2>, 4> corners;
    corners[0][0] = tagDetections[i].corners[0][0];
    corners[0][1] = tagDetections[i].corners[0][1];
    corners[1][0] = tagDetections[i].corners[1][0];
    corners[1][1] = tagDetections[i].corners[1][1];
    corners[2][0] = tagDetections[i].corners[2][0];
    corners[2][1] = tagDetections[i].corners[2][1];
    corners[3][0] = tagDetections[i].corners[3][0];
    corners[3][1] = tagDetections[i].corners[3][1];

    std::string referenceFrameName =
        "tag" + std::to_string(tagDetections[i].id);
    auto tagMeas = std::make_shared<confusion::TagMeas>(
        t, referenceFrameName, *T_c_i_ptr, corners,
        aprilTagParameters_.tagMeasCalibration_, tagMeasIndex_,
        aprilTagParameters_.stateParameterIndex_);
    conFusorPtr_->addUpdateMeasurement(tagMeas);

    addedTagMeas = true;
  }

  if (newMeasReceivedFlag_)
    *newMeasReceivedFlag_ =
        addedTagMeas;  // To tell the estimator a new measurement is ready

  if (verbose_) {
    std::cout << "Added " << tagDetections.size()
              << " tag measurements with stamp " << t << std::endl;
  }
}

void AprilTagModule::imageCallback(const double& t, const cv::Mat& image) {
  // Only process images at the desired rate
  if (t - t_last_image_ < maxImageDt_) {
    return;
  }
  t_last_image_ = t;

  // Detect tags in the image
  if (tagDetectionRunning_) {
    std::cout << "WARNING: New image received even though tag detection is "
                 "running. Dropping the image."
              << std::endl;
    return;
  }

  tagDetectionRunning_ = true;
  std::thread tagDetectionThread(&AprilTagModule::tagDetection, this, t, image);
  tagDetectionThread.detach();
}

void AprilTagModule::tagDetection(const double& t, const cv::Mat& image) {
  auto t_start = std::chrono::high_resolution_clock::now();
  // todo Check/correct image format?

  std::vector<TagDetection> tagDetections;
  tagDetector_.detectTags(image, tagDetections);

  if (!tagDetections.empty()) {
    processTagDetections(t, tagDetections);
  }
  tagDetectionRunning_ = false;
  auto t_stop = std::chrono::high_resolution_clock::now();
  std::cout << "Detected " << tagDetections.size() << " tags in "
            << std::chrono::duration<double>(t_stop - t_start).count() << " sec"
            << std::endl;
}

void AprilTagModule::copyOutEstimateAfterOptimization() {
  std::lock_guard<std::mutex> lg(estimateMutex_);
  for (auto& externalReferenceFrame : referenceFrameOffsets_)
    referenceFrameOffsetsOut_[externalReferenceFrame.first] =
        *externalReferenceFrame.second;
  for (auto& sensorFrame : sensorFrameOffsets_)
    sensorFrameOffsetsOut_[sensorFrame.first] = *sensorFrame.second;
}

void AprilTagModule::projectTagsIntoImage(const confusion::Pose<double>& T_w_i,
                                          const std::string& cameraName,
                                          cv::Mat& image) {
  std::vector<confusion::Pose<double>> T_w_t_vec;
  confusion::Pose<double> T_c_i;
  {
    std::lock_guard<std::mutex> lg(estimateMutex_);
    for (const auto& tagFrame : referenceFrameOffsetsOut_) {
      T_w_t_vec.push_back(tagFrame.second);
    }
    try {
      T_c_i = sensorFrameOffsetsOut_[cameraName];
    } catch (const std::out_of_range& oor) {
      std::cout << "ERROR: AprilTagModule::projectTagsIntoImage called for "
                   "unknown camera name."
                << std::endl;
      return;
    }
  }

  confusion::Pose<double> T_c_w = T_c_i * T_w_i.inverse();

  for (const auto& T_w_t : T_w_t_vec) {
    std::array<cv::Point, 4> corners;
    std::array<bool, 4> cornerVisible;

    confusion::Pose<double> T_c_t = T_c_w * T_w_t;
    for (int i = 0; i < 4; ++i) {
      // Get the position of each tag corner in the camera frame
      Eigen::Vector3d t_c_corner =
          T_c_t * aprilTagParameters_.tagMeasCalibration_.t_t_corner_[i];

      // Project the corner into the image plane
      corners[i].x = aprilTagParameters_.tagMeasCalibration_.projMat_(0, 0) *
                         (t_c_corner(0) / t_c_corner(2)) +
                     aprilTagParameters_.tagMeasCalibration_.projMat_(0, 2);
      corners[i].y = aprilTagParameters_.tagMeasCalibration_.projMat_(1, 1) *
                         (t_c_corner(1) / t_c_corner(2)) +
                     aprilTagParameters_.tagMeasCalibration_.projMat_(1, 2);
      cornerVisible[i] = t_c_corner(2) > 0.05;
    }

    // Draw the tag in the image
    static std::vector<int> firstCornerIndices = {0, 1, 2, 3};
    static std::vector<int> secondCornerIndices = {1, 2, 3, 0};
    for (int i = 0; i < 4; ++i) {
      // todo Improve handling of partially visible tags. For now just
      // thresholding on depth to corners.
      if (cornerVisible[firstCornerIndices[i]] &&
          cornerVisible[secondCornerIndices[i]]) {
        cv::line(image, corners[firstCornerIndices[i]],
                 corners[secondCornerIndices[i]], CV_RGB(0, 255, 0));
      }
    }
  }
}

}  // namespace confusion
