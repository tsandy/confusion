/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/modules/apriltag/apriltag_utils.h"

namespace confusion {

void AprilTagParameters::initialize() {
  tagMeasCalibration_.w_cx_ = 1.0 / tag_corner_stddev_;
  tagMeasCalibration_.w_cy_ = 1.0 / tag_corner_stddev_;

  double s = tagMeasCalibration_.tagSize_ / 2.0;
  tagMeasCalibration_.t_t_corner_[0] = Eigen::Vector3d(-s, -s, 0.0);
  tagMeasCalibration_.t_t_corner_[1] = Eigen::Vector3d(s, -s, 0.0);
  tagMeasCalibration_.t_t_corner_[2] = Eigen::Vector3d(s, s, 0.0);
  tagMeasCalibration_.t_t_corner_[3] = Eigen::Vector3d(-s, s, 0.0);

  initialized_ = true;
}

Eigen::Matrix4d getRelativeTransform(
    const std::array<std::array<double, 2>, 4>& corners, double tag_size,
    double fx, double fy, double cx, double cy) {
  std::vector<cv::Point3d> objPts;
  std::vector<cv::Point2d> imgPts;
  double s = tag_size / 2.;
  objPts.emplace_back(cv::Point3d(-s, -s, 0));  // bottom left
  objPts.emplace_back(cv::Point3d(s, -s, 0));   // bottom right
  objPts.emplace_back(cv::Point3d(s, s, 0));    // top right
  objPts.emplace_back(cv::Point3d(-s, s, 0));   // top left

  imgPts.emplace_back(cv::Point2d(corners[0][0], corners[0][1]));
  imgPts.emplace_back(cv::Point2d(corners[1][0], corners[1][1]));
  imgPts.emplace_back(cv::Point2d(corners[2][0], corners[2][1]));
  imgPts.emplace_back(cv::Point2d(corners[3][0], corners[3][1]));

  cv::Mat rvec, tvec;
  cv::Matx33d cameraMatrix(fx, 0, cx, 0, fy, cy, 0, 0, 1);
  cv::Vec4d distParam(0, 0, 0, 0);  // all 0?

  cv::solvePnP(objPts, imgPts, cameraMatrix, distParam, rvec, tvec);
  cv::Matx33d r;
  cv::Rodrigues(rvec, r);
  Eigen::Matrix3d wRo;
  wRo << r(0, 0), r(0, 1), r(0, 2), r(1, 0), r(1, 1), r(1, 2), r(2, 0), r(2, 1),
      r(2, 2);

  Eigen::Matrix4d T;
  T.topLeftCorner(3, 3) = wRo;
  T.col(3).head(3) << tvec.at<double>(0), tvec.at<double>(1),
      tvec.at<double>(2);
  T.row(3) << 0, 0, 0, 1;

  return T;
}

bool readTagPoses(
    const std::string& fname,
    std::map<std::string, std::shared_ptr<confusion::Pose<double>>>&
        externalReferenceFrames) {
  std::cout << "Reading tag map from " << fname << std::endl;
  std::ifstream file(fname, std::ios::in);

  if (!file.good()) {
    std::cout << "ERROR: Unable to open the tag map file!" << std::endl;
    return false;
  }

  char line[200];
  char delimiters[] = " ";
  while (file.good()) {
    file.getline(line, sizeof(line));
    char* token = strtok(line, delimiters);
    if (strcmp(token, "donetags") == 0) {
      break;
    } else {
      if (token == NULL) {
        std::cout << "Error in readTagPoses. Got a NULL token when expecting a "
                     "tag pose or donetags"
                  << std::endl;
        return false;
      }
      std::string frameName(token);

      Eigen::Matrix<double, 7, 1> poseData;
      for (int i = 0; i < 7; ++i) {
        token = strtok(NULL, delimiters);
        if (token == NULL) {
          std::cout
              << "Error in readTagPoses. Got a NULL token when reading the "
              << i << "-th element of a tag pose" << std::endl;
          return false;
        } else
          poseData(i) = atof(token);
      }

      confusion::Pose<double> T_w_t(poseData(0), poseData(1), poseData(2),
                                    poseData(3), poseData(4), poseData(5),
                                    poseData(6));
      T_w_t.rot.normalize();
      T_w_t.print();

      externalReferenceFrames[frameName] =
          std::make_shared<confusion::Pose<double>>(T_w_t);
    }
  }
  std::cout << "Read " << externalReferenceFrames.size()
            << " tag poses from the tag map file" << std::endl;

  return true;
}

void writeTagPoses(
    const std::string& fname,
    const std::map<std::string, std::shared_ptr<confusion::Pose<double>>>&
        externalReferenceFrames) {
  std::ofstream file(fname);

  // Write the tag poses
  for (auto extFrameIter = externalReferenceFrames.begin();
       extFrameIter != externalReferenceFrames.end(); ++extFrameIter) {
    file << extFrameIter->first << " " << extFrameIter->second->trans(0) << " "
         << extFrameIter->second->trans(1) << " "
         << extFrameIter->second->trans(2) << " "
         << extFrameIter->second->rot.w() << " "
         << extFrameIter->second->rot.x() << " "
         << extFrameIter->second->rot.y() << " "
         << extFrameIter->second->rot.z() << "\n";
  }
  file << "donetags\n";

  file.close();

  std::cout << "Wrote tag map to " << fname << std::endl;
}

}  // namespace confusion