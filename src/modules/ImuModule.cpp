/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/modules/imu/ImuModule.h"

namespace confusion {

ImuModule::ImuModule(ConFusor& conFusor,
                     boost::property_tree::ptree& pt, bool forwardPropagate)
    : conFusor_(conFusor),
      imuPropagator_(5000),
      forwardPropagateState_(forwardPropagate) {
  // Read the config
  imuCalibration_.gravityMagnitude_ = pt.get<double>("gravityMagnitude");
  double wi_stddev_ = pt.get<double>("wi_stddev");
  double ai_stddev_ = pt.get<double>("ai_stddev");
  double bg_stddev_ = pt.get<double>("bg_stddev");
  double ba_stddev_ = pt.get<double>("ba_stddev");

  imuCalibration_.cov_imu_nominal_.setIdentity();
  imuCalibration_.cov_imu_nominal_.block<3, 3>(0, 0) *= wi_stddev_ * wi_stddev_;
  imuCalibration_.cov_imu_nominal_.block<3, 3>(3, 3) *= ai_stddev_ * ai_stddev_;
  imuCalibration_.cov_imu_nominal_.block<3, 3>(6, 6) *= bg_stddev_ * bg_stddev_;
  imuCalibration_.cov_imu_nominal_.block<3, 3>(9, 9) *= ba_stddev_ * ba_stddev_;

  imuCalibration_.g_w_ << 0.0, 0.0, imuCalibration_.gravityMagnitude_;

  gravity_rot_.setZero();
#ifdef OPT_GRAVITY
  //	Eigen::MatrixXd gravityRotInitialWeighting(2,2);
  //	gravityRotInitialWeighting.setIdentity();
  //	gravityRotInitialWeighting /= pt.get<double>("ftr_init_stddev");
  conFusor_.addStaticParameter(Parameter(
      gravity_rot_.data(), 2, "g_r"));  //, gravityRotInitialWeighting);
#endif
}

void ImuModule::imuCallback(const std::shared_ptr<ImuMeas>& imuMeasPtr) {
  conFusor_.addProcessMeasurement(imuMeasPtr);

  if (forwardPropagateState_) imuPropagator_.addImuMeasurement(*imuMeasPtr);
}

void ImuModule::setImuStateEstimate(const ImuStateParameters& estState) {
  tracking_ = true;
  imuPropagator_.update(estState);
}

bool ImuModule::getImuStateAtTime(const double& t,
                                  ImuStateParameters& propState) {
  if (tracking_) {
    propState = imuPropagator_.propagate(t);
    return true;
  }

  return false;
}

}  // namespace confusion