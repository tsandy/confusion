/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/models/ErtMeasBase.h"

namespace confusion {

ErtMeasBase::ErtMeasBase(const double t, const std::string& referenceFrameName,
                         const std::string& sensorFrameName,
                         const ErtMeasConfig& config,
                         const std::string& measName,
                         const int measurement_type,
                         int starting_state_param_index)
    : confusion::UpdateMeasurement(measurement_type, t, measName, false),
      config_(config),
      starting_state_param_index_(starting_state_param_index) {
  setFrameNames(referenceFrameName, sensorFrameName);
}

void ErtMeasBase::setFrameNames(const std::string& referenceFrameName,
                                const std::string& sensorFrameName) {
  // Make sure the reference name doesn't start with a slash
  if (referenceFrameName.at(0) == '/')
    referenceFrameName_ = referenceFrameName.substr(1);
  else
    referenceFrameName_ = referenceFrameName;
  if (sensorFrameName.at(0) == '/')
    sensorFrameName_ = sensorFrameName.substr(1);
  else
    sensorFrameName_ = sensorFrameName;
}

void ErtMeasBase::assignExternalReferenceFrame(
    std::shared_ptr<confusion::Pose<double>> erf) {
  if (T_w_ref_ptr_) {
    std::cout << "ERROR: Trying to assign an externalReferenceFrame to a "
                 "PositionMeas that already "
                 "has one assigned to it!"
              << std::endl;
    return;
  }
  T_w_ref_ptr_ = erf;
}

}  // namespace confusion