/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitationsoptGravity_ under the License.
 */

#include "confusion/models/ImuChain.h"

namespace confusion {

ImuChainCostFuntion::ImuChainCostFuntion(ImuChain* imuChain)
    : imuChain_(imuChain) {}

bool ImuChainCostFuntion::Evaluate(double const* const* parameters,
                                   double* residuals,
                                   double** jacobians) const {
  return evaluateImuChainCost(imuChain_, parameters, residuals, jacobians);
}

ImuChainCostFuntionOptGravity::ImuChainCostFuntionOptGravity(ImuChain* imuChain)
    : imuChain_(imuChain) {
  // We have to use a dynamically sized cost function here because we require 11
  // parameters Set the parameter block sizes [p0,q0,v0,p1,q1,v1,ba,bg,gr]
  std::vector<int>* param_sizes = mutable_parameter_block_sizes();
  param_sizes->push_back(3);  // p0
  param_sizes->push_back(4);  // q0
  param_sizes->push_back(3);  // v0
  param_sizes->push_back(3);  // ba0
  param_sizes->push_back(3);  // bg0
  param_sizes->push_back(3);  // p1
  param_sizes->push_back(4);  // q1
  param_sizes->push_back(3);  // v1
  param_sizes->push_back(3);  // ba1
  param_sizes->push_back(3);  // bg1
  param_sizes->push_back(2);  // gravity_roll_pitch

  // Set the number of residuals
  set_num_residuals(15);
}

bool ImuChainCostFuntionOptGravity::Evaluate(double const* const* parameters,
                                             double* residuals,
                                             double** jacobians) const {
  return evaluateImuChainCost(imuChain_, parameters, residuals, jacobians);
}

ImuChain::ImuChain(bool optGravity, int startingStateParamIndex)
    : ProcessChain("IMU", false),
      optGravity_(optGravity),
      startingStateParamIndex_(startingStateParamIndex) {}

bool ImuChain::createCostFunction(
    std::unique_ptr<ceres::CostFunction>& costFunctionPtr,
    std::unique_ptr<ceres::LossFunction>& lossFunctionPtr,
    std::vector<size_t>& stateParameterIndexVector,
    std::vector<double*>& staticParameterDataVector) {
  auto firstMeasPtr = std::dynamic_pointer_cast<ImuMeas>(measurements_.front());

  stateParameterIndexVector.push_back(startingStateParamIndex_);
  stateParameterIndexVector.push_back(startingStateParamIndex_ + 1);
  stateParameterIndexVector.push_back(startingStateParamIndex_ + 2);
  stateParameterIndexVector.push_back(startingStateParamIndex_ + 3);
  stateParameterIndexVector.push_back(startingStateParamIndex_ + 4);

  if (optGravity_)
    staticParameterDataVector.push_back(firstMeasPtr->gravity_rot_->data());

  if (firstMeasPtr->imuCalibration_->imu_use_loss_func) {
    std::unique_ptr<ceres::LossFunction> lossFunctionPtr_(
        new ceres::CauchyLoss(firstMeasPtr->imuCalibration_->imu_loss_coeff));
    lossFunctionPtr = std::move(lossFunctionPtr_);
  } else
    lossFunctionPtr.reset();

  if (optGravity_) {
    std::unique_ptr<ceres::CostFunction> constFunctionPtr_(
        new ImuChainCostFuntionOptGravity(this));
    costFunctionPtr = std::move(constFunctionPtr_);
  } else {
    std::unique_ptr<ceres::CostFunction> constFunctionPtr_(
        new ImuChainCostFuntion(this));
    costFunctionPtr = std::move(constFunctionPtr_);
  }

  return true;
}

int ImuChain::residualDimension() { return 15; }

void ImuChain::setForUnitTestTrue() { forUnitTest_ = true; }

bool evaluateImuChainCost(ImuChain* imuChain, double const* const* x,
                          double* residuals, double** jacobians) {
#ifdef COST_DEBUG
  std::cout << "Starting IMU cost computation at t=" << imuChain->tStart()
            << " with " << imuChain->measurements_.size() << " measurements"
            << std::endl;
#endif
#ifdef IMU_COST_DEBUG
  std::cout << "Starting IMU cost computation" << std::endl;
#endif

  // x contains the previous (x[0] to x[4]) and the current state (x[5] to
  // x[9]),
  Eigen::Matrix<double, 3, 1> p_i_prev(x[0]);
  Eigen::Quaterniond q_i_prev = Eigen::Map<const Eigen::Quaterniond>(x[1]);
  Eigen::Matrix<double, 3, 1> v_i_prev(x[2]);
  Eigen::Matrix<double, 3, 1> b_a_prop(x[3]);
  Eigen::Matrix<double, 3, 1> b_g_prop(x[4]);

  // Get the current estimate of the gravity vector
  auto firstMeasPtr =
      std::dynamic_pointer_cast<ImuMeas>(imuChain->measurements_.front());
  Eigen::Vector3d g_w;
  if (imuChain->optGravity())
    g_w = gravityVec(x[10], firstMeasPtr->imuCalibration_->gravityMagnitude_);
  else
    g_w = firstMeasPtr->imuCalibration_->g_w_;

  Eigen::Matrix<double, 3, 1> p_i_prop = p_i_prev;
  Eigen::Quaterniond q_i_prop = q_i_prev;
  Eigen::Matrix<double, 3, 1> v_i_prop = v_i_prev;

  std::deque<std::shared_ptr<ProcessMeasurement>>::const_iterator imu_current;
  imu_current = imuChain->measurements_.begin();

  // Start with no state uncertainty
  Eigen::Matrix<double, 16, 16> cov_x(Eigen::Matrix<double, 16, 16>::Zero());
  Eigen::Matrix<double, 16, 16> dx1_dx0(
      Eigen::Matrix<double, 16, 16>::Identity());
  Eigen::Matrix<double, 16, 3> dx1_dgw(Eigen::Matrix<double, 16, 3>::Zero());

#ifdef IMU_COST_DEBUG
  std::cout << "Before p: " << p_i_prop.transpose()
            << "; q: " << q_i_prop.coeffs().transpose()
            << "; v: " << v_i_prop.transpose()
            << "; ba: " << b_a_prop.transpose()
            << "; bg: " << b_g_prop.transpose() << std::endl;
#endif

  size_t numIntegrations = 0;
  if (imuChain->optGravity()) {
    // Propagate from ti_ to t_imu_1
    if ((imu_current + 1) != imuChain->measurements_.end()) {
      if ((*imu_current)->t() <= imuChain->tStart()) {
        integrate_opt_gravity(
            p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop, *imu_current, g_w,
            (*(imu_current + 1))->t() - imuChain->tStart(), cov_x, dx1_dx0,
            dx1_dgw, firstMeasPtr->imuCalibration_->cov_imu_nominal_);
        ++imu_current;
        ++numIntegrations;
      } else {
        // This when there is no imu measurement preceding ti_
        integrate_opt_gravity(
            p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop, *imu_current, g_w,
            ((*imu_current)->t() - imuChain->tStart()), cov_x, dx1_dx0, dx1_dgw,
            firstMeasPtr->imuCalibration_->cov_imu_nominal_);
        ++numIntegrations;
      }
    }

    // Iterate through the imu measurements
    while ((imu_current + 1) != imuChain->measurements_.end() &&
           (*(imu_current + 1))->t() <= imuChain->tEnd()) {
      integrate_opt_gravity(
          p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop, *imu_current, g_w,
          (*(imu_current + 1))->t() - (*imu_current)->t(), cov_x, dx1_dx0,
          dx1_dgw, firstMeasPtr->imuCalibration_->cov_imu_nominal_);
      ++imu_current;
      ++numIntegrations;
    }

    // Propagate the last chunk of time. Don't do anything else if the last IMU
    // measurement is directly at tj_
    if ((*imu_current)->t() < imuChain->tEnd()) {
      integrate_opt_gravity(
          p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop, *imu_current, g_w,
          imuChain->tEnd() - (*imu_current)->t(), cov_x, dx1_dx0, dx1_dgw,
          firstMeasPtr->imuCalibration_->cov_imu_nominal_);
      ++numIntegrations;
    }
  } else {
    // Propagate from ti_ to t_imu_1
    if ((imu_current + 1) != imuChain->measurements_.end()) {
      if ((*imu_current)->t() <= imuChain->tStart()) {
        integrate(p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop,
                  *imu_current, g_w,
                  (*(imu_current + 1))->t() - imuChain->tStart(), cov_x,
                  dx1_dx0, firstMeasPtr->imuCalibration_->cov_imu_nominal_);
        ++imu_current;
        ++numIntegrations;
      } else {
        // This when there is no imu measurement preceding ti_
        integrate(p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop,
                  *imu_current, g_w, ((*imu_current)->t() - imuChain->tStart()),
                  cov_x, dx1_dx0,
                  firstMeasPtr->imuCalibration_->cov_imu_nominal_);
        ++numIntegrations;
      }
    }

    // Iterate through the imu measurements
    while ((imu_current + 1) != imuChain->measurements_.end() &&
           (*(imu_current + 1))->t() <= imuChain->tEnd()) {
      integrate(p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop, *imu_current,
                g_w, (*(imu_current + 1))->t() - (*imu_current)->t(), cov_x,
                dx1_dx0, firstMeasPtr->imuCalibration_->cov_imu_nominal_);
      ++imu_current;
      ++numIntegrations;
    }

    // Propagate the last chunk of time. Don't do anything else if the last IMU
    // measurement is directly at tj_
    if ((*imu_current)->t() < imuChain->tEnd()) {
      integrate(p_i_prop, q_i_prop, v_i_prop, b_a_prop, b_g_prop, *imu_current,
                g_w, imuChain->tEnd() - (*imu_current)->t(), cov_x, dx1_dx0,
                firstMeasPtr->imuCalibration_->cov_imu_nominal_);
      ++numIntegrations;
    }
  }

  if (numIntegrations < 2) {
    // We need to give a small non-zero uncertainty to the IMU position if only
    // one measurement is linked even though there should actually be no
    // constraint in the position according to the measurement model.
    cov_x.topLeftCorner<3, 3>().setIdentity();
    cov_x.topLeftCorner<3, 3>() *= 1e-22;
  }

#if defined IMU_COST_DEBUG
  std::cout << "After p: " << p_i_prop.transpose()
            << "; q: " << q_i_prop.coeffs().transpose()
            << "; v: " << v_i_prop.transpose()
            << "; ba: " << b_a_prop.transpose()
            << "; bg: " << b_g_prop.transpose() << std::endl;
#endif

  // Get error from the propagated state to the current estimate
  Eigen::Map<Eigen::Matrix<double, 15, 1>> e(residuals);
  VectorDistance(p_i_prop.data(), x[5], e.data());
  QuatDistance(q_i_prop, Eigen::Quaterniond(x[6]), e.data() + 3);
  VectorDistance(v_i_prop.data(), x[7], e.data() + 6);
  VectorDistance(b_a_prop.data(), x[8], e.data() + 9);
  VectorDistance(b_g_prop.data(), x[9], e.data() + 12);

  // Partial derivative of the error wrt the propogated state
  Eigen::Matrix<double, 15, 16> de_dx1p(Eigen::Matrix<double, 15, 16>::Zero());
  de_dx1p.block<3, 3>(0, 0).setIdentity();
  de_dx1p.block<9, 9>(6, 7).setIdentity();
  Eigen::Matrix<double, 3, 4> de_dqm_left;
  calc_de_dq_left(q_i_prop, Eigen::Quaterniond(x[6]), de_dqm_left);
  de_dx1p.block<3, 4>(3, 3) = de_dqm_left;

  // Partial derivative of the error wrt the estimated state
  Eigen::Matrix<double, 15, 16> de_dx1 = -1.0 * de_dx1p;
  Eigen::Matrix<double, 3, 4> de_dqm_right;
  calc_de_dq_right(q_i_prop, Eigen::Quaterniond(x[6]), de_dqm_right);
  de_dx1.block<3, 4>(3, 3) = de_dqm_right;

  // For unit testing, we set the weighting to the identity. This is because the
  // assumption that the weighting is constant breaks the unit test.
  Eigen::Matrix<double, 15, 15> S;
  if (imuChain->forUnitTest()) {
    S.setIdentity();
  } else {
    Eigen::Matrix<double, 15, 15> cov_e = de_dx1p * cov_x * de_dx1p.transpose();

    // We need the stiffness matrix: cov^(-1/2)
    Eigen::Matrix<double, 15, 15> inf = cov_e.inverse();
    inf = 0.5 * inf + 0.5 * inf.transpose().eval();
    Eigen::LLT<Eigen::Matrix<double, 15, 15>> llt(inf);
    S = llt.matrixL().transpose();
  }

  // Weigh errors by the stiffness
  e = S * e;

  // Fill in Jacobians
  if (jacobians) {
    Eigen::Matrix<double, 15, 16> c1 = S * de_dx1p;
    Eigen::Matrix<double, 15, 16> c2 = S * de_dx1;
    if (jacobians[0]) {
      // de_dp0
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dp0(
          jacobians[0]);
      de_dp0 = c1 * dx1_dx0.block<16, 3>(0, 0);
    }
    if (jacobians[1]) {
      // de_dq0
      // Need to reorder columns [x,y,z,w]!
      Eigen::Matrix<double, 15, 4> de_dq0_temp =
          c1 * dx1_dx0.block<16, 4>(0, 3);
      Eigen::Map<Eigen::Matrix<double, 15, 4, Eigen::RowMajor>> de_dq0(
          jacobians[1]);
      de_dq0.block<15, 3>(0, 0) = de_dq0_temp.block<15, 3>(0, 1);
      de_dq0.block<15, 1>(0, 3) = de_dq0_temp.block<15, 1>(0, 0);
    }
    if (jacobians[2]) {
      // de_dv0
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dv0(
          jacobians[2]);
      de_dv0 = c1 * dx1_dx0.block<16, 3>(0, 7);
    }
    if (jacobians[3]) {
      // de_dba0
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dba0(
          jacobians[3]);
      de_dba0 = c1 * dx1_dx0.block<16, 3>(0, 10);
    }
    if (jacobians[4]) {
      // de_dbg0
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dbg0(
          jacobians[4]);
      de_dbg0 = c1 * dx1_dx0.block<16, 3>(0, 13);
    }
    if (jacobians[5]) {
      // de_dp1
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dp1(
          jacobians[5]);
      de_dp1 = c2.block<15, 3>(0, 0);
      // std::cout << "dei_dp1: " << de_dp1 << std::endl;
    }
    if (jacobians[6]) {
      // de_dq1
      // Need to reorder columns [x,y,z,w] because Ceres sees the quat data in
      // that order!
      Eigen::Matrix<double, 15, 4> de_dq1_temp = c2.block<15, 4>(0, 3);
      Eigen::Map<Eigen::Matrix<double, 15, 4, Eigen::RowMajor>> de_dq1(
          jacobians[6]);
      de_dq1.block<15, 3>(0, 0) = de_dq1_temp.block<15, 3>(0, 1);
      de_dq1.block<15, 1>(0, 3) = de_dq1_temp.block<15, 1>(0, 0);
    }
    if (jacobians[7]) {
      // de_dv1
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dv1(
          jacobians[7]);
      de_dv1 = c2.block<15, 3>(0, 7);
    }
    if (jacobians[8]) {
      // de_dba
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dba(
          jacobians[8]);
      de_dba = c2.block<15, 3>(0, 10);
    }
    if (jacobians[9]) {
      // de_dbg
      Eigen::Map<Eigen::Matrix<double, 15, 3, Eigen::RowMajor>> de_dbg(
          jacobians[9]);
      de_dbg = c2.block<15, 3>(0, 13);
    }
    if (imuChain->optGravity() && jacobians[10]) {
      // de_dgw
      Eigen::Matrix<double, 15, 3> de_dgw = c1 * dx1_dgw;

      // Need to relate this to the gravity vector rotation angles
      Eigen::Matrix<double, 3, 2> dg_dr = gravityVec_jacob(
          x[10], firstMeasPtr->imuCalibration_->gravityMagnitude_);
      Eigen::Map<Eigen::Matrix<double, 15, 2, Eigen::RowMajor>> de_dr(
          jacobians[10]);
      de_dr = de_dgw * dg_dr;
    }
  }

#ifdef COST_DEBUG
  std::cout << "IMU cost for " << imuChain->measurements_.size() << " meas = ["
            << e.transpose() << "]" << std::endl;
#endif

  return true;
}

}  // namespace confusion