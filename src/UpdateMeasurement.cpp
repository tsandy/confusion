/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/UpdateMeasurement.h"

namespace confusion {

UpdateMeasurement::UpdateMeasurement(int measType, double t, std::string name,
                                     bool dynamicParameters)
    : measType_(measType),
      t_(t),
      name_(std::move(name)),
      dynamicParameters_(dynamicParameters) {}

bool UpdateMeasurement::check() const {
  bool res = true;

  if (initialized_) {
    if (linkedStateParameters_.empty()) {
      std::cout << "ERROR: Update measurement type " << measType()
                << " at t=" << t_
                << " does not have any state parameters assigned to it!"
                << std::endl;
      res = false;
    }
    if (linkedStateParameters_.size() + linkedStaticParameters_.size() !=
        parameterDataVector_.size()) {
      std::cout << "ERROR in UpdateMeasurement type " << measType()
                << "! Mismatch between "
                   " the number of linked parameters and parameter blocks!"
                << std::endl;
      res = false;
    }
  }

  if (!checkDerived()) res = false;

  return res;
}

std::vector<double> UpdateMeasurement::evaluateResidual() {
  if (!initialized()) {
    std::cout << "The user requested to externally evaluate a cost function"
                 " that has not yet been initialized!"
              << std::endl;
    return {};
  }
  std::vector<double> residual(residualDimension());
  costFunctionPtr_->Evaluate(parameterDataVector_.data(), residual.data(),
                             nullptr);
  return residual;
}

bool UpdateMeasurement::addCostToProblem(
    ceres::Problem* problem, std::vector<Parameter>& stateParameters,
    StaticParameterVector& staticParameterVector) {
  if (!enable_)
    return false;
  else if (dynamicParameters_ || !initialized_) {
    costFunctionPtr_.reset();
    lossFunctionPtr_.reset();
    parameterDataVector_.clear();
    linkedStateParameters_.clear();
    linkedStaticParameters_.clear();
    initialized_ = false;

    std::vector<size_t> stateParameterIndexVector;
    std::vector<double*> staticParameterDataVector;
    if (!createCostFunction(costFunctionPtr_, lossFunctionPtr_,
                            stateParameterIndexVector,
                            staticParameterDataVector))
      return false;

    if (!costFunctionPtr_) {
      std::cout << "ERROR: createCostFunction returned true but the"
                   " cost function was not created!"
                << std::endl;
      return false;
    }

    if (stateParameterIndexVector.empty() &&
        staticParameterDataVector.empty()) {
      std::cout << "ERROR: createCostFunction returned true but there"
                   " are no state or static parameters linked to the "
                   "UpdateMeasurment cost function type "
                << measType_ << "!" << std::endl;
      return false;
    }

    // Parse the linked parameters specified and build the objects required
    // for linking in ConFusion Parameters must always be ordered with state
    // parameters first
    for (auto& index : stateParameterIndexVector) {
      if (index >= stateParameters.size() || index < 0) {
        std::cout << "ERROR: Specified state parameter index of " << index
                  << " out of range in UpdateMeasurement::addCostToProblem "
                     "for measurement type "
                  << measType_ << std::endl;
        abort();
      }
      linkedStateParameters_.push_back(&stateParameters[index]);
      parameterDataVector_.push_back(stateParameters[index].data_);
    }
    for (auto& dataPtr : staticParameterDataVector) {
      Parameter* parameter = staticParameterVector.getParameter(dataPtr);
      if (!parameter) {
        std::cout << "ERROR: Couldn't find a linked static parameter in "
                     "UpdateMeasurement::addCostToProblem "
                     "for measurement type "
                  << measType_ << std::endl;
        abort();
      }
      linkedStaticParameters_.push_back(parameter);
      parameterDataVector_.push_back(dataPtr);
    }

    if (parameterDataVector_.empty()) {
      std::cout << "ERROR: createCostFunction returned true but the"
                   " parameter vector is empty!"
                << std::endl;
      return false;
    }

    initialized_ = true;
  }

  // Add the cost function to the problem
  // Note that we assume that a Dynamic ceres cost function type is not used
  // unless there are more than 10 associated parameters
  int numParams = parameterDataVector_.size();
  if (dynamicParameters_ || numParams > 10)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_);
  else if (numParams == 10)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6],
        parameterDataVector_[7], parameterDataVector_[8],
        parameterDataVector_[9]);
  else if (numParams == 9)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6],
        parameterDataVector_[7], parameterDataVector_[8]);
  else if (numParams == 8)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6],
        parameterDataVector_[7]);
  else if (numParams == 7)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6]);
  else if (numParams == 6)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5]);
  else if (numParams == 5)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4]);
  else if (numParams == 4)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3]);
  else if (numParams == 3)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2]);
  else if (numParams == 2)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1]);
  else
    residualBlockId_ = problem->AddResidualBlock(costFunctionPtr_.get(),
                                                 lossFunctionPtr_.get(),
                                                 parameterDataVector_[0]);

  // Active the linked state and static parameters
  for (auto& param : linkedStateParameters_) param->active_ = true;
  for (auto& param : linkedStaticParameters_) param->active_ = true;

  return true;
}

}  // namespace confusion