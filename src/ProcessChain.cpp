/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/ProcessChain.h"

namespace confusion {

void ProcessChain::assignTimes(double tStart, double tEnd) {
  tStart_ = tStart;
  tEnd_ = tEnd;
  ready_ = true;
}

bool ProcessChain::check() {
  bool res = true;

  if (ready_) {
    // Check for process measurement at or before the leading state
    if (!measurements_.empty()) {  // We allow there to be no associated process
                                   // measurements to support purely random walk
                                   // process models
      if (measurements_.front()->t() > tStart()) {
        std::cout << "WARNING: There is no process measurement of type "
                  << name()
                  << "  at or before the leading state at t=" << tStart()
                  << "; t_process_front=" << measurements_.front()->t()
                  << std::endl;
      }
      if (measurements_.size() > 1 && measurements_[1]->t() <= tStart_) {
        int i = 0;
        while (i < measurements_.size() && measurements_[i]->t() <= tStart_)
          ++i;
        std::cout << "ERROR: There are too many process measurements of type "
                  << name()
                  << " at or before the leading state at t=" << tStart() << "; "
                  << i << " of " << measurements_.size()
                  << " meas are at or before the start state" << std::endl;
        res = false;
      }

      // Check for process measurements coming at or after the following state
      if (measurements_.back()->t() >= tEnd()) {
        std::cout << "ERROR: ProcessChain of type " << name()
                  << "  has a measurement following the trailing state! "
                     "t_state="
                  << tEnd() << ". t_meas_back=" << measurements_.back()->t()
                  << std::endl;
        res = false;
      }
    }
  }

  if (initialized_) {
    if (linkedStartStateParameters_.empty()) {
      std::cout << "ERROR: Process chain of type " << name()
                << "  does not have any state parameters assigned to the "
                   "starting state!"
                << std::endl;
      res = false;
    }

    if (linkedEndStateParameters_.empty()) {
      std::cout << "ERROR: Process chain of type " << name()
                << " does not have any state parameters assigned to the "
                   "trailing state!"
                << std::endl;
      res = false;
    }

    if (linkedStartStateParameters_.size() + linkedEndStateParameters_.size() +
            linkedStaticParameters_.size() !=
        parameterDataVector_.size()) {
      std::cout << "ERROR in ProcessChain of type " << name()
                << "! Mismatch between "
                   " the number of linked parameters and parameter blocks!"
                << std::endl;
    }
  }

  return res;
}

bool ProcessChain::addCostToProblem(
    ceres::Problem* problem, std::vector<Parameter>& startStateParameters,
    std::vector<Parameter>& endStateParameters,
    StaticParameterVector& staticParameterVector) {
  if (!enable_)
    return false;
  else if (dynamicParameters_ || !initialized_) {
    costFunctionPtr_.reset();
    lossFunctionPtr_.reset();
    parameterDataVector_.clear();
    linkedStartStateParameters_.clear();
    linkedEndStateParameters_.clear();
    linkedStaticParameters_.clear();

    std::vector<size_t> stateParameterIndexVector;
    std::vector<double*> staticParameterDataVector;
    if (!createCostFunction(costFunctionPtr_, lossFunctionPtr_,
                            stateParameterIndexVector,
                            staticParameterDataVector))
      return false;

    if (!costFunctionPtr_) {
      std::cout << "ERROR: createCostFunction returned true but the"
                   " cost function was not created!"
                << std::endl;
      return false;
    }

    if (stateParameterIndexVector.empty() &&
        staticParameterDataVector.empty()) {
      std::cout << "ERROR: createCostFunction returned true but there"
                   " are no state or static parameters linked to the "
                   "ProcessChain cost function "
                << name_ << "!" << std::endl;
      return false;
    }

    // Parse the linked parameters specified and build the objects required for
    // linking in ConFusion Parameters must always be ordered as [leading state
    // parameters, trailing state parameters, static parameters]
    for (auto& index : stateParameterIndexVector) {
      if (index >= startStateParameters.size() || index < 0) {
        std::cout << "ERROR: Specified state parameter index of " << index
                  << " out of range in ProcessChain::addCostToProblem "
                     "for measurement type "
                  << name_ << std::endl;
        abort();
      }
      linkedStartStateParameters_.push_back(&startStateParameters[index]);
      parameterDataVector_.push_back(startStateParameters[index].data_);
    }
    for (auto& index : stateParameterIndexVector) {
      if (index >= endStateParameters.size() || index < 0) {
        std::cout << "ERROR: Specified state parameter index of " << index
                  << " out of range in ProcessChain::addCostToProblem "
                     "for measurement type "
                  << name_ << std::endl;
        abort();
      }
      linkedEndStateParameters_.push_back(&endStateParameters[index]);
      parameterDataVector_.push_back(endStateParameters[index].data_);
    }
    for (auto& dataPtr : staticParameterDataVector) {
      Parameter* parameter = staticParameterVector.getParameter(dataPtr);
      if (!parameter) {
        std::cout << "ERROR: Couldn't find a linked static parameter in "
                     "ProcessChain::addCostToProblem "
                     "for measurement type "
                  << name_ << std::endl;
        abort();
      }
      linkedStaticParameters_.push_back(parameter);
      parameterDataVector_.push_back(dataPtr);
    }

    initialized_ = true;
  }

  // Add the cost function to the problem
  // Note that we assume that a Dynamic ceres cost function type is not used
  // unless there are more than 10 associated parameters
  int numParams = parameterDataVector_.size();
  if (dynamicParameters_ || numParams > 10)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_);
  else if (numParams == 10)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6],
        parameterDataVector_[7], parameterDataVector_[8],
        parameterDataVector_[9]);
  else if (numParams == 9)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6],
        parameterDataVector_[7], parameterDataVector_[8]);
  else if (numParams == 8)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6],
        parameterDataVector_[7]);
  else if (numParams == 7)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5], parameterDataVector_[6]);
  else if (numParams == 6)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4],
        parameterDataVector_[5]);
  else if (numParams == 5)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3], parameterDataVector_[4]);
  else if (numParams == 4)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2],
        parameterDataVector_[3]);
  else if (numParams == 3)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1], parameterDataVector_[2]);
  else if (numParams == 2)
    residualBlockId_ = problem->AddResidualBlock(
        costFunctionPtr_.get(), lossFunctionPtr_.get(), parameterDataVector_[0],
        parameterDataVector_[1]);
  else
    residualBlockId_ = problem->AddResidualBlock(costFunctionPtr_.get(),
                                                 lossFunctionPtr_.get(),
                                                 parameterDataVector_[0]);

  // Active the linked state and static parameters
  for (auto& param : linkedStartStateParameters_) param->active_ = true;
  for (auto& param : linkedEndStateParameters_) param->active_ = true;
  for (auto& param : linkedStaticParameters_) param->active_ = true;

  return true;
}

void ProcessChain::reset() {
  measurements_.clear();
  initialized_ = false;
  linkedStartStateParameters_.clear();
  linkedEndStateParameters_.clear();
  linkedStaticParameters_.clear();
  costFunctionPtr_.reset();
  lossFunctionPtr_.reset();
  parameterDataVector_.clear();

  derivedReset();
}

}  // namespace confusion