/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/State.h"

namespace confusion {

State::State(double t, int numProcessSensors, int numUpdateSensors)
    : t_(t),
      updateMeasurements_(numUpdateSensors),
      processChains_(numProcessSensors),
      numProcessSensors_(numProcessSensors),
      numUpdateSensors_(numUpdateSensors) {}

void State::setInitialStateWeighting(
    std::vector<Eigen::MatrixXd> stateParamWeightings) {
  if (stateParamWeightings.size() != parameters_.size()) {
    std::cout << "ERROR: setInitialStateConfidence called with the wrong "
                 "number of weighting matrices. "
                 "If a state parameter has no prior constraint, send an "
                 "empty matrix for it. "
                 "# of matrices: "
              << stateParamWeightings.size()
              << ", expected #: " << parameters_.size() << std::endl;
    abort();
  }

  for (size_t i = 0; i < stateParamWeightings.size(); ++i) {
    if (stateParamWeightings[i].rows() > 0)
      parameters_[i].setInitialConstraintWeighting(stateParamWeightings[i]);
  }
}

bool State::addUpdateMeasDerived(std::shared_ptr<UpdateMeasurement> measPtr,
                                 StaticParameterVector& staticParameters) {
  // This check is in the default derived implementation so that the user
  // can optionally pass measurements beyond the specified number of sensors
  if (measPtr->measType() < int(updateMeasurements_.size()))
    updateMeasurements_[measPtr->measType()].push_back(measPtr);
  else {
    std::cout << "WARNING: State tried adding an update measurement with "
                 "undefined type! Dropping the measurement."
              << std::endl;
    return false;
  }

  return true;
}

void State::addProcessMeas(std::shared_ptr<ProcessMeasurement> measPtr) {
  // todo Is there a case where a derived implementation is required?
  // todo Return a boolean like addUpdateMeas?
  if (measPtr->measType() < int(processChains_.size()))
    processChains_[measPtr->measType()]->measurements_.push_back(measPtr);
  else
    std::cout << "WARNING: State tried adding a process measurement with "
                 "undefined type! Dropping the measurement."
              << std::endl;
}

std::shared_ptr<State> State::createNextState(
    const std::shared_ptr<UpdateMeasurement>& updateMeas,
    StaticParameterVector& staticParameters) {
  // Create dummy measurement buffers
  std::vector<std::deque<std::shared_ptr<UpdateMeasurement>>> updateMeasBuffer(
      numUpdateSensors_);
  updateMeasBuffer[updateMeas->measType_].push_back(updateMeas);
  std::vector<std::deque<std::shared_ptr<ProcessMeasurement>>>
      processMeasBuffer;
  for (size_t i = 0; i < numProcessSensors_; ++i) {
    processMeasBuffer.push_back(processChains_[i]->measurements_);
  }

  return createNextState(processMeasBuffer, updateMeasBuffer, staticParameters);
}

size_t State::residualDimension() {
  size_t errorDim = 0;
  for (size_t i = 0; i < numUpdateSensors_; ++i)
    for (size_t j = 0; j < updateMeasurements_[i].size(); ++j)
      errorDim += updateMeasurements_[i][j]->residualDimension();
  for (size_t i = 0; i < numProcessSensors_; ++i)
    errorDim += processChains_[i]->residualDimension();
  return errorDim;
}

void State::addNewParametersToPrior(StaticParameterVector& staticParameters,
                                    PriorConstraint& priorConstraint) {
  for (size_t i = 0; i < numUpdateSensors_; ++i) {
    for (size_t j = 0; j < updateMeasurements_[i].size(); ++j) {
      if (updateMeasurements_[i][j]->isEnabled()) {
        for (size_t k = 0;
             k < updateMeasurements_[i][j]->linkedStaticParameters_.size();
             ++k) {
          priorConstraint.addStaticParameter(
              *updateMeasurements_[i][j]->linkedStaticParameters_[k]);
        }
      }
    }
  }
  for (size_t i = 0; i < numProcessSensors_; ++i) {
    for (size_t j = 0; j < processChains_[i]->linkedStaticParameters_.size();
         ++j) {
      priorConstraint.addStaticParameter(
          *processChains_[i]->linkedStaticParameters_[j]);
    }
  }
}

void State::check() const {
  if (parameters_.empty()) {
    std::cout << "ERROR: State parameters_ is empty! You must specify the "
                 "state parameters in the derived class!"
              << std::endl;
    abort();
  }
  if (processChains_.size() != numProcessSensors_) {
    std::cout << "ERROR: The processChains_ vector is not properly setup!"
              << std::endl;
    abort();
  }
  for (const auto& processChain : processChains_) {
    if (!processChain) {
      std::cout << "ERROR: One process chain is NULL in processChains_!"
                << std::endl;
      abort();
    }
  }
}

void State::reset() {
  // Clear out the update measurements
  for (size_t i = 0; i < numUpdateSensors_; ++i) updateMeasurements_[i].clear();
  // Reset the process chains
  for (size_t i = 0; i < numProcessSensors_; ++i) processChains_[i]->reset();

  derivedReset();
}

bool State::addUpdateMeas(std::shared_ptr<UpdateMeasurement> measPtr,
                          StaticParameterVector& staticParameters) {
  return addUpdateMeasDerived(std::move(measPtr), staticParameters);
}

}  // namespace confusion