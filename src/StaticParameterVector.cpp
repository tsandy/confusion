/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/StaticParameterVector.h"

namespace confusion {

StaticParameterVector::StaticParameterVector(const StaticParameterVector& sp)
    : parameterMap_(sp.parameterMap_), globalSize_(sp.globalSize_) {}

StaticParameterVector& StaticParameterVector::operator=(
    const StaticParameterVector& spv) {
  if (this == &spv) return *this;

  parameterMap_ = spv.parameterMap_;
  globalSize_ = spv.globalSize_;

  return *this;
}

size_t StaticParameterVector::numParameters() const {
  return parameterMap_.size();
}

void StaticParameterVector::addParameter(const confusion::Parameter& param) {
  parameterMap_.emplace(std::make_pair(param.data_, param));
  globalSize_ += param.size();
}

confusion::Parameter* StaticParameterVector::getParameter(double* dataPtr) {
  confusion::Parameter* parameter;
  try {
    parameter = &(parameterMap_.at(dataPtr));
  } catch (const std::out_of_range& oor) {
    std::cerr << "ERROR: Didn't find parameter at " << dataPtr
              << " in the StaticParameterVector!" << std::endl;
    return nullptr;
  }
  return parameter;
}

void StaticParameterVector::deactivateParameters() {
  for (auto& p : parameterMap_) p.second.active_ = false;
}

void StaticParameterVector::removeParameter(double* dataPtr) {
  int numParamsRemoved = parameterMap_.erase(dataPtr);
  if (numParamsRemoved != 1) {
    std::cout << "Removing parameter from the StaticParameterVector actually "
                 "removed "
              << numParamsRemoved << " parameters. Something is probably wrong!"
              << std::endl;
  }
}

void StaticParameterVector::attachStaticParameterRandomWalkProcess(
    double* data, const double& processNoise) {
  confusion::Parameter* parameter = getParameter(data);
  if (!parameter) {
    std::cout << "ERROR: Could not find desired parameter for "
                 "StaticParameterVector::setStaticParameterRandomWalkProcess."
              << std::endl;
    return;
  }
  parameter->attachRandomWalkProcess(processNoise);
}

void StaticParameterVector::detachStaticParameterRandomWalkProcess(
    double* data) {
  confusion::Parameter* parameter = getParameter(data);
  if (!parameter) {
    std::cout << "ERROR: Could not find desired parameter for "
                 "StaticParameterVector::"
                 "deactivateStaticParameterRandomWalkProcess."
              << std::endl;
    return;
  }

  parameter->detachRandomWalkProcess();
}

void StaticParameterVector::print() const {
  for (auto& p : parameterMap_) {
    std::cout << "Static parameter " << p.second.name()
              << ": size=" << p.second.size()
              << "; constant=" << p.second.isConstant()
              << "; priorConstr=" << p.second.priorConstraintActive()
              << std::endl;
  }
}

// todo Add tests?
bool StaticParameterVector::check() const { return true; }

size_t StaticParameterVector::globalSize() const { return globalSize_; }

std::map<double*, confusion::Parameter>::iterator
StaticParameterVector::begin() {
  return parameterMap_.begin();
}
std::map<double*, confusion::Parameter>::iterator StaticParameterVector::end() {
  return parameterMap_.end();
}
std::map<double*, confusion::Parameter>::const_iterator
StaticParameterVector::begin() const {
  return parameterMap_.begin();
}
std::map<double*, confusion::Parameter>::const_iterator
StaticParameterVector::end() const {
  return parameterMap_.end();
}
std::size_t StaticParameterVector::size() const { return parameterMap_.size(); }

}  // namespace confusion