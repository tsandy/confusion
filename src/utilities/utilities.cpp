/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/utilities/utilities.h"

namespace confusion {

SolverOverrunCallback::SolverOverrunCallback(const bool& abortSolver,
                                             const int minIterations)
    : abortSolver_(abortSolver), minIterations_(minIterations) {}

ceres::CallbackReturnType SolverOverrunCallback::operator()(
    const ceres::IterationSummary& summary) {
  // Allow the user to terminate the optimization if desired
  if (abortSolver_ && summary.iteration >= minIterations_) {
    std::cout << "--------Solver stopping early because a new update "
                 "measurement was received---------"
              << std::endl;
    return ceres::SOLVER_TERMINATE_SUCCESSFULLY;
  }

  return ceres::SOLVER_CONTINUE;
}

SolverTimeoutCallback::SolverTimeoutCallback(
    const std::chrono::time_point<std::chrono::high_resolution_clock>& t_start,
    const double& maxOptimizationDuration, const int minIterations)
    : t_start_(t_start),
      maxOptimizationDuration_(maxOptimizationDuration),
      minIterations_(minIterations) {}

ceres::CallbackReturnType SolverTimeoutCallback::operator()(
    const ceres::IterationSummary& summary) {
  // Allow the user to terminate the optimization if desired
  if (std::chrono::high_resolution_clock::now() - t_start_ >
          maxOptimizationDuration_ &&
      summary.iteration >= minIterations_) {
    std::cout
        << "--------Solver stopping early because solving took long---------"
        << std::endl;
    return ceres::SOLVER_TERMINATE_SUCCESSFULLY;
  }

  return ceres::SOLVER_CONTINUE;
}

bool schurMarginalize(const Eigen::MatrixXd& H, const Eigen::VectorXd& b,
                      const size_t& margDim, Eigen::MatrixXd& Wout,
                      Eigen::VectorXd& eout, Eigen::MatrixXd* HstarOut,
                      Eigen::VectorXd* bstarOut) {
  bool res = true;
  size_t stayDim = H.rows() - margDim;

  // Invert the top-left corner of H. Could use a pre-conditioner here if
  // required, but haven't needed it yet.
  Eigen::MatrixXd Hmm = 0.5 * (H.topLeftCorner(margDim, margDim) +
                               H.topLeftCorner(margDim, margDim).transpose())
                                  .eval();  // Enforce symmetry
  Eigen::LLT<Eigen::MatrixXd> llt(Hmm);
  Eigen::MatrixXd ii(margDim, margDim);
  ii.setIdentity();
  Eigen::MatrixXd Hmm_inv = llt.solve(ii);
  if (llt.info() == Eigen::NumericalIssue) {
    std::cout << "\n-----Numerical issue inverting Hmm!-----\n" << std::endl;
    res = false;
    // todo Use SVD and force no update on unobserved parameter dimensions?
  }

  // Compute Hstar
  Eigen::MatrixXd Hstar = H.bottomRightCorner(stayDim, stayDim) -
                          H.bottomLeftCorner(stayDim, margDim) * Hmm_inv *
                              H.topRightCorner(margDim, stayDim);
  if (HstarOut) {
    *HstarOut = Hstar;
  }

  // Compute bstar
  Eigen::VectorXd bstar =
      b.tail(stayDim) -
      H.bottomLeftCorner(stayDim, margDim) * Hmm_inv * b.head(margDim);
  if (bstarOut) {
    *bstarOut = bstar;
  }

  // Compute SVD of Hstar, using a preconditioner (copied from okvis)
  // Hstar = 0.5*(Hstar+Hstar.transpose()).eval(); //Make sure Hstar is symmetric
  Eigen::MatrixXd p = (Hstar.diagonal().array() > 1.0e-9)
                          .select(Hstar.diagonal().cwiseSqrt(), 1.0e-3);
  Eigen::MatrixXd p_inv = p.cwiseInverse();

  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> saes(
      0.5 * p_inv.asDiagonal() * (Hstar + Hstar.transpose()) *
      p_inv.asDiagonal());
  if (saes.info() != Eigen::Success) {
    std::cout << "\n-----Eigendecomposition of Hstar was not successful while "
                 "computing Schur complement! Aborting-----\n"
              << std::endl;
    std::cout << "Hstar:\n" << Hstar << std::endl;
    abort();
  }

  // Pre-conditioned SVD matrix inversion
  static const double epsilon = std::numeric_limits<double>::epsilon();
  double tolerance = epsilon * stayDim * saes.eigenvalues().array().maxCoeff();
  Eigen::MatrixXd S = (saes.eigenvalues().array() > tolerance)
                          .select(saes.eigenvalues().array(), 0);
  Eigen::MatrixXd Sinv = (saes.eigenvalues().array() > tolerance)
                             .select(saes.eigenvalues().array().inverse(), 0);
  Eigen::MatrixXd S_sqrt = S.cwiseSqrt();
  Eigen::MatrixXd Sinv_sqrt = Sinv.cwiseSqrt();

  // Effective jacobian J: from H = J^T J
  Wout = (p.asDiagonal() * saes.eigenvectors() * (S_sqrt.asDiagonal()))
             .transpose();

  // Constant error at marginalization point: -(J^T)^(-1)*bstar
  Eigen::MatrixXd Jinv_T = Sinv_sqrt.asDiagonal() *
                           saes.eigenvectors().transpose() * p_inv.asDiagonal();
  eout = -1.0 * Jinv_T * bstar;

  constexpr bool kUseCholeskyDecomposition = false;
  if (kUseCholeskyDecomposition) {
    // TODO(tim) You can alternatively using Cholesky decomposition, but it
    // didn't work as well. Keeping it here for reference.
    Hstar = 0.5 * Hstar + 0.5 * Hstar.transpose().eval();  // Ensure symmetric
    Eigen::LLT<Eigen::MatrixXd, Eigen::Upper> llt2(Hstar);
    Eigen::MatrixXd J_p = llt2.matrixU();

    Wout = J_p;
    Eigen::MatrixXd Jinv_T = (J_p * J_p.transpose()).inverse() * J_p;
    eout = -1.0 * Jinv_T * bstar;
  }

  return res;
}

Eigen::PermutationMatrix<Eigen::Dynamic> getPermutation(
    int dim, const std::vector<std::pair<int, int>>& segmentPositionAndSize) {
  Eigen::PermutationMatrix<Eigen::Dynamic> perm(dim);
  perm.indices().setConstant(-1);

  // Put the target section at the front
  int index = 0;
  for (auto& s : segmentPositionAndSize) {
    for (int i = 0; i < s.second; ++i) {
      perm.indices()[s.first + i] = index;
      ++index;
    }
  }

  // Everything else goes to the back in the same order
  for (int i = 0; i < dim; ++i) {
    if (perm.indices()[i] < 0) {
      perm.indices()[i] = index;
      ++index;
    }
  }

  return perm;
}

void reorderNormalEquations(
    Eigen::MatrixXd& Hstar, Eigen::VectorXd& bstar,
    const std::vector<std::pair<int, int>>& segmentPositionAndSize) {
  Eigen::PermutationMatrix<Eigen::Dynamic> perm =
      getPermutation(Hstar.rows(), segmentPositionAndSize);
  // std::cout << "perm: " << perm.indices().transpose() << std::endl;
  Hstar = perm * Hstar * perm.transpose();
  bstar = perm * bstar;
}

}  // namespace confusion
