/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/utilities/time_utils.h"

namespace confusion {

Rate::Rate(const double& freq) { setFreq(freq); }

void Rate::sleep() {
  std::this_thread::sleep_until(t_last_ + dt_);

  t_last_ = std::chrono::high_resolution_clock::now();
}

void Rate::reset() { t_last_ = std::chrono::high_resolution_clock::now(); }

void Rate::setFreq(const double& freq) {
  if (freq <= 0) {
    std::cout << "ERROR: User set the confusion::Rate frequency <= 0. Not "
                 "applying the change."
              << std::endl;
    return;
  }
  if (freq > 1000) {
    std::cout << "WARNING: confusion::Rate will overrun considerably for "
                 "rates higher than 1 kHz."
              << std::endl;
  }
  freq_ = freq;
  double dt_double = 1.0 / freq_ * 1.0e9;
  dt_ = std::chrono::nanoseconds((long int)dt_double);
  reset();
}

Timer::Timer() { t_start_ = std::chrono::high_resolution_clock::now(); }

double Timer::getDuration() {
  return std::chrono::duration<double>(
             std::chrono::high_resolution_clock::now() - t_start_)
      .count();
}

void Timer::reset() { t_start_ = std::chrono::high_resolution_clock::now(); }

RateLimiter::RateLimiter(const double& freq) { setFrequency(freq); }

void RateLimiter::setFrequency(const double& freq) {
  if (freq <= 0) {
    std::cout << "ERROR: User set the confusion::RateLimiter frequency <= 0. "
                 "Not applying the change."
              << std::endl;
    return;
  }
  if (freq > 1000) {
    std::cout << "WARNING: confusion::RateLimiter will overrun considerably "
                 "for rates higher than 1 kHz."
              << std::endl;
  }
  dt_ = 1.0 / freq;
  timer_.reset();
}

bool RateLimiter::check() {
  if (timer_.getDuration() >= dt_) {
    timer_.reset();
    return true;
  }
  return false;
}

void RateLimiter::reset() { timer_.reset(); }

double cpuTimeNow() {
  auto current_time = std::chrono::system_clock::now();
  auto duration_in_seconds =
      std::chrono::duration<double>(current_time.time_since_epoch());
  return duration_in_seconds.count();
}

}  // namespace confusion