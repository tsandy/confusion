/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/StateVector.h"

namespace confusion {

StateVector::StateVector(std::shared_ptr<State> firstStatePtr)
    : numProcessSensors_(firstStatePtr->numProcessSensors_),
      numUpdateSensors_(firstStatePtr->numUpdateSensors_),
      updateMeasStateIndex(firstStatePtr->numUpdateSensors_),
      processMeasStateIndex(firstStatePtr->numProcessSensors_) {
  pushState(firstStatePtr);
  for (size_t i = 0; i < firstStatePtr->numProcessSensors_; ++i)
    processMeasStateIndex[i] = 0;
  for (size_t i = 0; i < firstStatePtr->numUpdateSensors_; ++i)
    updateMeasStateIndex[i] = 0;
}

StateVector::StateVector(StateVector& stateVector)
    : states_(stateVector.states_),
      firstStateInitialized_(stateVector.firstStateInitialized_),
      numProcessSensors_(stateVector.numProcessSensors_),
      numUpdateSensors_(stateVector.numUpdateSensors_) {}

StateVector::StateVector(const size_t numProcessSensors,
                         const size_t numUpdateSensors)
    : numProcessSensors_(numProcessSensors),
      numUpdateSensors_(numUpdateSensors) {}

void StateVector::pushState(const std::shared_ptr<State>& newState) {
  states_.push_back(newState);
}

void StateVector::insertState(const std::shared_ptr<State>& newState,
                              size_t index) {
  if (index == 0 || index >= states_.size()) {
    std::cout << "WARNING: StateVector::insertState requested for a position "
                 "not inside of the current batch. Will not insert the state."
              << std::endl;
    return;
  }

  states_.insert(states_.begin() + index, newState);

  // We additionally need to shift the measurement assignment indices
  // Note that we increment only if > index for update measurements (try to
  // add more update measurements to the new intermediate state first but also
  // if = for process measurements so that duplicate process measurements
  // don't get assigned to the new state
  for (auto& binIndex : updateMeasStateIndex) {
    if (binIndex > index) ++binIndex;
  }
  for (auto& binIndex : processMeasStateIndex) {
    if (binIndex >= index) ++binIndex;
  }
}

void StateVector::popState() {
  states_.pop_front();

  // We additionally need to shift the measurement assignment indices
  for (auto& binIndex : updateMeasStateIndex) {
    if (binIndex > 0) --binIndex;
  }
  for (auto& binIndex : processMeasStateIndex) {
    if (binIndex > 0) --binIndex;
  }
}

void StateVector::removeState(size_t index) {
  states_.erase(states_.begin() + index);

  // Shift the measurement assignment indices if they come after the removed
  // state.
  for (auto& binIndex : updateMeasStateIndex) {
    if (binIndex > 0 && binIndex >= index) --binIndex;
  }
  for (auto& binIndex : processMeasStateIndex) {
    if (binIndex > 0 && binIndex >= index) --binIndex;
  }
}

bool StateVector::check() const {
  bool res = true;
  for (size_t i = 0; i < numProcessSensors_; ++i) {
    for (size_t j = 0; j < states_.size() - 1; ++j) {
      if (states_[j]->processChains_[i]->ready()) {
        if (states_[j]->processChains_[i]->tStart() != states_[j]->t()) {
          std::cout << "ERROR: ProcessChain type " << i
                    << " at t=" << states_[j]->processChains_[i]->tStart()
                    << " incorrectly assigned to state at t=" << states_[j]->t()
                    << std::endl;
          res = false;
        }
        if (states_[j]->processChains_[i]->tEnd() != states_[j + 1]->t()) {
          std::cout << "ERROR: ProcessChain type " << i
                    << " at t=" << states_[j]->processChains_[i]->tStart()
                    << " has incorrect tEnd="
                    << states_[j]->processChains_[i]->tEnd()
                    << " with following state at t=" << states_[j + 1]->t()
                    << std::endl;
          res = false;
        }

        if (!states_[j]->processChains_[i]->check()) res = false;
      }
    }
  }

  for (size_t i = 0; i < numUpdateSensors_; ++i) {
    for (size_t j = 0; j < states_.size(); ++j) {
      for (size_t k = 0; k < states_[j]->updateMeasurements_[i].size(); ++k) {
        if (states_[j]->updateMeasurements_[i][k]->initialized()) {
          if (states_[j]->updateMeasurements_[i][k]->t() != states_[j]->t()) {
            std::cout << "Update measurement type " << i << " at "
                      << states_[j]->updateMeasurements_[i][k]->t()
                      << " incorrectly assigned to state at " << states_[j]->t()
                      << std::endl;
            res = false;
          }
          if (!states_[j]->updateMeasurements_[i][k]->check()) res = false;
        }
      }
    }
  }

  return res;
}

void StateVector::print() const {
  for (size_t i = 0; i < states_.size(); ++i) {
    std::cout << "State at T=" << states_[i]->t() << " has ";

    // Print process measurement times
    for (size_t j = 0; j < numProcessSensors_; ++j) {
      if (!states_[i]->processChains_[j]->measurements_.empty()) {
        std::cout << states_[i]->processChains_[j]->measurements_.size()
                  << " of process type " << j << ": t_front="
                  << states_[i]->processChains_[j]->measurements_.front()->t()
                  << ": t_back="
                  << states_[i]->processChains_[j]->measurements_.back()->t()
                  << std::endl;
      }
    }

    // Print update measurement times
    for (size_t j = 0; j < numUpdateSensors_; ++j) {
      if (!states_[i]->updateMeasurements_[j].empty()) {
        std::cout << "Update type " << j << " at time: ";
        for (size_t k = 0; k < states_[i]->updateMeasurements_[j].size(); ++k) {
          std::cout << states_[i]->updateMeasurements_[j][k]->t() << ", ";
        }
      }
      std::cout << "\n";
    }
    std::cout << std::endl;
  }
}

void StateVector::deactivateParameters() {
  for (auto& state : states_)
    for (auto& param : state->parameters_) param.active_ = false;
}

size_t StateVector::size() const { return states_.size(); }

void StateVector::reset() {
  // Remove all states but the most recent one
  while (size() > 1) popState();

  // Reset the first state
  front()->reset();

  firstStateInitialized_ = false;
}

void StateVector::clear() {
  states_.clear();
  firstStateInitialized_ = false;
}

}  // namespace confusion