/*
 * Copyright 2018 ETH Zurich, Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "confusion/PriorCost.h"

namespace confusion {

PriorCost::PriorCost(const std::vector<FixedParameter>& marginalizedParams,
                     const Eigen::MatrixXd& W_,
                     Eigen::VectorXd& priorErrorOffset_)
    : marginalizedParams_(marginalizedParams),
      W(W_),
      priorErrorOffset(priorErrorOffset_) {
  std::vector<int>* param_sizes = mutable_parameter_block_sizes();
  int priorParameterLocalDim = 0;

  // Do the bookkeeping on the parameter sizes for ceres
  for (const auto& param : marginalizedParams) {
    param_sizes->push_back(param.size());
    priorParameterLocalDim += param.localSize();
  }

  // Make sure the sizes match
  if (priorParameterLocalDim != W.rows() ||
      priorParameterLocalDim != W.cols()) {
    std::cout << "---Size mismatch in PriorCost! W size:" << W.rows() << ","
              << W.cols()
              << "; expected to be of size: " << priorParameterLocalDim
              << std::endl;
    abort();
  }
  if (W.rows() != priorErrorOffset.rows()) {
    std::cout << "---Size of W and priorErrorOffset don't match! W size: "
              << W.rows()
              << "; priorErrorOffset size: " << priorErrorOffset.rows()
              << std::endl;
    abort();
  }

  errorSize = priorParameterLocalDim;

  // Set the number of residuals for ceres
  set_num_residuals(errorSize);

#ifdef DEBUG_PRIOR
  std::cout << "PriorCost created with " << marginalizedParams_.size()
            << " marginalized parameters of " << errorSize << " local size "
            << std::endl;
#endif
}

bool PriorCost::Evaluate(double const* const* x, double* residuals,
                         double** jacobians) const {
#ifdef COST_DEBUG
  std::cout << "Starting PriorCost" << std::endl;
#endif
  size_t residualIndex = 0;

  Eigen::Map<Eigen::VectorXd> e(residuals, errorSize);

  // Evaluate for the static parameters
  for (size_t i = 0; i < marginalizedParams_.size(); ++i) {
    if (!marginalizedParams_[i].isConstant()) {
      if (marginalizedParams_[i].parameterization_) {
        marginalizedParams_[i].parameterization_->boxMinus(
            marginalizedParams_[i].copyOfData_.data(), x[i],
            e.data() + residualIndex);
        if (jacobians && jacobians[i]) {
          Eigen::MatrixXd dd_dxi =
              marginalizedParams_[i].parameterization_->boxMinusJacobianRight(
                  marginalizedParams_[i].copyOfData_.data(), x[i]);
          Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic,
                                   Eigen::RowMajor> >
              de_dxi(jacobians[i], errorSize, marginalizedParams_[i].size());
          de_dxi = W.block(0, residualIndex, errorSize,
                           marginalizedParams_[i].localSize()) *
                   dd_dxi;
        }
      } else {
        // No local parameterization
        for (size_t j = 0; j < marginalizedParams_[i].size(); ++j) {
          e[residualIndex + j] =
              marginalizedParams_[i].copyOfData_(j) - x[i][j];
        }
        if (jacobians && jacobians[i]) {
          // The Jacobian is W.col[xi]*Identity
          Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic,
                                   Eigen::RowMajor> >
              de_dxi(jacobians[i], errorSize, marginalizedParams_[i].size());
          de_dxi = -1.0 * W.block(0, residualIndex, errorSize,
                                  marginalizedParams_[i].size());
        }
      }
      residualIndex += marginalizedParams_[i].localSize();
    }
  }

  // Do some sanity checks
  if (residualIndex != errorSize) {
    std::cout << "ERROR! PriorCost residualIndex is not equal to errorSize "
                 "after cost computation! actual size: "
              << residualIndex << ", expected size: " << errorSize << std::endl;
    abort();
  }
#ifdef PRINT_PRIOR_COST
  std::cout << "PriorCost before weighting: " << e.transpose() << std::endl;
#endif

  e = priorErrorOffset + W * e;

#ifdef PRINT_PRIOR_COST
  std::cout << "PriorCost: " << e.transpose() << std::endl;
#endif
#ifdef COST_DEBUG
  std::cout << "Done PriorCost" << std::endl;
//		std::cout << "PriorCost: " << e.transpose() << std::endl;
#endif
  return true;
}

}  // namespace confusion