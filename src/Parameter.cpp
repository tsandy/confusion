/*
* Copyright 2018 ETH Zurich, Timothy Sandy
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may teobtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "confusion/Parameter.h"

namespace confusion {

Parameter::Parameter(
    double* data, size_t size, const std::string& name, bool constant,
    std::shared_ptr<LocalParameterizationBase> parameterization)
    : data_(data),
      parameterization_(std::move(parameterization)),
      size_(size),
      name_(name),
      constant_(constant) {
  if (parameterization_)
    localSize_ = parameterization_->LocalSize();
  else
    localSize_ = size_;
}

Parameter::Parameter(double* data, size_t size, const std::string& name,
          std::shared_ptr<LocalParameterizationBase> parameterization)
    : data_(data),
      parameterization_(std::move(parameterization)),
      size_(size),
      name_(name),
      constant_(false) {
  if (parameterization_)
    localSize_ = parameterization_->LocalSize();
  else
    localSize_ = size_;
}

void Parameter::setInitialConstraintWeighting(const Eigen::MatrixXd& w) {
  if (w.rows() != int(localSize_) || w.cols() != int(localSize_)) {
    std::cout << "ERROR: Trying to set an initial constraint weighting for "
                 "parameter "
              << name() << " with the wrong size!" << std::endl;
    abort();
  }
  if (active_) {
    std::cout << "ERROR: Trying to set an initial constraint weighting for "
                 "parameter "
              << name()
              << " that is already active! "
                 "This should be done before starting fusion"
              << std::endl;
    return;
  }
  initialConstraintWeighting_ = w;
  immediatelyAddToPrior_ = true;
}

void Parameter::addToProblem(ceres::Problem* problem) {
  if (active_) {
    if (constant_) {
      // todo Remove this check once I am sure that its not needed
      if (problem->HasParameterBlock(data_))
        problem->SetParameterBlockConstant(data_);
      else
        std::cout << "Trying to set parameter " << name()
                  << " const when it isn't in the ceres problem???"
                  << std::endl;
    } else if (parameterization_) {
      if (problem->HasParameterBlock(data_))
        problem->SetParameterization(data_, parameterization_.get());
      else
        std::cout << "Trying to set parameterization for parameter " << name()
                  << " when it isn't in the ceres problem???" << std::endl;
    }

    if (randomWalkProcessActive_) {
      if (constant_) {
        // todo Remove this check once I am sure that its not needed
        if (problem->HasParameterBlock(rwpPriorSideParameterValue_.data()))
          problem->SetParameterBlockConstant(
              rwpPriorSideParameterValue_.data());
        else
          std::cout << "Trying to set a rwp parameter const that isn't in "
                       "the ceres problem???"
                    << std::endl;
      } else if (parameterization_) {
        problem->SetParameterization(rwpPriorSideParameterValue_.data(),
                                     parameterization_.get());
      }
    }
  }
}

Parameter Parameter::clone(double* data) const {
  Parameter param(data, size_, name_, constant_, parameterization_);

  // Copy over the values of the parameter
  for (size_t i = 0; i < size_; ++i) data[i] = data_[i];

  // Copy over the internal state
  param.immediatelyAddToPrior_ = immediatelyAddToPrior_;
  param.initialConstraintWeighting_ = initialConstraintWeighting_;

  return param;
}

void Parameter::attachRandomWalkProcess(const double& processNoise) {
  if (randomWalkProcess_) {
    std::cout << "WARNING: User requested to attach a random walk process to "
                 "a static parameter with one "
                 "already attached. Ignoring the request."
              << std::endl;
    return;
  }

  // Copy the parameter value over for the prior side parameter
  rwpPriorSideParameterValue_.resize(size());
  for (size_t i = 0; i < size(); ++i)
    rwpPriorSideParameterValue_(i) = data_[i];

  // Create the RWP
  if (parameterization_)
    randomWalkProcess_ = std::make_shared<StaticParameterRandomWalkProcess>(
        rwpPriorSideParameterValue_.data(), data_, parameterization_.get(),
        processNoise);
  else
    randomWalkProcess_ = std::make_shared<StaticParameterRandomWalkProcess>(
        rwpPriorSideParameterValue_.data(), data_, size(), processNoise);
}

void Parameter::detachRandomWalkProcess() {
  if (!randomWalkProcess_) {
    std::cout << "WARNING: User requested to detach a random walk process "
                 "from a static parameter without one "
                 "attached. Ignoring the request."
              << std::endl;
    return;
  }

  randomWalkProcess_ = nullptr;
  randomWalkProcessActive_ = false;
}

void Parameter::toCout() const {
  std::cout << "[";
  for (size_t i = 0; i < size_; ++i) {
    std::cout << data_[i];
    if (i + 1 < size_) std::cout << ",";
  }
  std::cout << "]";
}

void Parameter::setConstant() {
  if (priorConstraintActive_) {
    std::cout << "ERROR: Cannot directly set parameter " << name_
              << " constant because it is active in the prior constraint! "
                 "To set a parameter constant during operation, use "
                 "ConFusor::setStaticParamsConstant"
              << std::endl;
    return;
  }
  constant_ = true;
  randomWalkProcessActive_ = false;
}

void Parameter::updateRandomWalkProcess(const double& dt) {
  if (!randomWalkProcess_) {
    std::cout << "ERROR: Parameter::updateRandomWalkProcess called with no "
                 "RWP attached!"
              << std::endl;
    abort();
  }

  // Update the value of the prior side parameter
  for (size_t i = 0; i < size(); ++i)
    rwpPriorSideParameterValue_(i) = data_[i];

  // Update the dt in the cost function
  randomWalkProcess_->updateDt(dt);

  randomWalkProcessActive_ = true;
}

void Parameter::addRandomWalkProcessToProblem(ceres::Problem* problem) {
  if (!randomWalkProcess_) {
    std::cout << "ERROR: Parameter::addRandomWalkProcessToProblem called "
                 "with no RWP attached!"
              << std::endl;
    abort();
  }

  if (!randomWalkProcess_) {
    std::cout << "ERROR: No RWP attached when "
                 "Parameter::addRandomWalkProcessesToProblem was called??"
              << std::endl;
    abort();
  }

  randomWalkProcess_->addCostFunctionToProblem(problem);
}

FixedParameter::FixedParameter(Parameter paramIn)
    : size_(paramIn.size_),
      localSize_(paramIn.localSize_),
      parameterization_(paramIn.parameterization_),
      constant_(paramIn.constant_),
      name_(paramIn.name_) {
  copyOfData_.resize(paramIn.size_);
  for (size_t i = 0; i < paramIn.size_; ++i) {
    // If a RWP is active, we want to copy the value of the prior side
    // parameter
    if (paramIn.isRandomWalkProcessActive())
      copyOfData_(i) = paramIn.rwpPriorSideParameterValue_(i);
    else
      copyOfData_(i) = paramIn.data_[i];
  }
}

} // namespace confusion