% This function imports the data from the confusion logger.
% * bagName: ConFusion data bag name
function [conFusionData] = importConFusionData(bagName)
tic
file = fopen(bagName);

batches = cell(0);
staticParameters = struct();
priorResiduals = [];
updateResidualBatches = cell(0);
processResidualBatches = cell(0);
userData = [];

% Process header
line = fgetl(file);
if strcmp(line, 'State names')
    usingStateNames = true;
    
    stateLengths = [];
    stateNames = cell(0);
    
    while true
        line = fgetl(file);
        if strcmp(line, 'Start log')
            break;
        end
        
        [currentStateLength, currentStateName] = strtok(line, ' ');
        currentStateName(isspace(currentStateName)) = [];
        
%         eval(['batches.' currentStateName ' = [];']);
        
        stateLengths(end+1, 1) = str2num(currentStateLength);
        stateNames{end+1} = currentStateName;
    end
elseif strcmp(line, 'Start log')
    usingStateNames = false;
    % empty
else
    error('Invalid first line');
end


while true
    [success, line] = getNextLine(file);
    if ~success
        break;
    end
    
    type = line(1:2);
    switch type
        case 'BS'
            % Batches need cells, as these can have different numbers of
            % states
            numBatch = sscanf(line(4:end), '%i');
            
            if (usingStateNames)
                % Initialize
                newBatch = struct();
                newBatch.time = [];
                for iState = 1:length(stateNames)
                     eval(['newBatch.' stateNames{iState} ' = [];']);
                end

                success = true;
                for i = 1:numBatch
                    [success, line] = getNextLine(file);
                    if ~success
                        disp('WARNING: Batch of states entry incomplete.');
                        break;
                    end
                    currentData = sscanf(line(4:end), '%f')';

                    newBatch.time(end+1,1) = currentData(1);
                    endColumn = 1;
                    for iState = 1:length(stateNames)
                        startColumn = endColumn + 1;
                        endColumn = endColumn + stateLengths(iState);
                        if endColumn > length(currentData)
                            success = false;
                            disp('WARNING: State entry incomplete while reading batch.');
                            break;
                        end
                        eval(['newBatch.' stateNames{iState} '(end+1, :) = currentData(startColumn:endColumn);']);
                    end
                    if ~success
                        disp('WARNING: Batch of states entry incomplete.');
                        break;
                    end
                end
                if ~success
                    break;
                end
                
                batches{end+1} = newBatch;
            else
                newBatch = [];
                success = true;
                for i = 1:numBatch
                    [success, line] = getNextLine(file);
                    if ~success
                        disp('WARNING: Batch of states entry incomplete.');
                        break;
                    end
                    newBatch = readLogFileLine(newBatch, line);
                end
                if ~success
                    break;
                end
                
                batches{end+1} = newBatch;
            end
            
            [success, line] = getNextLine(file);
            if ~success
                disp('WARNING: Batch of states entry incomplete.');
                break;
            end
            type = line(1:2);
            code = line(4:5);
            if ~strcmp(type, 'BS') || ~strcmp(code, '-1')
                error('Batch ended in wrong order');
            end
        case 'SP'
            % Set of estimated static parameters
            numStaticParameters = sscanf(line(4:end), '%i');
            staticParameterLines = {};
            success = true;
            for i = 1:numStaticParameters
                [success, staticParameterLines{end+1}] = getNextLine(file);
                if ~success
                    break;
                end
            end
            
            if ~success || length(staticParameterLines) ~= numStaticParameters
                disp('WARNING: Static parameter entry incomplete.');
                break;
            end
            
            [success, line] = getNextLine(file);
            if ~success
                disp('WARNING: Batch of states entry incomplete.');
                break;
            end
            type = line(1:2);
            code = line(4:5);
            if ~strcmp(type, 'SP') || ~strcmp(code, '-1')
                error('Static parameters ended in wrong order');
            end
            
            for i = 1:numStaticParameters
                parameterName = strtok(staticParameterLines{i});
                parameterExists = eval(['isfield(staticParameters, ''' parameterName ''');']);
                if ~parameterExists
                    eval(['staticParameters.' parameterName '= [];']);
                end
                eval(['staticParameters.' parameterName '(end+1,:) = sscanf(staticParameterLines{i}(length(parameterName)+2:end), ''%f'');']);
            end
            
        case 'PI'
            % Prior residuals grow over time
            priorResiduals = readLogFileLine(priorResiduals, line);
        case 'BU'
            numBatch = sscanf(line(4:end), '%i');
            newResidualBatch = [];
            
            success = true;
            for i = 1:numBatch
                [success, line] = getNextLine(file);
                if ~success
                    disp('WARNING: Update residual batch entry incomplete.');
                    break;
                end
                newResidualBatch = readLogFileLine(newResidualBatch, line);
            end
            if ~success
                break;
            end
            
            [success, line] = getNextLine(file);
            if ~success
                disp('WARNING: Update residual batch entry incomplete.');
                break;
            end
            type = line(1:2);
            code = line(4:5);
            if ~strcmp(type, 'BU') || ~strcmp(code, '-1')
                error('Update residual batch ended in wrong order');
            end
            
            updateResidualBatches{end+1} = newResidualBatch;
        case 'BP'
            numBatch = sscanf(line(4:end), '%i');
            if numBatch > 0
                newResidualBatch = [];
                success = true;
                for i = 1:numBatch
                    [success, line] = getNextLine(file);
                    if ~success
                        disp('WARNING: Process residual batch entry incomplete.');
                        break;
                    end
                    newResidualBatch = readLogFileLine(newResidualBatch, line);
                end
                if ~success
                    break;
                end
                
                [success, line] = getNextLine(file);
                if ~success
                    disp('WARNING: Process residual batch entry incomplete.');
                    break;
                end
                type = line(1:2);
                code = line(4:5);
                if ~strcmp(type, 'BP') || ~strcmp(code, '-1')
                    error('Process residual batch ended in wrong order');
                end
                
                processResidualBatches{end+1} = newResidualBatch;
            end
        case 'UU'
            userData = readLogFileLine(userData, line);
        otherwise
            error('Unrecognized line format');
    end           
end
toc

disp(['Imported ' num2str(size(batches,2)) ' state batches, ' num2str(size(updateResidualBatches,2)) ' update residual batches, and ' num2str(size(processResidualBatches,2)) ' process residual batches']);

conFusionData.stateNames = stateNames;
conFusionData.stateLengths = stateLengths;
conFusionData.batches = batches;
conFusionData.staticParameters = staticParameters;
conFusionData.priorResiduals = priorResiduals;
conFusionData.updateResidualBatches = updateResidualBatches;
conFusionData.processResidualBatches = processResidualBatches;
conFusionData.userData = userData;

end
