function [success,line] = getNextLine(file)
    % Read the next line of a loaded text file
    % success=TRUE if a non-empty line was read that doesn't start with a space.
    line = fgetl(file);
    if ~ischar(line) || isempty(line) || isspace(line(1))
        success = false;
    else
        success = true;
    end
end

